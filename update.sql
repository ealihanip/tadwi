-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 25 Des 2017 pada 08.20
-- Versi Server: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ponpes`
--

-- --------------------------------------------------------
TRUNCATE TABLE tbl_pbr_syh;
ALTER TABLE `tbl_pbr_syh`  
ADD FOREIGN KEY (`id_tagihan`) REFERENCES `tb_tagihan`(`id_tagihan`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `tb_pbr_pdftrn` DROP FOREIGN KEY `tb_pbr_pdftrn_ibfk_1`;

ALTER TABLE `tb_pbr_pdftrn` ADD CONSTRAINT `tb_pbr_pdftrn_ibfk_1` FOREIGN KEY (`NIS`) REFERENCES `tb_santri`(`NIS`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `tb_trx_dntr` DROP FOREIGN KEY `tb_trx_dntr_ibfk_1`;

ALTER TABLE `tb_trx_dntr` ADD CONSTRAINT `tb_trx_dntr_ibfk_1` FOREIGN KEY (`id_donatur`) REFERENCES `tb_donatur`(`id_donatur`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `tb_tagihan` DROP FOREIGN KEY `FKSANTRI`;

ALTER TABLE `tb_tagihan` ADD CONSTRAINT `FKSANTRI` FOREIGN KEY (`NIS`) REFERENCES `tb_santri`(`NIS`) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Struktur dari tabel `tb_user`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
