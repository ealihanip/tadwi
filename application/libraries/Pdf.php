<?php
	class Pdf {
		
		function pdf_create($html, $filename, $paper, $orientation,$view=true, $stream=TRUE)
		{
			require_once 'dompdf/autoload.inc.php';
			
			

			// instantiate and use the dompdf class
			$dompdf = new Dompdf\Dompdf();
			$dompdf->loadHtml($html);

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper($paper,$orientation);

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			if ($stream) {
				if ($view) {
					$dompdf->stream($filename.".pdf",array('Attachment'=>0));//output ke browser	
				}else{
					$dompdf->stream($filename.".pdf",array('Attachment'=>1));//download pdf
				}				
			}
		}
	}
?>