<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_laporan extends CI_Model {
	
	
	
	
	function get($where='',$table) {
		
		
		if($where!=""){	
			$this->db->like($where);
		}
		
		
		$query = $this->db->get($table);
		
		
		
		return $query;
		$query->free_result();
		
	}
	
	
	function gettagihan($where='') {
		
		
		if($where!=""){	
			$this->db->like($where);
		}
		
		
		$this->db->select('*');
		$this->db->from('tb_tagihan');
		$this->db->join('tb_santri', 'tb_tagihan.NIS = tb_santri.NIS');
		
		$query = $this->db->get();
		
		
		
		return $query;
		$query->free_result();
		
	}
	
	function getpembayaranpendaftaran($where='') {
		
		
		if($where!=""){	
			$this->db->like($where);
		}
		
		
		$this->db->select('tb_santri.NIS,tb_santri.nm_santri,tb_santri.angkatan');
		$this->db->select("IFNULL(tb_pbr_pdftrn.`id_pmbyrn1`,'Belum Bayar') AS id_pmbyrn1,IFNULL(tb_pbr_pdftrn.`tgl_bayar`,'Belum Bayar') AS tgl_bayar,IFNULL(tb_pbr_pdftrn.`total_bayar`,'Belum Bayar') AS total_bayar ");
		$this->db->from('tb_santri');
		$this->db->join('tb_pbr_pdftrn', 'tb_santri.NIS = tb_pbr_pdftrn.NIS','LEFT');
		
		$query = $this->db->get();
		
		
		
		return $query;
		$query->free_result();
		
	}
		
	function getlaporanpembayarabsyahriyah($where='') {
		
		
		if($where!=""){	
			$this->db->like($where);
		}
		
		
		$this->db->select('*');
		$this->db->from('tbl_pbr_syh');
		$this->db->join('tb_tagihan', 'tbl_pbr_syh.id_tagihan = tb_tagihan.id_tagihan');
		$this->db->join('tb_santri', 'tb_tagihan.NIS = tb_santri.NIS');
		
		$query = $this->db->get();
		
		
		
		return $query;
		$query->free_result();
		
	}
	
	
	function getlaporandanadonasi($where='') {
		
		
		if($where!=""){	
			$this->db->like($where);
		}
		
		
		$this->db->select('*');
		$this->db->from('tb_trx_dntr');
		$this->db->join('tb_donatur', 'tb_trx_dntr.id_donatur = tb_donatur.id_donatur');
		
		
		$query = $this->db->get();
		
		
		
		return $query;
		$query->free_result();
		
	}
		
	
}