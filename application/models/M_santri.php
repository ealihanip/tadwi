<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_santri extends CI_Model {
	
	
	
	
	function get($where) {
		
		
		$this->db->where($where);
		$query = $this->db->get('tb_santri');
		
		
		return $query;
		$query->free_result();
		
	}
	
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('tb_santri');
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
        $this->datatables->select('NIS,nm_santri,kelamin,tmp_lahir,tgl_lahir,alamat,no_hp,angkatan,nm_ortu,foto');
		$this->datatables->add_column("aksi"
		,'
		<a class="btn green" onclick="javascript:openmodaledit($1)">Edit</a>
		<a class="btn yellow" onclick="javascript:openmodaldetail($1)">Detail</a>
		<a class="btn red" onclick="javascript:deletedata($1)">hapus</a>'
		, 'NIS,nm_santri,kelamin,tmp_lahir,tgl_lahir,alamat,no_hp,angkatan,nm_ortu,foto');
		if($where !=""){
			$this->datatables->where($where);	
		
		}
		$this->datatables->from('tb_santri');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('tb_santri',$data);
		
		return;
		
	}
	
	
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('tb_santri');
		
		return;
		
	}
	
	
	//for user
	
	function insertuser($data) {
		
		$this->db->insert('tb_user',$data);
		
		return;
		
	}
	
	function getuser($where="") {
		
		if($where){
				
			$this->db->where($where);
		}
		
		
		$query = $this->db->get('tb_user');
		
		
		return $query;
		$query->free_result();
		
	}
	
	function updateuser($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('tb_user');
		
	}
	
	function deleteuser($where) {
		
		$this->db->where($where);
		$this->db->delete('tb_user');
		
		return;
		
	}
	
	
}