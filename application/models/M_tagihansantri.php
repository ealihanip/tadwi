<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_tagihansantri extends CI_Model {
	
	
	
	
	function get($where='') {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('tb_tagihan');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
	function getdatasantri() {
		
		
		$query = $this->db->get('tb_santri');
		
		
		return $query;
		$query->free_result();
		
	}
		
	
	
	function getdata($where) {
		
		$this->load->library('datatables');
        $this->datatables->select(
		
			'tb_santri.NIS,tb_santri.nm_santri,tb_santri.kelamin
			,tb_santri.tmp_lahir,tb_santri.tgl_lahir,tb_santri.alamat,tb_santri.no_hp
			,tb_santri.angkatan,tb_santri.nm_ortu,tb_santri.foto,
			,tb_tagihan.id_tagihan,tb_tagihan.NIS,tb_tagihan.bulan,tb_tagihan.tahun
			,tb_tagihan.makan,tb_tagihan.listrik,tb_tagihan.infaq,tb_tagihan.status'
		);
		
        $this->datatables->join('tb_santri', 'tb_tagihan.NIS = tb_santri.NIS');
		if($where!=""){
				
			$this->datatables->where($where);
			
		}
		$this->datatables->from('tb_tagihan');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('tb_tagihan',$data);
		
		return;
		
	}
		
	
	
	
}