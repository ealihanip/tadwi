<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pembayaranpendaftaran extends CI_Model {
	
	
	
	
	function get($where='') {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('tb_pbr_pdftrn');
		
		
		return $query;
		$query->free_result();
		
	}
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('tb_pbr_pdftrn');
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
        $this->datatables->select(
		
			'tb_santri.NIS,tb_santri.nm_santri,tb_santri.kelamin
			,tb_santri.tmp_lahir,tb_santri.tgl_lahir,tb_santri.alamat,tb_santri.no_hp
			,tb_santri.angkatan,tb_santri.nm_ortu,tb_santri.foto,tb_pbr_pdftrn.id_pmbyrn1
			,tb_pbr_pdftrn.tgl_bayar,tb_pbr_pdftrn.total_bayar'
		);
		$this->datatables->add_column(
		
			"aksi"
			,'
			<a class="btn red" onclick="javascript:deletedata($1)">hapus</a>'
			, 'id_pmbyrn1'
		
		);
        $this->datatables->join('tb_santri', 'tb_pbr_pdftrn.NIS = tb_santri.NIS');
		
		
		$this->datatables->like($where);
		$this->datatables->from('tb_pbr_pdftrn');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('tb_pbr_pdftrn',$data);
		
		return;
		
	}
		
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('tb_pbr_pdftrn');
		
		return;
		
	}
	
	
}