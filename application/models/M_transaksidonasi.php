<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_transaksidonasi extends CI_Model {
	
	
	
	
	function get($where='') {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('tb_trx_dntr');
		
		
		return $query;
		$query->free_result();
		
	}
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('tb_trx_dntr');
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
        $this->datatables->select(
		
			'tb_donatur.id_donatur
			,tb_donatur.nm_donatur
			,tb_donatur.kelamin
			,tb_donatur.tmp_lahir
			,tb_donatur.tgl_lahir
			,tb_donatur.alamat
			,tb_donatur.no_telp
			,tb_donatur.no_rek
			,tb_donatur.status
			,tb_donatur.tgl_join
			,tb_trx_dntr.id_trans
			,tb_trx_dntr.id_donatur
			,tb_trx_dntr.tgl_donasi
			,tb_trx_dntr.jml_donasi
			,tb_trx_dntr.transaksi
			,tb_trx_dntr.statverifikasi'
		);
		$this->datatables->add_column(
		
			"aksi"
			,'
			<a class="btn red" onclick="javascript:deletedata($1)">hapus</a> 
			<a class="btn blue" onclick="javascript:verifikasi($1)">Verifikasi</a>'
			, 'id_trans'
		
		);
        $this->datatables->join('tb_donatur', 'tb_donatur.id_donatur = tb_trx_dntr.id_donatur');
		
		if($where!=""){
				
			$this->datatables->where($where);
		
		}
		
		$this->datatables->from('tb_trx_dntr');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('tb_trx_dntr',$data);
		
		return;
		
	}
		
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('tb_trx_dntr');
		
		return;
		
	}
	
	
}