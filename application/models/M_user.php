<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_user extends CI_Model {
	
	
	
	
	function get($where="") {
		
		if($where!=""){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('tb_user');
		
		
		return $query;
		$query->free_result();
		
	}
		
	function insert($data) {
		
		$this->db->insert('tb_user',$data);
		
		return;
		
	}
		
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('tb_user');
		
		return;
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
        $this->datatables->select('id_user,username,jenisuser');
		$this->datatables->add_column("aksi"
		,'
		<a class="btn green" onclick="javascript:openmodaledit($1)">Edit</a>
		
		<a class="btn red" onclick="javascript:deletedata($1)">hapus</a>'
		, 'id_user');
		if($where !=""){
			$this->datatables->where($where);	
		
		}
		$this->datatables->from('tb_user');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('tb_user');
		
	}
	
	
}