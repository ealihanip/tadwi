<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_donatur extends CI_Model {
	
	
	
	
	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('tb_donatur');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('tb_donatur');
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
        $this->datatables->select('id_donatur,nm_donatur,kelamin,tmp_lahir,tgl_lahir,alamat,no_telp,no_rek,status,tgl_join');
		$this->datatables->add_column("aksi"
		,'
		<a class="btn green" onclick="javascript:openmodaledit($1)">Edit</a>
		<a class="btn yellow" onclick="javascript:openmodaldetail($1)">Detail</a>
		<a class="btn red" onclick="javascript:deletedata($1)">hapus</a>'
		, 'id_donatur,id_pdftr,nm_donatur,kelamin,tmp_lahir,tgl_lahir,alamat,no_telp,no_rek,status,tgl_join');
		
		if($where!=""){
				
			$this->datatables->where($where);
			
		}
        $this->datatables->from('tb_donatur');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('tb_donatur',$data);
		
		return;
		
	}
		
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('tb_donatur');
		
		return;
		
	}
	
	function insertpendaftaran($data) {
		
		$this->db->insert('tb_pdftrn',$data);
		
		return;
		
	}
	
	function getpendaftaran($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('tb_pdftrn');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
	//for user
	
	function insertuser($data) {
		
		$this->db->insert('tb_user',$data);
		
		return;
		
	}
	
	function getuser($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('tb_user');
		
		
		return $query;
		$query->free_result();
		
	}
	
	function updateuser($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('tb_user');
		
	}
	
	function deleteuser($where) {
		
		$this->db->where($where);
		$this->db->delete('tb_user');
		
		return;
		
	}
	
}