<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pembayaransyahriyah extends CI_Model {
	
	
	
	
	function get($where="") {
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('tbl_pbr_syh');
		
		
		return $query;
		$query->free_result();
		
	}
	
	function gettagihan($where) {
		
		
		$this->db->where($where);
		$query = $this->db->get('tb_tagihan');
		
		
		return $query;
		$query->free_result();
		
	}
	
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('tb_tagihan');
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
        $this->datatables->select('
		
		tbl_pbr_syh.id_pmbyrn2,
		tbl_pbr_syh.id_tagihan,
		tbl_pbr_syh.tgl_bayar,
		tbl_pbr_syh.pembayaran_makan,
		tbl_pbr_syh.pembayaran_listrik,
		tbl_pbr_syh.pembayaran_infaq,
		
		tb_santri.NIS,
		tb_santri.nm_santri,
		tb_santri.angkatan,
		
		
		tb_tagihan.id_tagihan,
		tb_tagihan.NIS,
		tb_tagihan.status
		
		');
		
		
		$this->datatables->join('tb_tagihan', 'tb_tagihan.id_tagihan = tbl_pbr_syh.id_tagihan');
		$this->datatables->join('tb_santri', 'tb_tagihan.NIS = tb_santri.NIS');
		
		
		if($where !=""){
			$this->datatables->where($where);	
		
		}
		
		$this->datatables->from('tbl_pbr_syh');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('tbl_pbr_syh',$data);
		
		return;
		
	}
	
	
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('tbl_pbr_syh');
		
		return;
		
	}
	
	
	//for user
	
	function insertuser($data) {
		
		$this->db->insert('tb_user',$data);
		
		return;
		
	}
	
	function getuser($where="") {
		
		if($where){
				
			$this->db->where($where);
		}
		
		
		$query = $this->db->get('tb_user');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
	
}