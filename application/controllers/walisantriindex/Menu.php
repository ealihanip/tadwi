<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	
	function __construct(){
		
		parent::__construct();			
		
		
		if($this->session->userdata('jenis')=='santri'){
			
		}else{
			
			redirect(base_url());
			
		}
	}
	

	public function getviewhome()
	{

		
		$this->load->view('pembangun/walisantri/header');
		$this->load->view('pembangun/walisantri/menutab');
		$this->load->view('pembangun/walisantri/menukiri');
		$this->load->view('walisantri/home');
		$this->load->view('pembangun/walisantri/script');
		$this->load->view('pembangun/walisantri/footer');
	}
	
	public function getviewbiodata()
	{

		$data['pilmenukiri']="biodata";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/walisantri/header');
		$this->load->view('pembangun/walisantri/menutab',$data);
		$this->load->view('pembangun/walisantri/menukiri');
		$this->load->view('walisantri/biodata',$data);
		$this->load->view('pembangun/walisantri/script');
		$this->load->view('pembangun/walisantri/footer');
	}
	
	
	public function getvieweditbiodata()
	{

		$data['pilmenukiri']="biodata";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/walisantri/header');
		$this->load->view('pembangun/walisantri/menutab',$data);
		$this->load->view('pembangun/walisantri/menukiri');
		$this->load->view('walisantri/editbiodata',$data);
		$this->load->view('pembangun/walisantri/script');
		$this->load->view('pembangun/walisantri/footer');
	}
	
	public function getviewtagihan()
	{

		$data{'pilmenukiri'}="tagihan";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/walisantri/header');
		$this->load->view('pembangun/walisantri/menutab',$data);
		$this->load->view('pembangun/walisantri/menukiri');
		$this->load->view('walisantri/tagihan',$data);
		$this->load->view('pembangun/walisantri/script');
		$this->load->view('pembangun/walisantri/footer');
	}
	public function getviewtransaksidonasi()
	{

		$data{'pilmenukiri'}="transaksidonasi";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/walisantri/header');
		$this->load->view('pembangun/walisantri/menutab',$data);
		$this->load->view('pembangun/walisantri/menukiri',$data);
		$this->load->view('walisantri/transaksidonasi');
		$this->load->view('pembangun/walisantri/script');
		$this->load->view('pembangun/walisantri/footer');
	}
	
	
	public function getviewsuksestransaksidonasi()
	{

		$data{'pilmenukiri'}="transaksidonasi";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/walisantri/header');
		$this->load->view('pembangun/walisantri/menutab',$data);
		$this->load->view('pembangun/walisantri/menukiri',$data);
		$this->load->view('walisantri/suksestransaksidonasi');
		$this->load->view('pembangun/walisantri/script');
		$this->load->view('pembangun/walisantri/footer');
	}
	
	public function logout()
	{

		$data = array(
			'id'  => "",
			'username' => "",
			'jenis' => ""
		);
		
		$this->session->set_userdata($data);
		
		redirect(base_url());
	}
	
	
	
	
}
