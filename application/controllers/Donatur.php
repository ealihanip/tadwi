<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donatur extends CI_Controller {

	function __construct(){
		
		parent::__construct();			
		$this->load->model('m_donatur');
		$this->load->library('upload');
		$this->load->library('image_lib');
	}
	
	public function get(){
		$iddonatur = $this->input->post('iddonatur');
		
		$where = array(

			'id_donatur' => $iddonatur
			
		);
		
		$whereuser = array(

			'ref_id' => $iddonatur
			
		);
		
		$data['data']=$this->m_donatur->get($where)->result_array();
		$data['datauser']=$this->m_donatur->getuser($whereuser)->result_array();
		
		echo json_encode($data);
	}
	
	public function getdata(){
		
       if($this->uri->segment('3')){
			$where = array(

				'id_donatur' => $this->uri->segment('3')
					
			);
	   }else{
		   
		  $where="";
	   }

		echo $this->m_donatur->getdata($where);
    }
	
	public function update(){
		
		$iddonatur = $this->input->post('iddonatur');
		$namasantri = $this->input->post('namadonatur');
		$jeniskelamin = $this->input->post('jeniskelamin');
		$tempatlahir = $this->input->post('tempatlahir');
		$tanggallahir = $this->input->post('tanggallahir');
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$nohp = $this->input->post('nohp');
		$norekening = $this->input->post('norekening');
		$tanggaljoin = $this->input->post('tanggaljoin');
		$statusdonatur = $this->input->post('statusdonatur');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$prepassword = $this->input->post('prepassword');
		
		$where = array(

			'id_donatur' => $iddonatur
			
		);
		$data = array(

			'id_donatur' => $iddonatur,
			'nm_donatur' => $namasantri,
			'kelamin' => $jeniskelamin,
			'tmp_lahir' => $tempatlahir,
			'tgl_lahir' => $tanggallahir,
			'alamat' => $alamat,
			'email' => $email,
			'no_telp' => $nohp,
			'no_rek' => $norekening,
			'status' => $statusdonatur,
			'tgl_join' => $tanggaljoin
			
			
		);
		
		
		$this->m_donatur->update($where,$data);
		
		
		//update ke tabel user
		
		
		
		if($password!=$prepassword){//jika password tidak sama dengan sebelumnya
			
			
			$whereuser = array(

				'ref_id' => $iddonatur
				
			);
			
			$data = array(

				'username' => $username,
				'password' => md5($password),
				
			);
			
			$this->m_donatur->updateuser($whereuser,$data);
	
		}
		
		$respon = array(

			'pesan' => "Data Berhasil Di Perbaharui",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		
       
    }
	
	public function add()
	{
		
		
		$namasantri = $this->input->post('namadonatur');
		$jeniskelamin = $this->input->post('jeniskelamin');
		$tempatlahir = $this->input->post('tempatlahir');
		$tanggallahir = $this->input->post('tanggallahir');
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$nohp = $this->input->post('nohp');
		$norekening = $this->input->post('norekening');
		$tanggaljoin = $this->input->post('tanggaljoin');
		$statusdonatur = $this->input->post('statusdonatur');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		
		
		if($tanggaljoin=='today'){
			
			$tanggaljoin=date("Y-m-d");
			
		}
		
		
		
		
		//set id donatur
		
		$jenisid="200"; //2000 adalah kode id untuk menandakan donatur
		$tahun=date("Y"); //untuk mendapatkan tahun.
		$ekor=10000;
		
		
		$jumlahdata=$this->m_donatur->get()->num_rows(); //mendapatkan jumlahdata
		
		if($jumlahdata==0){
				
			$noid=1; //jika data kosong no id = 1
			$noid=$ekor+$noid; //untuk mendapatkan angka 10001
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0001
			$iddonatur=$jenisid.$tahun.$noid; //membuat id donatur
		}else{
			
			$noid=$this->m_donatur->get()->last_row()->id_donatur; //mendapatkan id terkahir
			$noid=substr($noid,7);// mendapatkan 4 digit terkahir id dengan cara memotong sebanyak 7 karakter akan menjadi 0001
			$noid=$noid+1; //untuk mendapatkan data terbaru.
			$noid=$ekor+$noid; //untuk mendapatkan angka 1000X
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0002 dan seterusnya
			$iddonatur=$jenisid.$tahun.$noid; //membuat id donatur
		}	
		
		
		
		//selesai set id donatur
		
		
		
		
		//input ke tabel donatur
		$data = array(

			'id_donatur' => $iddonatur,
			'nm_donatur' => $namasantri,
			'kelamin' => $jeniskelamin,
			'tmp_lahir' => $tempatlahir,
			'tgl_lahir' => $tanggallahir,
			'alamat' => $alamat,
			'email' => $email,
			'no_telp' => $nohp,
			'no_rek' => $norekening,
			'status' => $statusdonatur,
			'tgl_join' => $tanggaljoin
			
		);
	
		$this->m_donatur->insert($data);
		
		
		//Membuat data user untuk santri 
		
		$jenisid="100"; //2000 adalah kode id untuk menandakan user
		
		$ekor=10000;
		
		
		$jumlahdata=$this->m_donatur->getuser()->num_rows(); //mendapatkan jumlahdata
		
		if($jumlahdata==0){
				
			$noid=1; //jika data kosong no id = 1
			$noid=$ekor+$noid; //untuk mendapatkan angka 10001
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0001
			$iduser=$jenisid.$noid; //membuat id user
		}else{
			
			$noid=$this->m_donatur->getuser()->last_row()->id_user; //mendapatkan id terkahir
			$noid=substr($noid,3);// mendapatkan 4 digit terkahir id dengan cara memotong sebanyak 7 karakter akan menjadi 0001
			$noid=$noid+1; //untuk mendapatkan data terbaru.
			$noid=$ekor+$noid; //untuk mendapatkan angka 1000X
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0002 dan seterusnya
			$iduser=$jenisid.$noid; //membuat id user
		}	
		
		
		
		$data = array(

			'id_user' => $iduser,
			'username' => $username,
			'password' => md5($password),
			'jenisuser' => 'donatur',
			'ref_id' => $iddonatur
			
			
		);
		
		
		$this->m_donatur->insertuser($data);
		
		$respon = array(

			'pesan' => "Data Berhasil Di Tambah",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
	}
	
	public function delete()
	{
		
		
		$iddonatur = $this->input->post('iddonatur');
		
		
		//delete di tabel donatur
		$where = array(

			'id_donatur' => $iddonatur
			
		);
		
		$this->m_donatur->delete($where);
		
		//delete di tabel user

		$whereuser = array(

			'ref_id' => $iddonatur
			
		);
		$this->m_donatur->deleteuser($whereuser);
		
		
		
		$respon = array(

			'pesan' => "Data Berhasil Di Hapus",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		  
	}
	
	
	
	
	
}
