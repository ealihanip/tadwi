<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaransyahriyah extends CI_Controller {
	function __construct(){
		
		parent::__construct();			
		$this->load->model('m_pembayaransyahriyah');
		
	}
	
	
	
	
	public function getsisatagihan(){
		$nis = $this->input->post('nis');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		
		// ambil data tagihan 
		$where = array(

			'NIS' => $nis,
			'bulan' => $bulan,
			'tahun' => $tahun
			
			
		);
	
		
		$jumlahdata=$this->m_pembayaransyahriyah->gettagihan($where)->num_rows();	
		
		if($jumlahdata==0){ 
			
			$data = array(

				'id_tagihan' => "",
				'NIS' => "",
				'bulan' => "",
				'tahun' => "",
				'makan' => "",
				'listrik' => "",
				'infaq' => "",
				'status' => "",
				'respon' => "error"
				
			);
		
			echo json_encode($data);
			
			
		}else{
		
		
		
			// ambil data tagihan 
			$where = array(

				'NIS' => $nis,
				'bulan' => $bulan,
				'tahun' => $tahun
				
				
			);
			$tagihan=$this->m_pembayaransyahriyah->gettagihan($where)->row();
			$idtagihan=$tagihan->id_tagihan;
			$tagihanmakan=$tagihan->makan;
			$tagihanlistrik=$tagihan->listrik;
			$tagihaninfaq=$tagihan->infaq;
			$statustagihan=$tagihan->status;
			
			// ambil data Pembayaran
			$where = array(

				'id_tagihan' => $idtagihan
				
				
			);
				
			$pembayaran=$this->m_pembayaransyahriyah->get($where)->num_rows();//untuk cek jumlah data
			
				
			if($pembayaran==0){//jika data belum ada maka 
					
				
					
			}else{//jika data sudah ada maka dilakukan proses perhitungan
					
					
					
					
				$pembayaran=$this->m_pembayaransyahriyah->get($where)->result();//mengambildata
				
				$pembayaranmakan=0; //variabel untuk menangkap data pembayaran makan total
				$pembayaranlistrik=0; //variabel untuk menangkap data pembayaran listrik total
				$pembayaraninfaq=0; //variabel untuk menangkap data pembayaran infaq total
				
				foreach ($pembayaran as $row) //pengulangan untuk mendapatkan total dari pembayaran pembayaran
				{	$pembayaranmakan=$pembayaranmakan+$row->pembayaran_makan;
					$pembayaranlistrik=$pembayaranlistrik+$row->pembayaran_listrik;
					$pembayaraninfaq=$pembayaraninfaq+$row->pembayaran_infaq;
					
				}
					
					$tagihanmakan=$tagihanmakan-$pembayaranmakan; //mengurangi tagihan dengan pembayaran yang sudah di lakukan
					$tagihanlistrik=$tagihanlistrik-$pembayaranlistrik; //mengurangi tagihan dengan pembayaran yang sudah di lakukan
					$tagihaninfaq=$tagihaninfaq-$pembayaraninfaq; //mengurangi tagihan dengan pembayaran yang sudah di lakukan
			}
			
		
			
			
			$data = array(

				'id_tagihan' => $tagihan->id_tagihan,
				'NIS' =>  $tagihan->NIS,
				'bulan' =>  $tagihan->bulan,
				'tahun' =>  $tagihan->tahun,
				'makan' =>  $tagihanmakan,
				'listrik' => $tagihanlistrik,
				'infaq' =>  $tagihaninfaq,
				'status' => $tagihan->status,
				'respon' => "success"
				
			
			);
		
			echo json_encode($data);
			
			
		}
		
		
	}
	
	
	public function getdata(){
			
		if($this->uri->segment('3')){
			$where = array(

				'tb_tagihan.NIS' => $this->uri->segment('3')
					
			);
	   }else{
		   
		  $where="";
	   }
        echo $this->m_pembayaransyahriyah->getdata($where);
    }
	
	
	public function add()
	{
		
		
		$nis = $this->input->post('nis');
		$bulan =$this->input->post('bulan');
		$tahun =$this->input->post('tahun');
		$makan =$this->input->post('makan');
		$listrik =$this->input->post('listrik');
		$infaq =$this->input->post('infaq');
		$tanggalbayar= $this->input->post('tanggalbayar');
		
		
		
		
		// ambil data tagihan 
		$where = array(

			'NIS' => $nis,
			'bulan' => $bulan,
			'tahun' => $tahun
			
			
		);
		$tagihan=$this->m_pembayaransyahriyah->gettagihan($where)->row();
		$idtagihan=$tagihan->id_tagihan;
		$tagihanmakan=$tagihan->makan;
		$tagihanlistrik=$tagihan->listrik;
		$tagihaninfaq=$tagihan->infaq;
		$statustagihan=$tagihan->status;
		
		
		// selesai ambil data tagihan 
		
		if($statustagihan=="Sudah Lunas"){//jika sudah lunas maka pembayaran tidak akan di lakukan
			
			$respon = array(

				'pesan' => "Tagihan Sudah Lunas",
				'status' => "error"
			
			);
		
			
		
			echo json_encode($respon);
			
		}else{
			
			
			// ambil data Pembayaran
			$where = array(

				'id_tagihan' => $idtagihan
				
				
			);
			
				$jumlahdata=$this->m_pembayaransyahriyah->get($where)->num_rows();// Mendapatkan jumlah data pembayaran
				
				if($jumlahdata==0){ //jika belum pernah membayar
					$pembayaranmakan=0; //variabel untuk menangkap data pembayaran makan total
					$pembayaranlistrik=0; //variabel untuk menangkap data pembayaran listrik total
					$pembayaraninfaq=0; //variabel untuk menangkap data pembayaran infaq total
					
					
					
				}else{ //jika sudah pernah membayar
					
					$pembayaran=$this->m_pembayaransyahriyah->get($where)->result();//mengambildata
					
					$pembayaranmakan=0; //variabel untuk menangkap data pembayaran makan total
					$pembayaranlistrik=0; //variabel untuk menangkap data pembayaran listrik total
					$pembayaraninfaq=0; //variabel untuk menangkap data pembayaran infaq total
					
					foreach ($pembayaran as $row) //pengulangan untuk mendapatkan total dari pembayaran
		{				$pembayaranmakan=$pembayaranmakan+$row->pembayaran_makan;
						$pembayaranlistrik=$pembayaranlistrik+$row->pembayaran_listrik;
						$pembayaraninfaq=$pembayaraninfaq+$row->pembayaran_infaq;
						
					}	
					
					
				}
				
				
					$tagihanmakan=$tagihanmakan-$pembayaranmakan; //mengurangi tagihan dengan pembayaran yang sudah di lakukan
					$tagihanlistrik=$tagihanlistrik-$pembayaranlistrik; //mengurangi tagihan dengan pembayaran yang sudah di lakukan
					$tagihaninfaq=$tagihaninfaq-$pembayaraninfaq; //mengurangi tagihan dengan pembayaran yang sudah di lakukan
					
				//membandingkan dengan total pembayaran dengan tagihan
				
				if($makan>$tagihanmakan){ //cek value ...jika inputan user yaitu makan lebih besar dari tagihan makan maka eror, variabel makan adalah variabel yang di inputkan user
					$respon = array(

						'pesan' => "Pembayaran Makan Melebihi Sisa Tagihan",
						'status' => "error"
				
					);
			
			
			
					echo json_encode($respon);
					
					
				}else{//jika tidak melebihi akan di lanjutkan
					
					$tagihanmakan=$tagihanmakan-$makan; //maka sisa tagihan di kurangi input dari user
					
					if($listrik>$tagihanlistrik){ //cek value ...jika inputan user yaitu listrik lebih besar dari tagihan listrik maka eror, variabel listrik adalah variabel yang di inputkan user
						$respon = array(

							'pesan' => "Pembayaran Listrik Melebihi Sisa Tagihan",
							'status' => "error"
					
						);
						echo json_encode($respon);
						
						
						
					}else{//jika tidak melebihi akan di lanjutkan
						
						$tagihanlistrik=$tagihanlistrik-$listrik; //maka sisa tagihan di kurangi input dari user
						
						if($infaq>$tagihaninfaq){//cek value ...jika inputan user yaitu infaq lebih besar dari tagihan infaq maka eror, variabel infaq adalah variabel yang di inputkan user
							
							$respon = array(

								'pesan' => "Pembayaran Infaq Melebihi Sisa Tagihan",
								'status' => "error"
						
							);

							echo json_encode($respon);
							
						}else{
							
							
							$tagihaninfaq=$tagihaninfaq-$infaq; //maka sisa tagihan di kurangi input dari user
							
							
							//Jika Cek Value Telah Di Lakukan Maka Data Akan Di Simpan
							

							//set id pembayaran

							$jenisid="500"; //500 adalah kode id untuk menandakan id pembayaran Syahriyah
							$tahun=date("Y"); //untuk mendapatkan tahun.
							$ekor=10000;


							$jumlahdata=$this->m_pembayaransyahriyah->get()->num_rows(); //mendapatkan jumlahdata

							if($jumlahdata==0){
									
								$noid=1; //jika data kosong no id = 1
								$noid=$ekor+$noid; //untuk mendapatkan angka 10001
								$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0001
								$idpembayaransyahriyah=$jenisid.$tahun.$noid; //membuat id pembayaran Syahriyah
							}else{
								
								$noid=$this->m_pembayaransyahriyah->get()->last_row()->id_pmbyrn2; //mendapatkan id terkahir
								$noid=substr($noid,7);// mendapatkan 4 digit terkahir id dengan cara memotong sebanyak 7 karakter akan menjadi 0001
								$noid=$noid+1; //untuk mendapatkan data terbaru.
								$noid=$ekor+$noid; //untuk mendapatkan angka 1000X
								$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0002 dan seterusnya
								$idpembayaransyahriyah=$jenisid.$tahun.$noid; //membuat id pembayaransyahriyah
							}	



							//selesai set id pembayaran




							//input ke tabel pembayaran
							$data = array(

								'id_pmbyrn2' => $idpembayaransyahriyah,
								'id_tagihan' => $idtagihan,
								'tgl_bayar' => $tanggalbayar,
								'pembayaran_makan' => $makan,
								'pembayaran_listrik' => $listrik,
								'pembayaran_infaq' => $infaq
								
							);

							$this->m_pembayaransyahriyah->insert($data);
							
							if($tagihanmakan==0 and $tagihanlistrik==0 and $tagihaninfaq==0){//jika semua tagihan bernilai 0 maka akan update di tabel tagihan kolom status menjadi lunas
			
							
								$where = array(

									'id_tagihan'=> $idtagihan
									
								);
								$data = array(

									'status' => "Sudah Lunas"
								);
								
								$this->m_pembayaransyahriyah->update($where,$data);
			
			
							}

							$respon = array(

								'pesan' => "Pembayaran Berhasil DI Lakukan",
								'status' => "success"
						
							);
							echo json_encode($respon);
							
						}
						
						
						
					}
					
					
					
				}
				
				
				
				
				
		

			
			
		}
		
		
		
		
		
		
	}
	
	
	
	public function delete()
	{
		
		$idpembayaran = $this->input->post('idpembayaran');
		
		$where = array(

			'id_pmbyrn1' => $idpembayaran
			
		);
		
		$this->m_pembayaransyahriyah->delete($where);
		  
	}
	
	
	
	
	
	
}