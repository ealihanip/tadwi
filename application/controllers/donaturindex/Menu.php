<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	
	function __construct(){
		
		parent::__construct();			
		
		
		if($this->session->userdata('jenis')=='donatur'){
			
		}else{
			
			redirect(base_url());
			
		}
	}
	

	public function getviewhome()
	{

		
		$this->load->view('pembangun/donatur/header');
		$this->load->view('pembangun/donatur/menutab');
		$this->load->view('pembangun/donatur/menukiri');
		$this->load->view('donatur/home');
		$this->load->view('pembangun/donatur/script');
		$this->load->view('pembangun/donatur/footer');
	}
	
	public function getviewprofil()
	{

		$data['pilmenukiri']="profil";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/donatur/header');
		$this->load->view('pembangun/donatur/menutab',$data);
		$this->load->view('pembangun/donatur/menukiri',$data);
		$this->load->view('donatur/profil');
		$this->load->view('pembangun/donatur/script');
		$this->load->view('pembangun/donatur/footer');
	}
	
	
	public function getvieweditprofil()
	{

		$data['pilmenukiri']="profil";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/donatur/header');
		$this->load->view('pembangun/donatur/menutab',$data);
		$this->load->view('pembangun/donatur/menukiri');
		$this->load->view('donatur/editprofil',$data);
		$this->load->view('pembangun/donatur/script');
		$this->load->view('pembangun/donatur/footer');
	}
	
	public function getviewhistoridonasi()
	{

		$data{'pilmenukiri'}="historidonasi";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/donatur/header');
		$this->load->view('pembangun/donatur/menutab',$data);
		$this->load->view('pembangun/donatur/menukiri');
		$this->load->view('donatur/historidonasi',$data);
		$this->load->view('pembangun/donatur/script');
		$this->load->view('pembangun/donatur/footer');
	}
	public function getviewtransaksidonasi()
	{

		$data{'pilmenukiri'}="transaksidonasi";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/donatur/header');
		$this->load->view('pembangun/donatur/menutab',$data);
		$this->load->view('pembangun/donatur/menukiri',$data);
		$this->load->view('donatur/transaksidonasi');
		$this->load->view('pembangun/donatur/script');
		$this->load->view('pembangun/donatur/footer');
	}
	
	
	public function getviewsuksestransaksidonasi()
	{

		$data{'pilmenukiri'}="transaksidonasi";
		$data['id']=$this->session->userdata('id');
		$this->load->view('pembangun/donatur/header');
		$this->load->view('pembangun/donatur/menutab',$data);
		$this->load->view('pembangun/donatur/menukiri',$data);
		$this->load->view('donatur/suksestransaksidonasi');
		$this->load->view('pembangun/donatur/script');
		$this->load->view('pembangun/donatur/footer');
	}
	
	public function logout()
	{

		$data = array(
			'id'  => "",
			'username' => "",
			'jenis' => ""
		);
		
		$this->session->set_userdata($data);
		
		redirect(base_url());
	}
	
	
	
	
}
