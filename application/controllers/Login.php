<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		
		parent::__construct();			
		$this->load->model('m_login');
		
	}
	
	
	
	public function login()
	{
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$password = md5($password);
		
		$where = array(

			'username' => $username,
			
			
		);
		
		$jumlahdata=$this->m_login->get($where)->num_rows();
		$hasil=$this->m_login->get($where);
		$row = $hasil->row();
		
		if($jumlahdata==1){ //cek username ada atau tidak
			if($row->password==$password){// cek password
				
				if($row->jenisuser=='admin'){
					
					$data = array(
						'id'  => $row->ref_id,
						'username' => $row->username,
						'jenis' => $row->jenisuser
					);

					$this->session->set_userdata($data);
					
					
					$respon = array(

					'pesan' => 'Selamat Datang '.$this->session->userdata('username'),
					'status' => "success",
					'redirto' => base_url('admin')
					
				
					);
				
					echo json_encode($respon);
					
				}
					
				if($row->jenisuser=='donatur'){
					
					$data = array(
						'id'  => $row->ref_id,
						'username' => $row->username,
						'jenis' => $row->jenisuser
					);

					$this->session->set_userdata($data);
					
					
					$respon = array(

					'pesan' => 'Selamat Datang '.$this->session->userdata('username'),
					'status' => "success",
					'redirto' => base_url('donatur')
					
				
					);
				
					echo json_encode($respon);
					
				}
				
				
				if($row->jenisuser=='santri'){
					
					$data = array(
						'id'  => $row->ref_id,
						'username' => $row->username,
						'jenis' => $row->jenisuser
					);

					$this->session->set_userdata($data);
					
					
					$respon = array(

					'pesan' => 'Selamat Datang '.$this->session->userdata('username'),
					'status' => "success",
					'redirto' => base_url('walisantri')
					
				
					);
				
					echo json_encode($respon);
					
				}
					
					
				
				
				
				
				
				
					
			}else{
				
				$respon = array(

					'pesan' => "Password Salah",
					'status' => "error"
				
				);
				
				echo json_encode($respon);
			}
				
			
		}else{
			
			$respon = array(

					'pesan' => 'Username Tidak Di Temukan',
					'status' => "error"
				
			);
			
			echo json_encode($respon);
		}
		
		
		
		
	}
	
}
