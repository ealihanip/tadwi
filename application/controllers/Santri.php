<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Santri extends CI_Controller {
	function __construct(){
		
		parent::__construct();			
		$this->load->model('m_santri');
		$this->load->library('upload');
		$this->load->library('image_lib');
	}
	
	
	
	public function get(){
		$nis = $this->input->post('nis');
		
		$where = array(

			'NIS' => $nis
			
		);
		
		$whereuser = array(

			'ref_id' => $nis
			
		);
		
		
		$hasil=$this->m_santri->get($where);
		$hasiluser=$this->m_santri->getuser($whereuser);
		$jumlahdata=$this->m_santri->get($where)->num_rows();
		
		//jika data tidak di temukan makan akan mengirim respon kosong
		if($jumlahdata==0){ 
			
			$data = array(

				'NIS' => "",
				'nm_santri' => "",
				'kelamin' => "",
				'tmp_lahir' => "",
				'tgl_lahir' => "",
				'alamat' => "",
				'no_hp' => "",
				'angkatan' => "",
				'nm_ortu' => "",
				'dirfoto' => "",
				'foto' => "",
				'username' => "",
				'password' => ""
			
			);
		
			echo json_encode($data);
			
			
		}else{
			
			$row = $hasil->row();
			$rowuser=$hasiluser->row();
			$alamatfoto=base_url('upload/fotosantri/');
			$data = array(

				'NIS' => $row->NIS,
				'nm_santri' => $row->nm_santri,
				'kelamin' => $row->kelamin,
				'tmp_lahir' => $row->tmp_lahir,
				'tgl_lahir' => $row->tgl_lahir,
				'alamat' => $row->alamat,
				'no_hp' => $row->no_hp,
				'angkatan' => $row->angkatan,
				'nm_ortu' => $row->nm_ortu,
				'dirfoto' => base_url('upload/fotosantri/').$row->foto,
				'foto' => $row->foto,
				'username' => $rowuser->username,
				'password' => $rowuser->password
			
			);
		
			echo json_encode($data);
			
			
		}
		
		
	}
	
	
	public function getdata(){
		
		
	  
	   
       if($this->uri->segment('3')){
			$where = array(

				'NIS' => $this->uri->segment('3')
					
			);
	   }else{
		   
		  $where = "";
		   
	   }
       


	   echo $this->m_santri->getdata($where);
    }
	
	
	public function update(){
		
		$nis = $this->input->post('nis');
		$namasantri = $this->input->post('namasantri');
		$jeniskelamin = $this->input->post('jeniskelamin');
		$tempatlahir = $this->input->post('tempatlahir');
		$tanggallahir = $this->input->post('tanggallahir');
		$alamat = $this->input->post('alamat');
		$nohp = $this->input->post('nohp');
		$angkatan = $this->input->post('angkatan');
		$namaortu = $this->input->post('namaortu');
		$namafoto = $this->input->post('namafoto');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$prepassword = $this->input->post('prepassword');
		
			
		if($namafoto==""){
			
			
			$config['upload_path']= './upload/fotosantri';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name']=$nis;
			
		   
			$this->upload->initialize($config);
			
			
			
			$this->upload->do_upload('foto');
			
			
			
			$gambar=$this->upload->data('full_path');
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = $gambar;
			$config['new_image'] = $gambar;
			
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 500;
			$config['height']       = 500;

			$this->image_lib->initialize($config);
			
			
			$this->image_lib->resize();
		
			//upload gambar	
		
			$foto=$this->upload->data('file_name');
        
			
		
		}else{
			
			$foto=$namafoto;
			
			
		}
		
		
		
		//update ke tabel santri
		$where = array(

			'NIS' => $nis
			
		);
		$data = array(

			'nm_santri' => $namasantri,
			'kelamin' => $jeniskelamin,
			'tmp_lahir' => $tempatlahir,
			'tgl_lahir' => $tanggallahir,
			'alamat' => $alamat,
			'no_hp' => $nohp,
			'angkatan' => $angkatan,
			'nm_ortu' => $namaortu,
			'foto' => $foto
			
		);
		
		$this->m_santri->update($where,$data);
		
		
		//update ke tabel user
		if($password!=$prepassword){//jika password tidak sama dengan sebelumnya
			
			
			$whereuser = array(

				'ref_id' => $nis
				
			);
			
			$data = array(

				'username' => $username,
				'password' => md5($password),
				
			);
			
			$this->m_santri->updateuser($whereuser,$data);
	
		}
		
		
		$respon = array(

			'pesan' => "Data Berhasil Di Perbaharui",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		
       
    }
	
	
	public function add()
	{
		
		$nis = $this->input->post('nis');
		$namasantri = $this->input->post('namasantri');
		$jeniskelamin = $this->input->post('jeniskelamin');
		$tempatlahir = $this->input->post('tempatlahir');
		$tanggallahir = $this->input->post('tanggallahir');
		$alamat = $this->input->post('alamat');
		$nohp = $this->input->post('nohp');
		$angkatan = $this->input->post('angkatan');
		$namaortu = $this->input->post('namaortu');
		$namafoto = $this->input->post('namafoto');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		//upload gambar
		
		if($namafoto==""){
			
			$foto="kosong.jpg";
		
		}else{
			
			$config['upload_path']= './upload/fotosantri';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name']=$nis;
			
		   
			$this->upload->initialize($config);
			
			
			
			$this->upload->do_upload('foto');
			
			
			
			$gambar=$this->upload->data('full_path');
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = $gambar;
			$config['new_image'] = $gambar;
			
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 500;
			$config['height']       = 500;

			$this->image_lib->initialize($config);
			
			
			$this->image_lib->resize();
		
			//upload gambar	
		
			$foto=$this->upload->data('file_name');
			
			
		}
		
		
		
		//input ke tabel Sabtru
		$data = array(

			'NIS' => $nis,
			'nm_santri' => $namasantri,
			'kelamin' => $jeniskelamin,
			'tmp_lahir' => $tempatlahir,
			'tgl_lahir' => $tanggallahir,
			'alamat' => $alamat,
			'no_hp' => $nohp,
			'angkatan' => $angkatan,
			'nm_ortu' => $namaortu,
			'foto' => $foto
			
		);
		
		$this->m_santri->insert($data);
		
		
		//Membuat data user untuk santri 
		
		$jenisid="100"; //2000 adalah kode id untuk menandakan user
		
		$ekor=10000;
		
		
		$jumlahdata=$this->m_santri->getuser()->num_rows(); //mendapatkan jumlahdata
		
		if($jumlahdata==0){
				
			$noid=1; //jika data kosong no id = 1
			$noid=$ekor+$noid; //untuk mendapatkan angka 10001
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0001
			$iduser=$jenisid.$noid; //membuat id user
		}else{
			
			$noid=$this->m_santri->getuser()->last_row()->id_user; //mendapatkan id terkahir
			$noid=substr($noid,3);// mendapatkan 4 digit terkahir id dengan cara memotong sebanyak 7 karakter akan menjadi 0001
			$noid=$noid+1; //untuk mendapatkan data terbaru.
			$noid=$ekor+$noid; //untuk mendapatkan angka 1000X
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0002 dan seterusnya
			$iduser=$jenisid.$noid; //membuat id user
		}	
		
		
		
		$data = array(

			'id_user' => $iduser,
			'username' => $username,
			'password' => md5($password),
			'jenisuser' => 'santri',
			'ref_id' => $nis
			
			
		);
		
		
		$this->m_santri->insertuser($data);
		
		$respon = array(

			'pesan' => "Data Berhasil Di Tambah",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
	}
	
	public function delete()
	{
		
		$nis = $this->input->post('nis');
		
		
		//delete di tabel santri
		$where = array(

			'NIS' => $nis
			
		);
		$hasil=$this->m_santri->get($where);
		
		$row = $hasil->row();
		unlink('upload/fotosantri/'.$row->foto);
		$this->m_santri->delete($where);
		
		
		
		//delete di tabel user

		$whereuser = array(

			'ref_id' => $nis
			
		);
		$this->m_santri->deleteuser($whereuser);
		
		
		$respon = array(

			'pesan' => "Data Berhasil Di Hapus",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		
		
	}
	
	
	
	
	
	
	
	
	
	
}
