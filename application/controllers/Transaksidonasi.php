<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksidonasi extends CI_Controller {
	function __construct(){
		
		parent::__construct();			
		$this->load->model('m_transaksidonasi');
		$this->load->library('upload');
		$this->load->library('image_lib');
	}
	
	
	public function getdata(){
		
		
		
		if($this->uri->segment('3')){
			$where = array(

				'tb_donatur.id_donatur' => $this->uri->segment('3')
					
			);
	   }else{
		   
		  $where="";
	   }
	   
	   
        
        echo $this->m_transaksidonasi->getdata($where);
    }
	
	
	public function add()
	{
		
		$iddonatur = $this->input->post('iddonatur');
		$tanggaldonasi = $this->input->post('tanggaldonasi');
		$jumlahdonasi = $this->input->post('jumlahdonasi');
		
		$transaksi = $this->input->post('transaksi');
		$statusverifikasi = $this->input->post('statusverifikasi');
		
		
		
		
	
	
		
		//set id transaksidonasi
		
		$jenisid="700"; //700 adalah kode id untuk menandakan id transaksidonasi 
		$tahun=date("Y"); //untuk mendapatkan tahun.
		$ekor=10000;
		
		
		$jumlahdata=$this->m_transaksidonasi->get()->num_rows(); //mendapatkan jumlahdata
		
		if($jumlahdata==0){
				
			$noid=1; //jika data kosong no id = 1
			$noid=$ekor+$noid; //untuk mendapatkan angka 10001
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0001
			$idtrans=$jenisid.$tahun.$noid; //membuat id transaksi donasi
			
		}else{
			
			$noid=$this->m_transaksidonasi->get()->last_row()->id_trans; //mendapatkan id terkahir
			$noid=substr($noid,7);// mendapatkan 4 digit terkahir id dengan cara memotong sebanyak 7 karakter akan menjadi 0001
			$noid=$noid+1; //untuk mendapatkan data terbaru.
			$noid=$ekor+$noid; //untuk mendapatkan angka 1000X
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0002 dan seterusnya
			$idtrans=$jenisid.$tahun.$noid; //membuat id transaksidonasi
		}	
		
		
		
		//selesai set id transaksidonasi
		
		
		
		
		//input ke tabel transaksi donasi
		$data = array(

			'id_trans' => $idtrans,
			'id_donatur' => $iddonatur,
			'tgl_donasi' => $tanggaldonasi,
			
			'jml_donasi' => $jumlahdonasi,
			'transaksi' => $transaksi,
			'statverifikasi' => $statusverifikasi
			
		);
		
		$this->m_transaksidonasi->insert($data);
		
		
		
		$respon = array(

			'pesan' => "Transaksi Donasi Telah Berhasil Di Simpan",
			'status' => "success"
		
		);
		
		echo json_encode($respon);
			
		
			
			
		
		
		
		
		
	}
	
	
	
	public function delete()
	{
		
		$idtransaksi = $this->input->post('idtransaksi');
		
		$where = array(

			'id_trans' => $idtransaksi
			
		);
		
		$this->m_transaksidonasi->delete($where);
		  
	}
	
	
	public function update()
	{
		
		$idtransaksi = $this->input->post('idtransaksi');
		
		$where = array(

			'id_trans' => $idtransaksi,
			'statverifikasi' => "belum"
			
		);
		
		$jumlahdata=$this->m_transaksidonasi->get($where)->num_rows(); //cek sudah terverifikasi atau belum
		
		if($jumlahdata==1){
			$where = array(

				'id_trans' => $idtransaksi
			
			);
		
			$data = array(

				'statverifikasi' => 'sudah'
			
			);
		
			$this->m_transaksidonasi->update($where,$data);
			
			$respon = array(

				'pesan' => "Transaksi Telah Berhasil di verifikasi",
				'status' => "success"
			
			);
			
			echo json_encode($respon);
			
		}else{
		
			$respon = array(

				'pesan' => "Gagal, karena data sudah terverifikasi",
				'status' => "error"
			
			);
			
			echo json_encode($respon);
		
		
		}
		
		  
	}
	
	
	
	
	
	
}
