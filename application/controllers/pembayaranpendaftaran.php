<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaranpendaftaran extends CI_Controller {
	function __construct(){
		
		parent::__construct();			
		$this->load->model('m_pembayaranpendaftaran');
		$this->load->library('upload');
		$this->load->library('image_lib');
	}
	
	
	public function getdata(){
		
        $where = array(

			'tb_pbr_pdftrn.NIS' => "",
			
		);
        echo $this->m_pembayaranpendaftaran->getdata($where);
    }
	
	
	public function add()
	{
		
		$nis = $this->input->post('nis');
		$tanggalbayar = $this->input->post('tanggalbayar');
		$totalbayar = $this->input->post('totalbayar');
		
		
		
		
		$where = array(

			'NIS' => $nis
			
		);
		
		
		$jumlahdata=$this->m_pembayaranpendaftaran->get($where)->num_rows();
		if($jumlahdata>0){ //jika data kosong maka proses penyimpanan data akan di lakukan
			
			$respon = array(

				'pesan' => "Data Tidak Berhasil Di Simpan Karena Santri Dengan Nis Tersebut Telah Membayar Pendaftaran",
				'status' => "error"
			
			);
			
			echo json_encode($respon);
			
			
		}else{
			
			//set id donatur
			
			$jenisid="600"; //800 adalah kode id untuk menandakan id pembayaran pendataran 
			$tahun=date("Y"); //untuk mendapatkan tahun.
			$ekor=10000;
			
			
			$jumlahdata=$this->m_pembayaranpendaftaran->get()->num_rows(); //mendapatkan jumlahdata
			
			if($jumlahdata==0){
					
				$noid=1; //jika data kosong no id = 1
				$noid=$ekor+$noid; //untuk mendapatkan angka 10001
				$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0001
				$idpembayaranpendaftaran=$jenisid.$tahun.$noid; //membuat id pembayaran pendaftaran
			}else{
				
				$noid=$this->m_pembayaranpendaftaran->get()->last_row()->id_pmbyrn1; //mendapatkan id terkahir
				$noid=substr($noid,7);// mendapatkan 4 digit terkahir id dengan cara memotong sebanyak 7 karakter akan menjadi 0001
				$noid=$noid+1; //untuk mendapatkan data terbaru.
				$noid=$ekor+$noid; //untuk mendapatkan angka 1000X
				$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0002 dan seterusnya
				$idpembayaranpendaftaran=$jenisid.$tahun.$noid; //membuat id donatur
			}	
			
			
			
			//selesai set id donatur
			
			
			
			
			//input ke tabel donatur
			$data = array(

				'id_pmbyrn1' => $idpembayaranpendaftaran,
				'NIS' => $nis,
				'tgl_bayar' => $tanggalbayar,
				'total_bayar' => $totalbayar
				
			);
			
			$this->m_pembayaranpendaftaran->insert($data);
			
			
			
			$respon = array(

				'pesan' => "Transaksi Pembayaran Pendaftaran Telah Berhasil Di Simpan",
				'status' => "success"
			
			);
			
			echo json_encode($respon);
			
		
			
			
		}
		
		
		
		
	}
	
	
	
	public function delete()
	{
		
		$idpembayaran = $this->input->post('idpembayaran');
		
		$where = array(

			'id_pmbyrn1' => $idpembayaran
			
		);
		
		$this->m_pembayaranpendaftaran->delete($where);
		  
	}
	
	
	
	
	
	
}
