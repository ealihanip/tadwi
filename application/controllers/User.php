<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct(){
		
		parent::__construct();			
		$this->load->model('m_user');
		
	}
	
	
	public function cekusername()
	{
		$username = $this->input->post('username');
		
		$where = array(

			'username' => $username
			
		);
		
		$jumlahdata=$this->m_user->get($where)->num_rows();
		
		if($jumlahdata==0){ //jika username tidak di temukan makan jumlah data - 0 dan mengirim respon sukses
			$respon = array(

					'status' => "success"
				
			);
				
			echo json_encode($respon);
			
			
		}else{//jika ada maka
			$respon = array(

					'status' => "error"
				
			);
				
			echo json_encode($respon);
			
			
		}
		
			
	}
		
	public function add()
	{
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		//Membuat data user untuk santri 
		
		$jenisid="100"; //2000 adalah kode id untuk menandakan user
		
		$ekor=10000;
		
		
		$jumlahdata=$this->m_user->get()->num_rows(); //mendapatkan jumlahdata
		
		if($jumlahdata==0){
				
			$noid=1; //jika data kosong no id = 1
			$noid=$ekor+$noid; //untuk mendapatkan angka 10001
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0001
			$iduser=$jenisid.$noid; //membuat id user
		}else{
			
			$noid=$this->m_user->get()->last_row()->id_user; //mendapatkan id terkahir
			$noid=substr($noid,3);// mendapatkan 4 digit terkahir id dengan cara memotong sebanyak 7 karakter akan menjadi 0001
			$noid=$noid+1; //untuk mendapatkan data terbaru.
			$noid=$ekor+$noid; //untuk mendapatkan angka 1000X
			$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0002 dan seterusnya
			$iduser=$jenisid.$noid; //membuat id user
		}	
		
		
		$data = array(

			'id_user' => $iduser,
			'username' => $username,
			'password' => md5($password),
			'jenisuser' => 'admin',
			'ref_id' => 'admin'
			
			
		);
		
		$this->m_user->insert($data);
		
		$respon = array(

			'pesan' => "Data User Di Simpan",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		
	}
		
	public function get(){
		$iduser =  $this->input->post('iduser');
		
		$where = array(

			'id_user' => $iduser
			
		);
		
		
		
		$hasil=$this->m_user->get($where);
		
			
		$row = $hasil->row();
	
		$data = array(
	
			'id_user' => $row->id_user,
			'username' => $row->username,
			'password' => $row->password
		
		);
	
		echo json_encode($data);
		
	
		
		
		
		
	}
	public function update(){
		
		$iduser = $this->input->post('iduser');
		$where = array(

			'id_user' => $iduser
			
		);
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$prepassword = $this->input->post('prepassword');
		
		
		if($password!=$prepassword){
			
			$data = array(

				'username' => $username,
				'password' => md5($password),
				
			);
			
		}else{
			
			$data = array(

				'username' => $username,
				'password' => $password,
				
			);
			
			
		}
			
			
		$this->m_user->update($where,$data);
		
		$respon = array(

			'pesan' => "Data User Di Update",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		
       
    }
	
	public function getdata(){
		
		if($this->uri->segment('3')){
			$where = array(

				'jenisuser' => $this->uri->segment('3')
					
			);
		}else{
		   
			$where = "";
		   
		}
       


	   echo $this->m_user->getdata($where);
		
		
	}
	
	public function delete()
	{
		
		
		$iduser = $this->input->post('iduser');
		
		$where = array(

			'id_user' => $iduser
			
		);
		
		
		
		$hasil=$this->m_user->get($where);
		
			
		$row = $hasil->row();
		
		if($row->username=="admin"){
			
			$where = array(

				'id_user' => $iduser
				
			);
			
			$this->m_user->delete($where);
		
			$respon = array(

				'pesan' => "Data Berhasil Di Hapus",
				'status' => "success"
			
			);
				
			echo json_encode($respon);
			
		}else{
			
			$respon = array(

				'pesan' => "Data Tidak Di Hapus, Hanya Data Admin Yang Dapat Di Hapus",
				'status' => "error"
			
			);
				
			echo json_encode($respon);
			
			
		}
		
		
		
			
	}
	
}
