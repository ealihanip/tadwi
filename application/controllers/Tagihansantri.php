<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihansantri extends CI_Controller {
	function __construct(){
		
		parent::__construct();			
		$this->load->model('m_tagihansantri');
		
	}
	public function get(){
		$nis = $this->input->post('nis');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		
		$where = array(

			'NIS' => $nis,
			'bulan' => $bulan,
			'tahun' => $tahun
			
		);
		
		
		
		
		$hasil=$this->m_tagihansantri->get($where);
		$jumlahdata=$this->m_tagihansantri->get($where)->num_rows();
		
		//jika data tidak di temukan makan akan mengirim respon kosong
		if($jumlahdata==0){ 
			
			$data = array(

				'id_tagihan' => "",
				'NIS' => "",
				'bulan' => "",
				'tahun' => "",
				'makan' => "",
				'listrik' => "",
				'infaq' => "",
				'status' => "",
				'respon' => "error"
				
			);
		
			echo json_encode($data);
			
			
		}else{
			
			$row = $hasil->row();
			
			$data = array(

				'id_tagihan' => $row->id_tagihan,
				'NIS' =>  $row->NIS,
				'bulan' =>  $row->bulan,
				'tahun' =>  $row->tahun,
				'makan' =>  $row->makan,
				'listrik' =>  $row->listrik,
				'infaq' =>  $row->infaq,
				'status' =>  $row->status,
				'respon' => "success"
				
			
			);
		
			echo json_encode($data);
			
			
		}
		
		
	}
	
	public function getdata(){
		if($this->uri->segment('3')){
			$where = array(

				'tb_tagihan.NIS' => $this->uri->segment('3')
					
			);
	   }else{
		   
		  $where="";
	   }
	   
       echo $this->m_tagihansantri->getdata($where);
    }
	
	
	public function add()
	{
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		
		
		$where = array(

			'bulan' => $bulan,
			'tahun' => $tahun
			
			
		);
		
		$jumlahdata=$this->m_tagihansantri->get($where)->num_rows();//cek apakah data ada atau tidak ada 
		
		if($jumlahdata==0){ //jika data tidak ada maka lanjutkan perintah 
			
			
			$santri=$this->m_tagihansantri->getdatasantri();
			
			foreach ($santri->result() as $row){
				
				//set id tagihan
				
				$jenisid="300"; //300 adalah kode id untuk menandakan id Tagihan
				$tahun=date("Y"); //untuk mendapatkan tahun.
				$ekor=10000;
				
				
				$jumlahdata=$this->m_tagihansantri->get()->num_rows(); //mendapatkan jumlahdata
				
				if($jumlahdata==0){
						
					$noid=1; //jika data kosong no id = 1
					$noid=$ekor+$noid; //untuk mendapatkan angka 10001
					$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0001
					$idtagihan=$jenisid.$tahun.$noid; //membuat id pembayaran tagihan
				}else{
					
					$noid=$this->m_tagihansantri->get()->last_row()->id_tagihan; //mendapatkan id terkahir
					$noid=substr($noid,7);// mendapatkan 4 digit terkahir id dengan cara memotong sebanyak 7 karakter akan menjadi 0001
					$noid=$noid+1; //untuk mendapatkan data terbaru.
					$noid=$ekor+$noid; //untuk mendapatkan angka 1000X
					$noid=substr($noid,1);// menghilangkan angka 1 di depan dan mengubah menjadi string 0002 dan seterusnya
					$idtagihan=$jenisid.$tahun.$noid; //membuat id tagihan
				}	
				
				
				
				//selesai set id tagihan
				
				
				
				
				
				$data = array(

					'id_tagihan' =>$idtagihan,
					'NIS' => $row->NIS,
					'bulan' => $bulan,
					'tahun' => $tahun,
					'makan' => "330000",
					'listrik' => "50000",
					'infaq' => "20000",
					'status' => "Belum Lunas"
				
				);
				$this->m_tagihansantri->insert($data);
			}
			
			$respon = array(

				'pesan' => "Data Tagihan Telah Berhasil Di Buat",
				'status' => "success"
			
			);
			
			echo json_encode($respon);
			
			
			
			
		}else{ //jika ada kirim respon eror
			
			$respon = array(

				'pesan' => "Data Tidak Di Buat Karena Telah Ada, Coba Dengan Bulan Atau Tahun Yang Lain",
				'status' => "error"
			
			);
			
			echo json_encode($respon);
			
			
		}
		
		
		
		
		
	}
	
	
	
}
