<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelolaweb extends CI_Controller {
	
	function __construct(){
		
		parent::__construct();			
		
		$this->load->model('m_kelolaweb');
	}
	

	public function get()
	{
		$namakonten = $this->input->post('namakonten');
		
		$where = array(

			'namakonten' => $namakonten
			
		);
		
		$data['konten']=$this->m_kelolaweb->get($where)->result();
		
		echo json_encode($data);
	}
	
	public function update(){
		
		$namakonten = $this->input->post('namakonten');
		
		$isikonten = $this->input->post('isikonten');
		
		$where = array(

			'namakonten' => $namakonten
			
		);
		$data = array(

			
			'isikonten' => $isikonten
			
			
		);
		
		
		$this->m_kelolaweb->update($where,$data);
		
		$respon = array(

			'pesan' => "Konten Sudah Di Perbaharui",
			'status' => "success"
			
		);
		
		echo json_encode($respon);
		
		
		
       
    }
	
	
	
}
