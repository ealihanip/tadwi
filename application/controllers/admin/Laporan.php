<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	
	function __construct(){
		
		parent::__construct();			
		
		$this->load->model('m_laporan');
		$this->load->library('pdf');
		
		
	}
	

	
	
	public function getlaporan()
	{
		
		$table="";
		$data['judul'] = '';
		
		
		if($this->uri->segment('3')=="laporan-data-santri"){
			$table="tb_santri";
			$data['judul'] = 'Laporan Data Santri';
			
			
			
			if($this->uri->segment('4')=="semua-angkatan"){// URI SEGMEN 4 untuk angkatan
				
				$angkatan="";
				
			}else{
				
				$angkatan=$this->uri->segment('4');
				
			}
			
			
			if($this->uri->segment('5')=="semua-jenis-kelamin"){// URI SEGMEN 4 untuk Jenis Kelamin
				
				$jeniskelamin="";
				
			}else{
				
				$jeniskelamin=$this->uri->segment('5');
				
			}
			
			
			$where = array(
			
				'angkatan'  => $angkatan,
				'kelamin'  => $jeniskelamin
				
				
			);
			
			$data['data'] = $this->m_laporan->get($where,$table)->result();
		}
		
		if($this->uri->segment('3')=="laporan-data-donatur"){
			$table="tb_donatur";
			$data['judul'] = 'Laporan Data Donatur';
			
			$where="";
			$data['data'] = $this->m_laporan->get($where,$table)->result();
		}
		
		if($this->uri->segment('3')=="laporan-data-tagihan"){
		
			$data['judul'] = 'Laporan Data Tagihan';
			$status="";
			$tahun="";
			$bulan="";
			
			if($this->uri->segment('4')=="semua-status"){// URI SEGMEN 4 untuk Status
				
				$status="";
				
			}else{
				
				if($this->uri->segment('4')=="sudah-lunas"){
					
					$status="Sudah Lunas";
				}
				
				if($this->uri->segment('4')=="belum-lunas"){
					
					$status="Belum Lunas";
				}
				
			}
			
			if($this->uri->segment('5')=="semua-tahun"){// URI SEGMEN 4 untuk tahun
				
				$tahun="";
				
			}else{
				
				$tahun=$this->uri->segment('5');
				
			}
			
			if($this->uri->segment('6')=="semua-bulan"){// URI SEGMEN 4 untuk tahun
				
				$bulan="";
				
			}else{
				
				$bulan=$this->uri->segment('6');
				
			}
			
			$where = array(
			
				'status'  => $status,
				'tahun'  => $tahun,
				'bulan'  => $bulan
				
				
			);
			
			$data['data'] = $this->m_laporan->gettagihan($where)->result();
			
			
		}
		
		if($this->uri->segment('3')=="laporan-data-pembayaran-pendaftaran"){
			
			$data['judul'] = 'Laporan Data Pembayaran Pendaftaran';
			
			
			if($this->uri->segment('4')=="semua-angkatan"){// URI SEGMEN 4 untuk angkatan
				
				$angkatan="";
				
			}else{
				
				$angkatan=$this->uri->segment('4');
				
			}
			
			if($this->uri->segment('5')=="semua-status-pembayaran"){// URI SEGMEN 4 untuk status pembayaran
				
				$idpembayaran="";
				
			}
			
			if($this->uri->segment('5')=="sudah-bayar"){// URI SEGMEN 5 untuk sudah bayar
				
				$idpembayaran="600";
				
			}
			
			if($this->uri->segment('5')=="belum-bayar"){// URI SEGMEN 5 untuk belum bayar
				
				$idpembayaran="Belum Bayar";
				
			}
			
			
			
			$where = array(
			
				'tb_santri.angkatan'  => $angkatan,
				"IFNULL(tb_pbr_pdftrn.`id_pmbyrn1`,'Belum Bayar')"  => $idpembayaran,
				
			);
			
			$data['data'] = $this->m_laporan->getpembayaranpendaftaran($where)->result();
			
			
			
		}
		
		if($this->uri->segment('3')=="laporan-data-pembayaran-syahriyah"){
			$table="tb_santri";
			$data['judul'] = 'Laporan Data Pembayaran Syahriyah';
			
			
			if($this->uri->segment('4')=="semua-angkatan"){// URI SEGMEN 4 untuk angkatan
				
				$angkatan="";
				
			}else{
				
				$angkatan=$this->uri->segment('4');
				
			}
			
			
			
			if($this->uri->segment('5')=="semua-tahun"){// URI SEGMEN 4 untuk angkatan
				
				$tahun="";
				
			}else{
				
				$tahun=$this->uri->segment('5');
				
			}
			
			if($this->uri->segment('6')=="semua-bulan"){// URI SEGMEN 4 untuk angkatan
				
				$bulan="";
				
			}else{
				
				$bulan=$this->uri->segment('6');
				
			}
			
			
			
			
			$where = array(
			
				'angkatan'  => $angkatan,
				'MONTH(tgl_bayar)'  => $bulan,
				'YEAR(tgl_bayar)'  => $tahun
				
				
			);
			
			
			
			$data['data'] = $this->m_laporan->getlaporanpembayarabsyahriyah($where,$table)->result();
			
		
		}
		
		if($this->uri->segment('3')=="laporan-data-pemasukan-dana-donatur"){
		
			$data['judul'] = 'Laporan Data Pemasukan Dana Donatur';
			
			
			
			if($this->uri->segment('4')=="semua-tahun"){// URI SEGMEN 4 untuk tahun
				
				$tahun="";
				
			}else{
				
				$tahun=$this->uri->segment('4');
				
			}
			
			if($this->uri->segment('5')=="semua-bulan"){// URI SEGMEN 4 untuk bulan
				
				$bulan="";
				
			}else{
				
				$bulan=$this->uri->segment('5');
				
			}
			
			
			
			
			$where = array(
			
				
				'MONTH(tgl_donasi)'  => $bulan,
				'YEAR(tgl_donasi)'  => $tahun
				
				
			);
			
			
			
			$data['data'] = $this->m_laporan->getlaporandanadonasi($where)->result();
			
			
		
		}
		
		
		
		
		
		
		
	
		$title_page = $data['judul'];
		$html=$this->load->view('admin/laporan/laporan',$data,true);
		$this->pdf->pdf_create($html,$title_page,'legal','landscape',"false");
		
		
		
		
		
		
	}
	
	
	
	
	
}
