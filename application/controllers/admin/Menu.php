<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	
	function __construct(){
		
		parent::__construct();			
		
		$this->load->library(array('ckeditor','ckfinder')); //library ckeditor
		
		if($this->session->userdata('jenis')=='admin'){
			
		}else{
			
			redirect(base_url());
			
		}
	}
	

	public function getviewhome()
	{

		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab');
		$this->load->view('admin/home');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	public function getviewuser()
	{
		
		
		$data{'pilmenutab'}="user";
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('admin/datauser');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	public function getviewdonatur()
	{
		$data{'pilmenutab'}="datadonatur";
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('admin/datadonatur');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	public function getviewsantri()
	{
		$data{'pilmenutab'}="datasantri";
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('admin/datasantri');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	
	public function getviewhomepembayaran()
	{
		$data{'pilmenutab'}="kelolapembayaran";
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('pembangun/admin/menukiri',$data);
		$this->load->view('admin/homepembayaran');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	public function getviewtagihansantri()
	{
		$data{'pilmenutab'}="kelolapembayaran";
		$data{'pilmenukiri'}="tagihansantri";
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('pembangun/admin/menukiri',$data);
		$this->load->view('admin/tagihansantri');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	
	
	public function getviewpembayaransyahriyahsantri()
	{
		$data{'pilmenutab'}="kelolapembayaran";
		$data{'pilmenukiri'}="pembayaransyahriyah";
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('pembangun/admin/menukiri',$data);
		$this->load->view('admin/pembayaransyahriyahsantri');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	public function getviewpembayaranpendaftaransantri()
	{
		$data{'pilmenutab'}="kelolapembayaran";
		$data{'pilmenukiri'}="pembayaranpendaftaran";
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('pembangun/admin/menukiri',$data);
		$this->load->view('admin/pembayaranpendaftaransantri');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	
	public function getviewpemasukandatadonatur()
	{
		
		$data{'pilmenutab'}="kelolapembayaran";
		$data{'pilmenukiri'}="pemasukandatadonatur";
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('pembangun/admin/menukiri',$data);
		$this->load->view('admin/pemasukandatadonatur');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	public function getviewkelolawebhome()
	{
		
		$width = '100%';
        $height = '500px';
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor-> config['toolbar'] = 'Full';
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor-> config['width'] = $width;
		$this->ckeditor-> config['height'] = $height;
		$path = '../assets/ckfinder'; //path folder ckfinder
		$this->ckfinder->SetupCKEditor($this->ckeditor,$path);
		
		
		$data{'pilmenutab'}="kelolaweb";
		$data{'pilmenukiri'}="home";
		
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('pembangun/admin/menukirikelolaweb',$data);
		$this->load->view('admin/kelolahome');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	
	
	public function getviewkelolawebcarapembayarandonasi()
	{
		
		$width = '100%';
        $height = '500px';
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor-> config['toolbar'] = 'Full';
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor-> config['width'] = $width;
		$this->ckeditor-> config['height'] = $height;
		$path = '../assets/ckfinder'; //path folder ckfinder
		$this->ckfinder->SetupCKEditor($this->ckeditor,$path);
		
		
		$data{'pilmenutab'}="kelolaweb";
		$data{'pilmenukiri'}="carapembayarandonasi";
		
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('pembangun/admin/menukirikelolaweb',$data);
		$this->load->view('admin/kelolacarapembayarandonasi');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	
	public function getviewlaporan()
	{

		$data{'pilmenutab'}="laporan";
		
		$this->load->view('pembangun/admin/header');
		$this->load->view('pembangun/admin/menutab',$data);
		$this->load->view('admin/laporan');
		$this->load->view('pembangun/admin/script');
		$this->load->view('pembangun/admin/footer');
	}
	
	public function logout()
	{
		$data = array(
			'id'  => "",
			'username' => "",
			'jenis' => ""
		);
		
		$this->session->set_userdata($data);
		
		redirect(base_url());
	
	
	}
	
}
