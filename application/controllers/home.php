<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		redirect(base_url("home"));
	}
	
	
	public function carapembayarandonasi()
	{

		$this->load->view('pembangun/home/header');
		$this->load->view('pembangun/home/kontenkiri');
		$this->load->view('home/carapembayarandonatur');
		$this->load->view('pembangun/home/script');
		$this->load->view('pembangun/home/footer');
	}
	
}
