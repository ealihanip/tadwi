	
	
	
	<div class="kontenpembayaran">
		
		<div class="row">
			<center><h5>Tagihan Santri</h5></center>
		</div>
		
		<div class="row grey lighten-5 z-depth-1">
		
			<div class="col s12">
				<table id="tabeltagihansantri" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th>Id Tagihan</th>
						<th>NIS</th>
						<th>Nama</th>
						<th>Angkatan</th>
						<th>Bulan</th>
						<th>Tahun</th>
						<th>Makan</th>
						<th>Listrik</th>
						<th>Infaq</th>
						<th>Status</th>
					</tr>
				</thead>
				
				
			</table>
				
			</div>
		
		
		</div>
	</div>
	
	
		<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabeltagihansantri').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Cari Berdasarkan ID , NIS Atau Nama Santri "
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [[ 0, "desc" ]], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url('tagihansantri/getdata/'); ?><?php echo $id;?>',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "id_tagihan"},
							{"data": "NIS"},
							{"data": "nm_santri"},
							{"data": "angkatan"},
							{"data": "bulan"},
							{"data": "tahun"},
							{"data": "makan"},
							{"data": "listrik"},
							{"data": "infaq"},
							{"data": "status"}
						],
						columnDefs: [
							
							{ targets: [9], orderable: false, },
							{ targets: [8], orderable: false, },
							{ targets: [7], orderable: false, },
							{ targets: [6], orderable: false, },
							{ targets: [5], orderable: false, },
							{ targets: [4], orderable: false, },
							
						],
						
						"createdRow": 
						
						function( row, data, dataIndex ) {
								if ( data['status'] == "Lunas" ) {        
									$(row).addClass('green accent-2');
						 
								}else{
									
									
								}
      

						}
						
						
						
						
					});
	 
				});
				
				
			</script>
		
		<!-- ================================================
						End Script Datatable
		================================================ -->
