	
	
	
	<div class="kontenpembayaran">
		
		<div class="row">
			<center><h5>BIODATA SANTRI</h5></center>
		</div>
		<div class="form grey lighten-5 z-depth-1">
			<form id="formeditsantri">

				
				<div class="row">
				  
				  <div class="input-field col s3">
					<label for="enis">NIS</label>
					
				  </div>
				  <div class="input-field col s6">
					<input id="enis" type="text" name="enis" type="text" data-error=".erorenis" placeholder="edit" disabled>
					<div class="erorenis" style="color:red;font-size:12px;"></div>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="enamasantri">Nama Santri</label>
				  </div>
				  
				  <div class="input-field col s19">
					<input id="enamasantri" type="text" name="enamasantri" type="text" data-error=".erorenamasantri" placeholder="edit">
					<div class="erorenamasantri" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="ejeniskelamin">Jenis Kelamin</label>
				  </div> 
				  <div class="input-field col s5">
					<input id="ejeniskelamin" type="text" name="ejeniskelamin" type="text" data-error=".erorejeniskelamin" placeholder="Silahkan Pilih" >
					<div class="erorejeniskelamin" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihjeniskelamin">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="pilihjeniskelamin" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("ejeniskelamin").value = "Pria"; document.getElementById("ejeniskelamin").focus();'>Pria</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("ejeniskelamin").value = "Wanita"; document.getElementById("ejeniskelamin").focus();'>Wanita</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="etempatlahir" >Tempat Lahir</label>
				  </div>
				  <div class="input-field col s3">
					<input id="etempatlahir" type="text" name="etempatlahir" type="text" data-error=".eroretempatlahir" placeholder="edit" >
					<div class="erorttempatlahir" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				  <div class="input-field col s3">
					
					<label for="etanggallahir" >Tangal Lahir</label>
				  </div>
				  
				  <div class="input-field col s2">
					<input id="etanggallahir" type="text" class="tanggallahir" name="etanggallahir" type="text" data-error=".eroretanggallahir" placeholder="edit" >
					<div class="eroretanggallahir" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ealamat">Alamat</label>
				  </div>
				  <div class="input-field col s9">
					<textarea id="ealamat" class="materialize-textarea" name="ealamat" type="text" data-error=".erortalamat" placeholder="edit"></textarea>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="enotelepon">No Telepon</label>
				  </div>
				  <div class="input-field col s9">
					<input id="enotelepon" type="text" name="enotelepon" type="text" data-error=".erorenotelepon" placeholder="edit">
					<div class="erorenotelepon" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="eangkatan">Angkatan </label>
				  </div> 
				  
				  
				   <div class="input-field col s5">
					<input id="eangkatan" type="text" name="eangkatan" type="text" data-error=".eroreangkatan" placeholder="Silahkan Pilih" onchange='document.getElementById("tangkatan").focus' placeholder="edit">
					
					<div class="eroreangkatan" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col 3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihangkatan">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="pilihangkatan" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("eangkatan").value = "2016"; document.getElementById("eangkatan").focus();'>2016</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("eangkatan").value = "2017"; document.getElementById("eangkatan").focus();'>2017</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="enamaorangtua">Nama Orang Tua</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="enamaorangtua" type="text" name="enamaorangtua" type="text" data-error=".erorenamaorangtua" placeholder="edit">
					<div class="erorenamaorangtua" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
				
				
				<div class="row">
				  <div class="input-field col s12">
				
						
						  <center><img id="efotoview" width="300px" height="auto"></center>
						
					
				  </div>
				</div>
				
				
				<div class="row">
					<div class="input-field col s2">	
						
					</div>
					
					<div class="input-field col s8">	
						<div class="file-field input-field">
						  <input id="enamafoto" class="file-path validate" type="text" name="enamafoto" data-error=".erorenamafoto" placeholder="edit">
							<input id="tempenamafoto" type="hidden">
							<div class="erorenamafoto" style="color:red;font-size:12px;"></div>
						  <div class="btn black">
							<span>File</span>
							<input type="file" id="efoto" />
						  </div>
						</div>
							
						
						
					</div>
	
				</div>
				
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="eusername">Username</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="eusername" type="text" name="eusername" type="text" data-error=".eroreusername" placeholder="edit" disabled>
					<div class="eroreusername" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3" >
					
					<label for="epassword">Password</label>
				  </div>
				  
				  
				  <div class="input-field col s9" >
					<input id="epassword" type="password" name="epassword" type="text" data-error=".erorepassword" placeholder="edit">
					<div class="erorepassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ekpassword">Ketik Ulang Password</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="ekpassword" type="password" name="ekpassword" type="text" data-error=".erorekpassword" placeholder="edit">
					<div class="erorekpassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				
				
				<div class="row">
					<div class="input-field col s4">
					  <button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
					  </button>
					</div>
				</div>
			</form>
			</div>
			
		
		
			
		
		
		
	</div>
	
	
	
	
	<!-- ================================================
				Script Validator Edit Data Santri
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					
					edit();
				}
			});
			
			
			$("#formeditsantri").validate({
				rules: {
					enis: {
						required: true,
						minlength: 4
					},
					enamasantri: {
						required: true,
						minlength: 4
					},
					ejeniskelamin: {
						required: true,
						minlength: 4
					},
					
					etempatlahir: {
						required: true,
						minlength: 4
					},
					etanggallahir: {
						required: true,
						minlength: 4
					},
					ealamat: {
						required: true,
						minlength: 4
					},
					enotelepon: {
						required: true,
						minlength: 4,
						number:true
					},
					eangkatan: {
						required: true,
						minlength: 4,
						number:true
					},
					enamaorangtua: {
						required: true,
						minlength: 4
					},
					enamafoto: {
						required: true,
						
					},
					eusername: {
						required: true,
						minlength: 4
					},
					
					epassword: {
						required: true,
						minlength: 4
					},
					
					ekpassword: {
						required: true,
						minlength: 4,
						equalTo: "#epassword"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					enis:{
						required: "Masukan NIS Santri",
						minlength: "NIS Santri Harus Lebih dari 4 huruf"
					},
					
					enamasantri:{
						required: "Masukan Nama Santri",
						minlength: "Nama Santri Harus Lebih Dari 4 Huruf"
					},
					
					ejeniskelamin:{
						required: "Pilih Jenis Kelamin",
						minlength: "Silahkan Pilih Jenis Kelamin"
					},
					
					
					etempatlahir:{
						required: "Masukan Tempat Lahir untuk pengguna",
						minlength: "Tempat Lahir Harus lebih dari 4 huruf"
					},
					etanggallahir:{
						required: "Masukan tanggal lahir santri",
						
					},
					ealamat:{
						required: "Masukan alamat santri",
						minlength: "Alamat Harus lebih dari 4 huruf"
					},
					enotelepon:{
						required: "Masukan no telepon santri",
						minlength: "No Telepon Santri Harus lebih dari 4 huruf",
						number: "Hanya Angka Yang di perbbolehkan"
					},
					eangkatan:{
						required: "Pilih angkatan santri",
						minlength: "angkatan Harus lebih dari 4 huruf",
						number:true
						
					},
					enamaorangtua:{
						required: "Masukan nama orang tua santri",
						minlength: "Nama Orang TUa Harus lebih dari 4 huruf"
					},
					enamafoto:{
						required: "pilih foto untuk di unggah",
						
					},
					eusername:{
						required: "Username tidak boleh kosong",
						minlength: "usernmae Harus lebih dari 4 huruf"
					},
					epassword:{
						required: "Masukan password untuk santri",
						minlength: "Password Harus lebih dari 4 huruf"
					},
					ekpassword:{
						required: "Ulangi kata sandi",
						equalTo: "Password tidak tepat"
					},
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
			});
			
		</script>
		
		
		<!-- ================================================
				Script Validator edit Data Santri
		================================================ -->

	
	
	<!-- ================================================
			Script Edit Data Donatur
	================================================ -->
	
	<script>
	
	function getdataedit(nis){
		
		var data = new FormData();
		data.append('nis', nis);
		
		
		$.ajax({
			url		: '<?php echo base_url('santri/get')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				var json = $.parseJSON(data);
				
				
				document.getElementById("enis").value = json.NIS; 
				document.getElementById("enamasantri").value = json.nm_santri; 
				document.getElementById("ejeniskelamin").value = json.kelamin; 
				document.getElementById("etempatlahir").value = json.tmp_lahir; 
				document.getElementById("etanggallahir").value = json.tgl_lahir; 
				document.getElementById("ealamat").value = json.alamat; 
				document.getElementById("enotelepon").value = json.no_hp; 
				document.getElementById("eangkatan").value = json.angkatan; 
				document.getElementById("enamaorangtua").value = json.nm_ortu;
				document.getElementById("enamafoto").value = json.foto;
				$('#efotoview').attr('src', json.dirfoto);	
				document.getElementById("tempenamafoto").value = json.foto; 
				document.getElementById("eusername").value = json.username; 
				document.getElementById("epassword").value = json.password; 
				document.getElementById("ekpassword").value = json.password; 
				$('#juduleditdatasantri').focus();
				
			}
		});
		
		
	}
			
	function edit(){
		 
		var data = new FormData();
		data.append('nis', $("#enis").val());
		data.append('namasantri', $("#enamasantri").val());
		data.append('jeniskelamin', $("#ejeniskelamin").val());
		data.append('tempatlahir', $("#etempatlahir").val());
		data.append('tanggallahir', $("#etanggallahir").val());
		data.append('alamat', $("#ealamat").val());
		data.append('nohp', $("#enotelepon").val());
		data.append('angkatan', $("#eangkatan").val());
		data.append('namaortu', $("#enamaorangtua").val());
		data.append('username', $("#eusername").val());
		data.append('password', $("#epassword").val());
		
		if ($("#tempenamafoto").val()==$("#enamafoto").val()){
			
			data.append('namafoto', $("#enamafoto").val());
		}else{
			
			data.append('foto', $("#efoto")[0].files[0]);
			data.append('namafoto','');
		}
		
			
		
		$.ajax({
			url		: '<?php echo base_url('santri/update')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				window.location.replace('<?php echo base_url('walisantri/biodata')?>');
			},
			error : function(error){
				
				
				 
			}
		});

	 
	};
	
	
	</script>
	<!-- ================================================
			Selesai Script Edit Data Donatur
	================================================ -->
	<script>
	
		getdataedit(<?php echo $id;?>);
		
	</script>