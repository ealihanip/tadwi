	
	
	<div class="kontenpembayaran">
		
		<div class="row">
			<center><h5>BIODATA SANTRI</h5></center>
		</div>
		
		<div class="form grey lighten-5 z-depth-1">
			

				<div class="row">
				  <div class="col s12">
				   
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>NIS</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnis"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Nama Santri</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnamasantri"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Jenis Kelamin</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="djeniskelamin"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Tempat Lahir</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dtempatlahir"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Tanggal Lahir</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dtanggallahir"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Alamat</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dalamat"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>No Handphone</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnotelepon"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Angkatan</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dangkatan"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Nama Orang Tua</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnamaorangtua"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Foto</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s7">
					
					
					<a id="dlfotoview" data-fancybox data-caption="My caption">
						<img id="dfotoview" width="300px" height="auto" />
					</a>
				  </div>
				</div>
				
				
				<a href="<?php echo base_url();?>walisantri/edit-biodata"class="btn blue">Edit Biodata</a>	
			</div>
			
		
	</div>
	
	
	
	<script>
	
	function getdata(nis){
				
		var data = new FormData();
		data.append('nis', nis);
		
		
		$.ajax({
			url		: '<?php echo base_url('santri/get')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				var json = $.parseJSON(data);
				
				
				
				$('#dnis').text(json.NIS);
				$('#dnamasantri').text(json.nm_santri); 
				$('#djeniskelamin').text(json.kelamin); 
				$('#dtempatlahir').text(json.tmp_lahir); 
				$('#dtanggallahir').text(json.tgl_lahir); 
				$('#dalamat').text(json.alamat); 
				$('#dnotelepon').text(json.no_hp); 
				$('#dangkatan').text(json.angkatan); 
				$('#dnamaorangtua').text(json.nm_ortu);
				$('#dnamafoto').text(json.foto);
				$('#dfotoview').attr('src', json.dirfoto);	
				$('#dlfotoview').attr('href', json.dirfoto);	
				
				document.getElementById("dempenamafoto").value = json.foto;
				
				
				
				
					
			}
		});
		
		
	}
				
	
	</script>
	
	<script>

		getdata(<?php echo $id;?>);

	</script>