




	<div class="kontenkanan row grey lighten-5 z-depth-1">
		
		
		<div class="row">
			<form id="formtambahdonatur">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="judultambahdonatur">Pendaftaran Donatur Online</h5></center>
				  </div>
				</div>
				
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="tnamadonatur" type="text" name="tnamadonatur" type="text" data-error=".erortnamadonatur">
					<div class="erortnamadonatur" style="color:red;font-size:12px;"></div>
					<label for="tnamadonatur" class="black-text">Nama Donatur</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<input id="tjeniskelamin" type="text" name="tjeniskelamin" type="text" data-error=".erortjeniskelamin" placeholder="SIlahkan Pilih" onchange="$('#tjeniskelamin').val('');">
					<label for="tjeniskelamin" class="black-text">Jenis Kelamin</label>
					<div class="erortjeniskelamin" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s6">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="tjeniskelaminihjeniskelamin">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="tjeniskelaminihjeniskelamin" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("tjeniskelamin").value = "Pria"; document.getElementById("tjeniskelamin").focus();'>Pria</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tjeniskelamin").value = "Wanita"; document.getElementById("tjeniskelamin").focus();'>Wanita</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s6">
					<input id="ttempatlahir" type="text" name="ttempatlahir" type="text" data-error=".erorttempatlahir" >
					<div class="erorttempatlahir" style="color:red;font-size:12px;"></div>
					<label for="ttempatlahir" class="black-text">Tempat Lahir</label>
				  </div>
				  <div class="input-field col s6">
					<input id="ttanggallahir" type="text" class="tanggallahir" name="ttanggallahir" type="text" data-error=".erorttanggallahir" >
					<div class="erorttanggallahir" style="color:red;font-size:12px;"></div>
					<label for="ttanggallahir" class="black-text">Tangal Lahir</label>
				 </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="temail" type="text" name="temail" type="text" data-error=".erortemail">
					<div class="erortemail" style="color:red;font-size:12px;"></div>
					<label for="temail" class="black-text">Email</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="tnotelepon" type="text" name="tnotelepon" type="email" data-error=".erortnotelepon" >
					<div class="erortnotelepon" style="color:red;font-size:12px;"></div>
					<label for="tnotelepon" class="black-text">No Telepon</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<textarea id="talamat" class="materialize-textarea" name="talamat" type="text" data-error=".erortalamat" ></textarea>
					<div class="erortalamat" style="color:red;font-size:12px;"></div>
					<label for="talamat" class="black-text">Alamat</label>
				  </div>
				</div>
								
				<div class="row">
				  <div class="input-field col s12">
					<input id="tnorekening" type="text" name="tnorekening" type="text" data-error=".erortnorekening">
					<div class="erortnorekening" style="color:red;font-size:12px;"></div>
					<label for="tnorekening" class="black-text">No Rekening</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<input id="tstatusdonatur" type="text" name="tstatusdonatur" type="text" data-error=".erortstatusdonatur" placeholder="SIlahkan Pilih" onchange="$('#tstatusdonatur').val('');" >
					<label for="tstatusdonatur" class="black-text">Status Donatur</label>
					<div class="erortstatusdonatur" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s4">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="tstatusdonaturih">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="tstatusdonaturih" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("tstatusdonatur").value = "Tetap"; document.getElementById("tstatusdonatur").focus();'>Tetap</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tstatusdonatur").value = "Bukan Tetap"; document.getElementById("tstatusdonatur").focus();'>Bukan Tetap</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s6">
					<input id="tusername" type="text" name="tusername" type="text" data-error=".erortusername" onchange='cekusername($("#tusername").val());'>
					<div class="erortusername" style="color:red;font-size:12px;"></div>
					<label for="tusername" class="black-text">Username</label>
				  </div>
				  <div class="input-field col s6">
					<div id="cekusername" class="red-text"></div>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s12" >
					<input id="tpassword" type="password" name="tpassword" type="text" data-error=".erortpassword" >
					<div class="erortpassword" style="color:red;font-size:12px;"></div>
					<label for="tpassword" class="black-text">Password</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="tkpassword" type="password" name="tkpassword" type="text" data-error=".erortkpassword" >
					<div class="erortkpassword" style="color:red;font-size:12px;"></div>
					<label for="tkpassword" class="black-text">Ketik Ulang Password</label>
				  </div>
				</div>
				<div class="row">
					<div class="input-field col s12">
					  <button class="btn blue" type="submit" style="width:100%; height:60px;">Daftar
						
					  </button>
					</div>
				</div>
			</form>
		</div>


	</div>

	
	<!-- ================================================
				Script Validator Tambah Data Donatur
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
			
			
			$("#formtambahdonatur").validate({
				rules: {
					tnamadonatur: {
						required: true,
						minlength: 4
					},
					tjeniskelamin: {
						required: true,
						minlength: 4
					},
					ttempatlahir: {
						required: true,
						minlength: 4
					},
					
					ttanggallahir: {
						required: true,
						minlength: 4
					},
					temail: {
						required: true,
						minlength: 4
					},
					tnotelepon: {
						required: true,
						minlength: 4,
						number:true
					},
					talamat: {
						required: true,
						minlength: 4
					},
					tnorekening: {
						required: true,
						minlength: 4,
						number:true
					},
					tstatusdonatur: {
						required: true,
						minlength: 4
					},
					ttanggaljoin: {
						required: true,
						
					},
					tusername: {
						required: true,
						minlength: 4
					},
					
					tpassword: {
						required: true,
						minlength: 4
					},
					
					tkpassword: {
						required: true,
						minlength: 4,
						equalTo: "#tpassword"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tnamadonatur:{
						required: "Masukan Nama Donatur",
						minlength: "Nama Donatur lebih dari 4 huruf"
					},
					
					tjeniskelamin:{
						required: "Pilih Jenis Kelamin",
						minlength: "Silahkan Pilih Jenis Kelamin"
					},
					
					temail:{
						required: "silahkan masukan email",
						minlength: "Email harus diisi"
					},
					
					
					ttempatlahir:{
						required: "Masukan Tempat Lahir",
						minlength: "Tempat Lahir Harus lebih dari 4 huruf"
					},
					ttanggallahir:{
						required: "Masukan tanggal lahir Donatur",
						
					},
					talamat:{
						required: "Masukan alamat Donatur",
						minlength: "Alamat Harus lebih dari 4 huruf"
					},
					tnotelepon:{
						required: "Masukan no telepon Donatur",
						minlength: "No Telepon Harus lebih dari 4 huruf",
						number: "Hanya Angka Yang Di Perbolehkan"
					},
					tnorekening:{
						required: "Silahkan Masukan No rekening",
						minlength: "No Rekening Harus lebih dari 4 Angka",
						number: "Hanya Angka Yang Di Perbolehkan"
					},
					tstatusdonatur:{
						required: "Silahkan Pilih Status Donatur",
						minlength: "Silahkan Pilih Status Donatur"
					},
					ttanggaljoin:{
						required: "Silahkan Masukan Tanggal Join",
						
					},
					
					tusername:{
						required: "Username tidak boleh kosong",
						minlength: "usernmae Harus lebih dari 4 huruf"
					},
					tpassword:{
						required: "Masukan password untuk Donatur",
						minlength: "Password Harus lebih dari 4 huruf"
					},
					tkpassword:{
						required: "Ulangi kata sandi",
						equalTo: "Password tidak tepat"
					},
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
		
		
		<!-- ================================================
				Selesai Script Validator Tambah Data donatur
		================================================ -->

	<!-- ================================================
					Script CRUD
	================================================ -->
	
	<script>
			
		function tambah(){
			 
			var data = new FormData();
			
			data.append('namadonatur', $("#tnamadonatur").val());
			data.append('jeniskelamin', $("#tjeniskelamin").val());
			data.append('tempatlahir', $("#ttempatlahir").val());
			data.append('tanggallahir', $("#ttanggallahir").val());
			data.append('alamat', $("#talamat").val());
			data.append('email', $("#temail").val());
			data.append('nohp', $("#tnotelepon").val());
			data.append('norekening', $("#tnorekening").val());
			data.append('statusdonatur', $("#tstatusdonatur").val());
			data.append('tanggaljoin', 'today');
			data.append('username', $("#tusername").val());
			data.append('password', $("#tpassword").val());
			
			$.ajax({
				url		: '<?php echo base_url('donatur/add')?>',
				type	: 'post',
				processData: false,
				contentType: false,
				dataType: 'html',
				data	: data,
				beforeSend : function(){
					
				},
				success : function(data){
					window.location.replace("<?php echo base_url('sukses-pendaftaran-donatur')?>");
					
				},
				error : function(error){
					
					
					 
				},
				
				
			});
		 
		 
		};
		
		
		
		
	</script>
	<!-- ================================================
					Script CRUD
	================================================ -->
	
	<!-- ================================================
						Script Cek Username
	================================================ -->
		<script>
		
			function cekusername(username){
				
				$('#cekusername').text('');
				var data = new FormData();
				data.append('username', username);
				$.ajax({
					url		: '<?php echo base_url('user/cekusername')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						var json = $.parseJSON(data);
						if(json.status=='success'){
							
							
							
						}else{
							
							$('#tusername').attr('placeholder', $('#tusername').val());
							document.getElementById("tusername").value = ''; 
							$('#cekusername').text('Username Telah Ada Silahkan Coba Yang Lain!');	
							
						}
						
						
						
					},
					error : function(error){
						
					}
				});
			 
						
						
			}
		
		
		</script>
		
		
		<!-- ================================================
						Script Cek Username
		================================================ -->
