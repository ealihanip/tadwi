<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 1.0
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
   
 
    <title>SISPENDANPEN</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/gambar/logotittle.png"/>
    

   
	<!-- bootstrap 3-->    
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" media="screen,projection">
    
	<!-- bootstrap date picker-->    
    <link href="<?php echo base_url();?>assets/boostrap-datepicker/css/bootstrap-datepicker.css" type="text/css" rel="stylesheet" media="screen,projection">
   
	
	<!-- mycssadmin-->    
    <link href="<?php echo base_url();?>assets/mycss/mycssadmin.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- mycss-->    
    <link href="<?php echo base_url();?>assets/mycss/mycss.css" type="text/css" rel="stylesheet" media="screen,projection">
    
    <!-- CORE CSS-->    
    <link href="<?php echo base_url();?>assets/material/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url();?>assets/material/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
	
	
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->    
    <link href="<?php echo base_url();?>assets/material/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url();?>assets/material/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    
	
	<!-- Sweet Alert-->
	<link href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.min.css" type="text/css" rel="stylesheet" media="screen,projection">
   
	<!-- fancy box-->
	<link href="<?php echo base_url();?>assets/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen,projection">
    <script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/jquery.fancybox.js"></script>
	
	
	<!-- jquery datatable -->
	<link href="<?php echo base_url();?>assets/material/js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
	
	<!-- jQuery Library -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/jquery-3.2.1.min.js"></script> 
	
	<!--jquery form validation-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/jquery-validation/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-validation/additional-methods.min.js"></script>
	
	<!-- data-tables -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins/data-tables/data-tables-script.js"></script>
    
	      
</head>

<body class="black"">
    <!-- Start Page Loading -->
   
    <!-- End Page Loading -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="grey darken-4">
                <div class="nav-wrapper">
					<div class="nav-wrapper">
						<div class="white-text">Sistem Informasi Pengolahan Dana Pendidikan</div>
					</div>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->
	
	<!-- START KONTEN -->
	<div class="konten">
    <!-- //////////////////////////////////////////////////////////////////////////// -->
	
    

           

               
                   

                  
                