	
	
	
	<!-- ================================================
    Scripts
    ================================================ -->
    

    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    
	
    <!-- sparkline -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins/sparkline/sparkline-script.js"></script>
    
    <!--jvectormap-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins/jvectormap/vectormap-script.js"></script>
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/material/js/plugins.js"></script>
	
	
	<!--bootrap date picker-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/boostrap-datepicker/js/bootstrap-datepicker.js"></script>
	
	
	<!--Sweet Alert 2-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.min.js"></script>
	
	<!--Fancy Box-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/jquery.fancybox.js"></script>
	
	
	
	<!-- config datepicker -->
	<script type="text/javascript">
		$(document).ready(function () {
			$('.tanggaljoin').datepicker({
				format: "yyyy-mm-dd",
				autoclose:true
			});
			$('.tanggallahir').datepicker({
				format: "yyyy-mm-dd",
				autoclose:true
			});
		});
    </script>
	
	
	
	
	<!--disable right click-->
	
	<script language=javascript>
	 
	<!--
	
	 
	///////////////////////////////////
	function clickIE4(){
	   if (event.button==2){
		  
		  return false;
	   }
	}
	 
	function clickNS4(e){
	   if (document.layers||document.getElementById&&!document.all){
		  if (e.which==2||e.which==3){
		  
		  return false;
		  }
	   }
	}
	 
	if (document.layers){
	   document.captureEvents(Event.MOUSEDOWN);
	   document.onmousedown=clickNS4;
	}
	else if (document.all&&!document.getElementById){
	   document.onmousedown=clickIE4;
	}
	 
	document.oncontextmenu=new Function("return false")
	 
	// -->
	</script>
	
	