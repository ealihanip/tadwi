




	
		
<div class="kontensingle">
		
	<div class="row">
		<h4>Data Donatur</h4>
	</div>
	
	<div class="row"></div>
	<div class="row"></div>
	
	<div class="row">
		<a class="waves-effect waves-light black btn" onclick="openmodaltambah();">Tambah Data Donatur</a>
	</div>
	

	<div class="row"></div>
	
	
	
	<div class="row grey lighten-5 z-depth-1">
	
		<div class="col s12">
			<table id="tabeldonatur" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th style="text-align:center;">ID Donatur</th>
						<th style="text-align:center;">Nama Donatur</th>
						<th style="text-align:center;">Jenis Kelamin</th>
						<th style="text-align:center;">Tempat Lahir</th>
						<th style="text-align:center;">Tanggal Lahir</th>
						<th style="text-align:center;">Status</th>
						<th style="text-align:center;">Aksi</th>
					</tr>
				</thead>
				<tbody>
                </tbody>
			</table>
			
		</div>
	
	
	</div>
	<input type="text" id="datepicker">
</div>
		
		
		<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabeldonatur').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Cari Berdasarkan ID Atau Nama Donatur"
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url('donatur/getdata/'); ?>',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "id_donatur"},
							{"data": "nm_donatur"},
							{"data": "kelamin"},
							{"data": "tmp_lahir"},
							{"data": "tgl_lahir"},
							{"data": "status"},
							{"data": "aksi"}
						],
						columnDefs: [
							{ targets: [6], orderable: false, className: "dt-center", width:"320px"},
							
						]	
						
					});
	 
				});
			</script>
		
		<!-- ================================================
						End Script Datatable
		================================================ -->
		
		<!-- ================================================
						Modal Tambah Data Donatur
		================================================ -->
		<div id="modaltambahdatadonatur" class="modal">
		  <div class="modal-header">
		  
		  </div>
		  
		  <div class="modal-content">
			<div class="form grey lighten-5 z-depth-1">
				<form id="formtambahdonatur" >

					<div class="row"  id="judultambahdonatur">
					  <div class="input-field col s12">
					   <center><h5>Tambah Data Donatur</h5></center>
					  </div>
					</div>
					
					
					<div class="row">
					  <div class="input-field col s12">
						<input id="tnamadonatur" type="text" name="tnamadonatur" type="text" data-error=".erortnamadonatur">
						<div class="erortnamadonatur" style="color:red;font-size:12px;"></div>
						<label for="tnamadonatur">Nama Donatur</label>
					  </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s6">
						<input id="tjeniskelamin" type="text" name="tjeniskelamin" type="text" data-error=".erortjeniskelamin" placeholder="SIlahkan Pilih" >
						<label for="tjeniskelamin">Jenis Kelamin</label>
						<div class="erortjeniskelamin" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s6">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="tjeniskelaminihjeniskelamin">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="tjeniskelaminihjeniskelamin" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tjeniskelamin").value = "Pria"; document.getElementById("tjeniskelamin").focus();'>Pria</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tjeniskelamin").value = "Wanita"; document.getElementById("tjeniskelamin").focus();'>Wanita</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
					  <div class="input-field col s6">
						<input id="ttempatlahir" type="text" name="ttempatlahir" type="text" data-error=".erorttempatlahir" >
						<div class="erorttempatlahir" style="color:red;font-size:12px;"></div>
						<label for="ttempatlahir" >Tempat Lahir</label>
					  </div>
					  <div class="input-field col s6">
						<input id="ttanggallahir" type="text" class="tanggallahir" name="ttanggallahir" type="text" data-error=".erorttanggallahir" >
						<div class="erorttanggallahir" style="color:red;font-size:12px;"></div>
						<label for="ttanggallahir" >Tangal Lahir</label>
					 </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s12">
						<input id="temail" type="text" name="temail" type="text" data-error=".erortemail">
						<div class="erortemail" style="color:red;font-size:12px;"></div>
						<label for="temail">Email</label>
					  </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s12">
						<input id="tnotelepon" type="text" name="tnotelepon" type="email" data-error=".erortnotelepon" >
						<div class="erortnotelepon" style="color:red;font-size:12px;"></div>
						<label for="tnotelepon">No Telepon</label>
					  </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s12">
						<textarea id="talamat" class="materialize-textarea" name="talamat" type="text" data-error=".erortalamat" ></textarea>
						<div class="erortalamat" style="color:red;font-size:12px;"></div>
						<label for="talamat">Alamat</label>
					  </div>
					</div>
									
					<div class="row">
					  <div class="input-field col s12">
						<input id="tnorekening" type="text" name="tnorekening" type="text" data-error=".erortnorekening">
						<div class="erortnorekening" style="color:red;font-size:12px;"></div>
						<label for="tnorekening">No Rekening</label>
					  </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s5">
						<input id="tstatusdonatur" type="text" name="tstatusdonatur" type="text" data-error=".erortstatusdonatur" placeholder="SIlahkan Pilih" >
						<label class="active" for="tstatusdonatur">Status Donatur</label>
						<div class="erortstatusdonatur" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s7">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="tstatusdonaturih">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="tstatusdonaturih" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tstatusdonatur").value = "Tetap"; document.getElementById("tstatusdonatur").focus();'>Tetap</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tstatusdonatur").value = "Bukan Tetap"; document.getElementById("tstatusdonatur").focus();'>Bukan Tetap</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
					  <div class="input-field col s12">
						<input id="ttanggaljoin" type="text" class="tanggallahir" name="ttanggaljoin" type="text" data-error=".erorttanggaljoin" >
						<div class="erorttanggaljoin" style="color:red;font-size:12px;"></div>
						<label for="ttanggaljoin" >Tanggal Join</label>
					  </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s6">
						<input id="tusername" type="text" name="tusername" type="text" data-error=".erortusername" onchange='cekusername($("#tusername").val());'>
						<div class="erortusername" style="color:red;font-size:12px;"></div>
						<label for="tusername">Username</label>
					  </div>
					  <div class="input-field col s6">
						<div id="cekusername" class="red-text"></div>
					  </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s12" >
						<input id="tpassword" type="password" name="tpassword" type="text" data-error=".erortpassword" >
						<div class="erortpassword" style="color:red;font-size:12px;"></div>
						<label for="tpassword">Password</label>
					  </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s12">
						<input id="tkpassword" type="password" name="tkpassword" type="text" data-error=".erortkpassword" >
						<div class="erortkpassword" style="color:red;font-size:12px;"></div>
						<label for="tkpassword">Ketik Ulang Password</label>
					  </div>
					</div>
					<div class="row">
						<div class="input-field col s4">
						  <button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
							
						  </button>
						</div>
					</div>
				</form>
				
			</div>
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
		
		<!-- ================================================
						Selesai Modal Tambah Donatur
		================================================ -->
		<!-- ================================================
						Modal Edit Data Donatur
		================================================ -->
		<div id="modaleditdatadonatur" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			<form id="formeditdonatur">

				<div class="row"  id="juduleditdatadonatur">
				  <div class="input-field col s12">
				   <center><h5>Edit Data Donatur</h5></center>
				  </div>
				</div>
				
				<input type="hidden" id="eiddonatur">
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="enamadonatur">Nama Donatur</label>
				  </div>
				  
				  <div class="input-field col s19">
					<input id="enamadonatur" type="text" name="enamadonatur" type="text" data-error=".erorenamadonatur" placeholder="edit">
					<div class="erorenamadonatur" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="ejeniskelamin">Jenis Kelamin</label>
				  </div> 
				  <div class="input-field col s5">
					<input id="ejeniskelamin" type="text" name="ejeniskelamin" type="text" data-error=".erorejeniskelamin" placeholder="Silahkan Pilih" >
					<div class="erorejeniskelamin" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihjeniskelamin">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="pilihjeniskelamin" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("ejeniskelamin").value = "Pria"; document.getElementById("ejeniskelamin").focus();'>Pria</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("ejeniskelamin").value = "Wanita"; document.getElementById("ejeniskelamin").focus();'>Wanita</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="etempatlahir" >Tempat Lahir</label>
				  </div>
				  <div class="input-field col s3">
					<input id="etempatlahir" type="text" name="etempatlahir" type="text" data-error=".eroretempatlahir" placeholder="edit" >
					<div class="eroretempatlahir" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				  <div class="input-field col s3">
					
					<label for="etanggallahir" >Tangal Lahir</label>
				  </div>
				  
				  <div class="input-field col s2">
					<input id="etanggallahir" type="text" class="tanggallahir" name="etanggallahir" type="text" data-error=".eroretanggallahir" placeholder="edit" >
					<div class="eroretanggallahir" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ealamat">Alamat</label>
				  </div>
				  <div class="input-field col s9">
					<textarea id="ealamat" class="materialize-textarea" name="ealamat" type="text" data-error=".erorealamat" placeholder="edit"></textarea>
					
				  </div>
				  
				</div>
								
				<div class="row">
				  <div class="input-field col s3">
					<label for="temail">Email</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="eemail" type="text" name="eemail" type="text" data-error=".eroreemail">
					<div class="eroreemail" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
								
				<div class="row">
				  <div class="input-field col s3">
					<label for="enotelepon">No Telepon</label>
				  </div>
				  <div class="input-field col s9">
					<input id="enotelepon" type="text" name="enotelepon" type="text" data-error=".erorenotelepon" placeholder="edit">
					<div class="erorenotelepon" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				
				  <div class="input-field col s3">
					<label for="tnorekening">No Rekening</label>
				  </div>
				  <div class="input-field col s9">
					<input id="enorekening" type="text" name="enorekening" type="text" data-error=".erorenorekening">
					<div class="erorenorekening" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
				<div class="row">
				   <div class="input-field col s3">
					<label for="etanggaljoin" >Tanggal Join</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="etanggaljoin" type="text" class="tanggallahir" name="etanggaljoin" type="text" data-error=".eroretanggaljoin" >
					<div class="erorettanggaljoin" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="input-field col s3">
					<label for="ejeniskelamin">Status Donatur</label>
				  </div> 
				  
				  <div class="input-field col s5">
					<input id="estatusdonatur" type="text" name="estatusdonatur" type="text" data-error=".erorestatusdonatur" placeholder="SIlahkan Pilih" >
					
					<div class="erorestatusdonatur" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="estatusdonaturih">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="estatusdonaturih" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("estatusdonatur").value = "Tetap"; document.getElementById("estatusdonatur").focus();'>Tetap</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("estatusdonatur").value = "Bukan Tetap"; document.getElementById("estatusdonatur").focus();'>Bukan Tetap</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="eusername">Username</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="eusername" type="text" name="eusername" type="text" data-error=".eroreusername" disabled>
					<div class="eroreusername" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3" >
					
					<label for="epassword">Password</label>
				  </div>
				  
				  
				  <div class="input-field col s9" >
					<input id="epassword" type="password" name="epassword" type="text" data-error=".erorepassword">
					<input id="prepassword" type="hidden">
					<div class="erorepassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ekpassword">Ketik Ulang Password</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="ekpassword" type="password" name="ekpassword" type="text" data-error=".erorekpassword">
					<div class="erorekpassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				
				
				<div class="row">
					<div class="input-field col s4">
					  <button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
					  </button>
					</div>
				</div>
			</form>
			</div>
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
		
		<!-- ================================================
						Selesai Modal Edit Data Donatur 
		================================================ -->
		
		<!-- ================================================
						Modal Detail Data Donatur
		================================================ -->
		<div id="modaldetaildonatur" class="modal">
		  <div class="modal=header">
			<a class="btn grey modal-action modal-close">Keluar</a>
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			

				<div class="row" id="juduldetaildatasantri">
				  <div class="col s12">
				   <center><h5>Data Donatur</h5></center>
				  </div>
				</div>
				
				
				<div class="row">
				  
				  <div class="col s2">
					<p>ID Donatur</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="didonatur"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Nama Donatur</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnamadonatur"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Jenis Kelamin</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="djeniskelamin"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Tempat Lahir</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dtempatlahir"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Tanggal Lahir</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dtanggallahir"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Alamat</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dalamat"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Email</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="demail"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>No Handphone</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnotelepon"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>No Rekening</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnorekening"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Status</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dstatus"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Tanggal Join</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dtanggaljoin"></p>
				  </div>
				</div>
				
				
				
			</div>
			
		  </div>
		  <div class="modal-footer">
				
		  </div>
		</div>
		
		<!-- ================================================
						Selesai Modal detail Data donatur 
		================================================ -->
		
		<!-- ================================================
				Script Validator Tambah Data Donatur
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
			
			
			$("#formtambahdonatur").validate({
				rules: {
					tnamadonatur: {
						required: true,
						minlength: 4
					},
					tjeniskelamin: {
						required: true,
						minlength: 4
					},
					ttempatlahir: {
						required: true,
						minlength: 4
					},
					
					ttanggallahir: {
						required: true,
						minlength: 4
					},
					temail: {
						required: true,
						minlength: 4
					},
					tnotelepon: {
						required: true,
						minlength: 4,
						number:true
					},
					talamat: {
						required: true,
						minlength: 4
					},
					tnorekening: {
						required: true,
						minlength: 4,
						number:true
					},
					tstatusdonatur: {
						required: true,
						minlength: 4
					},
					ttanggaljoin: {
						required: true,
						
					},
					tusername: {
						required: true,
						minlength: 4
					},
					
					tpassword: {
						required: true,
						minlength: 4
					},
					
					tkpassword: {
						required: true,
						minlength: 4,
						equalTo: "#tpassword"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tnamadonatur:{
						required: "Masukan Nama Donatur",
						minlength: "Nama Donatur lebih dari 4 huruf"
					},
					
					tjeniskelamin:{
						required: "Pilih Jenis Kelamin",
						minlength: "Silahkan Pilih Jenis Kelamin"
					},
					
					temail:{
						required: "silahkan masukan email",
						minlength: "Email harus diisi"
					},
					
					
					ttempatlahir:{
						required: "Masukan Tempat Lahir",
						minlength: "Tempat Lahir Harus lebih dari 4 huruf"
					},
					ttanggallahir:{
						required: "Masukan tanggal lahir Donatur",
						
					},
					talamat:{
						required: "Masukan alamat Donatur",
						minlength: "Alamat Harus lebih dari 4 huruf"
					},
					tnotelepon:{
						required: "Masukan no telepon Donatur",
						minlength: "No Telepon Harus lebih dari 4 huruf",
						number: "Hanya Angka Yang Di Perbolehkan"
					},
					tnorekening:{
						required: "Silahkan Masukan No rekening",
						minlength: "No Rekening Harus lebih dari 4 Angka",
						number: "Hanya Angka Yang Di Perbolehkan"
					},
					tstatusdonatur:{
						required: "Silahkan Pilih Status Donatur",
						minlength: "Silahkan Pilih Status Donatur"
					},
					ttanggaljoin:{
						required: "Silahkan Masukan Tanggal Join",
						
					},
					
					tusername:{
						required: "Username tidak boleh kosong",
						minlength: "usernmae Harus lebih dari 4 huruf"
					},
					tpassword:{
						required: "Masukan password untuk Donatur",
						minlength: "Password Harus lebih dari 4 huruf"
					},
					tkpassword:{
						required: "Ulangi kata sandi",
						equalTo: "Password tidak tepat"
					},
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
		
		
		<!-- ================================================
				Selesai Script Validator Tambah Data donatur
		================================================ -->
		
		<!-- ================================================
				Script Validator Edit Data donatur
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					edit();
					
				}
			});
			
			
			$("#formeditdonatur").validate({
				rules: {
					enamadonatur: {
						required: true,
						minlength: 4
					},
					ejeniskelamin: {
						required: true,
						minlength: 4
					},
					etempatlahir: {
						required: true,
						minlength: 4
					},
					etanggallahir: {
						required: true,
						minlength: 4
					},
					eemail: {
						required: true,
						minlength: 4
					},
					enotelepon: {
						required: true,
						minlength: 4,
						number:true
					},
					ealamat: {
						required: true,
						minlength: 4
					},
					enorekening: {
						required: true,
						minlength: 4,
						number:true
					},
					estatusdonatur: {
						required: true,
						minlength: 4
					},
					etanggaljoin: {
						required: true,
						
					},
					eusername: {
						required: true,
						minlength: 4
					},
					epassword: {
						required: true,
						minlength: 4
					},
					ekpassword: {
						required: true,
						minlength: 4,
						equalTo: "#epassword"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					enamadonatur:{
						required: "Masukan Nama Donatur",
						minlength: "Nama Donatur lebih dari 4 huruf"
					},
					ejeniskelamin:{
						required: "Pilih Jenis Kelamin",
						minlength: "Silahkan Pilih Jenis Kelamin"
					},
					eemail:{
						required: "silahkan masukan email",
						minlength: "Email harus diisi"
					},
					etempatlahir:{
						required: "Masukan Tempat Lahir",
						minlength: "Tempat Lahir Harus lebih dari 4 huruf"
					},
					etanggallahir:{
						required: "Masukan tanggal lahir Donatur",
						
					},
					ealamat:{
						required: "Masukan alamat Donatur",
						minlength: "Alamat Harus lebih dari 4 huruf"
					},
					enotelepon:{
						required: "Masukan no telepon Donatur",
						minlength: "No Telepon Harus lebih dari 4 huruf",
						number: "Hanya Angka Yang Di Perbolehkan"
					},
					enorekening:{
						required: "Silahkan Masukan No rekening",
						minlength: "No Rekening Harus lebih dari 4 Angka",
						number: "Hanya Angka Yang Di Perbolehkan"
					},
					estatusdonatur:{
						required: "Silahkan Pilih Status Donatur",
						minlength: "Silahkan Pilih Status Donatur"
					},
					etanggaljoin:{
						required: "Silahkan Masukan Tanggal Join",
						
					},
					eusername:{
						required: "Username tidak boleh kosong",
						minlength: "usernmae Harus lebih dari 4 huruf"
					},
					epassword:{
						required: "Masukan password untuk Donatur",
						minlength: "Password Harus lebih dari 4 huruf"
					},
					ekpassword:{
						required: "Ulangi kata sandi",
						equalTo: "Password tidak tepat"
					},
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
		
		<!-- ================================================
				Script Validator Edit Data Donatur
		================================================ -->

		<!-- ================================================
						Script CRUD
		================================================ -->
		
		<script>
			
			
			
			function openmodaltambah(){
				
				$('#modaltambahdatadonatur').openModal();
				$('#judultambahdonatur').focus();
				
			}
			
			function openmodaldetail(iddonatur){
				
				var data = new FormData();
				data.append('iddonatur', iddonatur);
				
				
				$.ajax({
					url		: '<?php echo base_url('donatur/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						$('#modaldetaildonatur').openModal();
						
						$('#didonatur').text(json.data[0].id_donatur);
						$('#dnamadonatur').text(json.data[0].nm_donatur);
						$('#djeniskelamin').text(json.data[0].kelamin); 
						$('#dtempatlahir').text(json.data[0].tmp_lahir); 
						$('#dtanggallahir').text(json.data[0].tgl_lahir); 
						$('#dalamat').text(json.data[0].alamat); 
						$('#demail').text(json.data[0].email); 
						$('#dnotelepon').text(json.data[0].no_telp); 
						$('#dnorekening').text(json.data[0].no_rek); 
						$('#dstatus').text(json.data[0].status); 
						$('#dtanggaljoin').text(json.data[0].tgl_join); 
						
						$('#juduldetaildatasantri').focus();
						$('#tabeldonatur').DataTable().ajax.reload();
						
						
							
					}
				});
				
				
			}
			
			function openmodaledit(iddonatur){
				
				var data = new FormData();
				data.append('iddonatur', iddonatur);
				
				
				$.ajax({
					url		: '<?php echo base_url('donatur/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						$('#modaleditdatadonatur').openModal();
						
						document.getElementById("eiddonatur").value =json.data[0].id_donatur;
						document.getElementById("enamadonatur").value =json.data[0].nm_donatur;
						document.getElementById("ejeniskelamin").value =json.data[0].kelamin; 
						document.getElementById("etempatlahir").value =json.data[0].tmp_lahir; 
						document.getElementById("etanggallahir").value =json.data[0].tgl_lahir; 
						document.getElementById("ealamat").value =json.data[0].alamat; 
						document.getElementById("eemail").value =json.data[0].email; 
						document.getElementById("enotelepon").value =json.data[0].no_telp; 
						document.getElementById("enorekening").value =json.data[0].no_rek; 
						document.getElementById("estatusdonatur").value =json.data[0].status; 
						document.getElementById("etanggaljoin").value =json.data[0].tgl_join; 
						document.getElementById("eusername").value =json.datauser[0].username; 
						document.getElementById("epassword").value =json.datauser[0].password; 
						document.getElementById("prepassword").value =json.datauser[0].password; 
						document.getElementById("ekpassword").value =json.datauser[0].password; 
						$('#juduleditdatadonatur').focus();
						
						
					}
				});
				
				
			}
						
			function tambah(){
				 
				var data = new FormData();
				
				data.append('namadonatur', $("#tnamadonatur").val());
				data.append('jeniskelamin', $("#tjeniskelamin").val());
				data.append('tempatlahir', $("#ttempatlahir").val());
				data.append('tanggallahir', $("#ttanggallahir").val());
				data.append('alamat', $("#talamat").val());
				data.append('email', $("#temail").val());
				data.append('nohp', $("#tnotelepon").val());
				data.append('norekening', $("#tnorekening").val());
				data.append('statusdonatur', $("#tstatusdonatur").val());
				data.append('tanggaljoin', $("#ttanggaljoin").val());
				data.append('username', $("#tusername").val());
				data.append('password', $("#tpassword").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('donatur/add')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						$('#modaltambahdatadonatur').closeModal();
						document.getElementById("formtambahdonatur").reset();
						$('#tabeldonatur').DataTable().ajax.reload();	

						var json = $.parseJSON(data);
						
						swal(json.pesan, '', json.status).catch(swal.noop);		
					},
					error : function(error){
						
						
						 
					},
					
					
				});
			 
			 
			};
			
			function edit(){
				 
				var data = new FormData();
				
				data.append('iddonatur', $("#eiddonatur").val());
				data.append('namadonatur', $("#enamadonatur").val());
				data.append('jeniskelamin', $("#ejeniskelamin").val());
				data.append('tempatlahir', $("#etempatlahir").val());
				data.append('tanggallahir', $("#etanggallahir").val());
				data.append('alamat', $("#ealamat").val());
				data.append('email', $("#eemail").val());
				data.append('nohp', $("#enotelepon").val());
				data.append('norekening', $("#enorekening").val());
				data.append('statusdonatur', $("#estatusdonatur").val());
				data.append('tanggaljoin', $("#etanggaljoin").val());
				data.append('username', $("#eusername").val());
				data.append('password', $("#epassword").val());
				data.append('prepassword', $("#prepassword").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('donatur/update')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						$('#modaleditdatadonatur').closeModal();
						document.getElementById("formeditdonatur").reset();
						$('#tabeldonatur').DataTable().ajax.reload();	

						var json = $.parseJSON(data);
						
						swal(json.pesan, '', json.status).catch(swal.noop);	
						 
					},
					error : function(error){
						
						
						 
					},
					
					
				});
			 
			 
			};
			
			function deletedata(iddonatur){
				
				swal({
				  title: 'Apakah Kamu Yakin Untuk Menghapus Item Yang Di Pilih ?',
				  text: 'Seluruh Data Identitas Donatur Dan Data Transaksi Donatur Yang Telah Di Hapus Tidak Akan Bisa Di Kembalikan',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Ya, Hapus'
				}).then(function () {
				  
					var data = new FormData();
					data.append('iddonatur', iddonatur);
					
					
					$.ajax({
						url		: '<?php echo base_url('donatur/delete')?>',
						type	: 'post',
						processData: false,
						contentType: false,
						dataType: 'html',
						data	: data,
						beforeSend : function(){
							
						},
						success : function(data){
							
							
							$('#tabeldonatur').DataTable().ajax.reload();
							var json = $.parseJSON(data);
						
							swal(json.pesan, '', json.status).catch(swal.noop);
						}
					});
				
				  
				}).catch(swal.noop);
				
				
				
				
			}
			
			
		</script>
		<!-- ================================================
						Script CRUD
		================================================ -->
		
		
		<!-- ================================================
						Script Cek Username
		================================================ -->
		<script>
		
			function cekusername(username){
				
				$('#cekusername').text('');
				var data = new FormData();
				data.append('username', username);
				$.ajax({
					url		: '<?php echo base_url('user/cekusername')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						var json = $.parseJSON(data);
						if(json.status=='success'){
							
							
							
						}else{
							
							$('#tusername').attr('placeholder', $('#tusername').val());
							document.getElementById("tusername").value = ''; 
							$('#cekusername').text('Username Telah Ada Silahkan Coba Yang Lain!');	
							
						}
						
						
						
					},
					error : function(error){
						
					}
				});
			 
						
						
			}
		
		
		</script>
		
		
		<!-- ================================================
						Script Cek Username
		================================================ -->


