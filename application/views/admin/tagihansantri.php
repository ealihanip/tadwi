	
	
	
	<div class="kontenpembayaran">
		
	<div class="row">
		<center><h5>Tagihan Santri</h5></center>
	</div>
	
	<div class="row">
		<a class="waves-effect waves-light black btn" onclick="openmodaltambah();">Tambah</a>
	</div>
	
	<div class="row"></div>
	
	
	<div class="row grey lighten-5 z-depth-1">
	
		<div class="col s12">
			<table id="tabeltagihansantri" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th>Id Tagihan</th>
						<th>NIS</th>
						<th>Nama</th>
						<th>Angkatan</th>
						<th>Bulan</th>
						<th>Tahun</th>
						<th>Makan</th>
						<th>Listrik</th>
						<th>Infaq</th>
						<th>Status</th>
					</tr>
				</thead>
				
				
			</table>
			
		</div>
	
	
	</div>
		
</div>

		<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabeltagihansantri').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Cari Berdasarkan ID , NIS Atau Nama Santri "
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [[ 0, "desc" ]], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url('tagihansantri/getdata/'); ?>',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "id_tagihan"},
							{"data": "NIS"},
							{"data": "nm_santri"},
							{"data": "angkatan"},
							{"data": "bulan"},
							{"data": "tahun"},
							{"data": "makan"},
							{"data": "listrik"},
							{"data": "infaq"},
							{"data": "status"}
						],
						columnDefs: [
							
							{ targets: [9], orderable: false, },
							{ targets: [8], orderable: false, },
							{ targets: [7], orderable: false, },
							{ targets: [6], orderable: false, },
							{ targets: [5], orderable: false, },
							{ targets: [4], orderable: false, },
							
						],
						
						"createdRow": 
						
						function( row, data, dataIndex ) {
								if ( data['status'] == "Lunas" ) {        
									$(row).addClass('green accent-2');
						 
								}else{
									
									
								}
      

						}
						
						
						
						
					});
	 
				});
				
				
			</script>
		
		<!-- ================================================
						End Script Datatable
		================================================ -->

		
		
		<!-- ================================================
						Modal Tambah Tagihan
		================================================ -->
		<div id="modaltambahtagihansantri" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			
			<form id="formtambahtagihan">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="judulmodaltransaksidonasi">Buat Tagihan Santri</h5></center>
				  </div>
				</div>
				
				
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="tbulan">Bulan</label>
				  </div> 
				  <div class="input-field col s5">
					<input id="tbulan" type="text" name="tbulan" class="black-text disable" data-error=".erortbulan" placeholder="Silahkan Pilih" onchange="$('#tbulan').val('');">
					<div class="erortbulan" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihbulan">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="pilihbulan" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Januari"; document.getElementById("tbulan").focus();'>Januari</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Februari"; document.getElementById("tbulan").focus();'>Februari</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Maret"; document.getElementById("tbulan").focus();'>Maret</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "April"; document.getElementById("tbulan").focus();'>April</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Mei"; document.getElementById("tbulan").focus();'>Mei</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Juni"; document.getElementById("tbulan").focus();'>Juni</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Juli"; document.getElementById("tbulan").focus();'>Juli</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Agustus"; document.getElementById("tbulan").focus();'>Agustus</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "September"; document.getElementById("tbulan").focus();'>September</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Oktober"; document.getElementById("tbulan").focus();'>Oktober</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "November"; document.getElementById("tbulan").focus();'>November</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Desember"; document.getElementById("tbulan").focus();'>Desember</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				<div class="row">
				  <div class="input-field col s3">
					<label for="ttahun">Tahun</label>
				  </div> 
				  <div class="input-field col s5">
					<input id="ttahun" type="text" name="ttahun" class="black-text" data-error=".erorttahun" placeholder="Silahkan Pilih" onchange="$('#ttahun').val('');">
					<div class="erorttahun" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihtahun">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="pilihtahun" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("ttahun").value = "2017"; document.getElementById("ttahun").focus();'>2017</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("ttahun").value = "2018"; document.getElementById("ttahun").focus();'>2018</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("ttahun").value = "2019"; document.getElementById("ttahun").focus();'>2019</a>
						</li>
									
					</ul>
				  </div>
				  
				</div>
				
				
				
				<div class="row">
				  
					<div class="input-field col s12">
						<button class="btn blue" type="submit" style="width:100%; height:60px;">Buat Tagihan
						
						</button>
					</div>
				</div>
				
			
				
			</form>
			
			
		  </div>
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
				
		<!-- ================================================
				Selesai Modal Tambah Tagihan
		================================================ -->
		
		<!-- ================================================
		  Script Validator Tagihan
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
			
			

			$("#formtambahtagihan").validate({
				rules: {
					tbulan: {
						required: true,
						minlength: 2
					},
					
					ttahun: {
						required: true,
						minlength: 2
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tbulan:{
						required: "Silahkan Pilih Bulan"
					},
					
					
					ttahun:{
						required: "Silahkan Pilih Tahun"
					},
					
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
			Selesai Script Validator Tagihan 
		================================================ -->
		<!-- ================================================
						Script Generate Tagihan Santri
		================================================ -->
		
		<script>
			
			
			
			function openmodaltambah(){
					
				$('#modaltambahtagihansantri').openModal();
				$('#judulmodaltambahtagihansantri').focus();
				
				
			}
									
			function getdata(){
				var data = new FormData();
				data.append('iddonatur',$("#tid").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('donatur/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
										

					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						document.getElementById("tnama").value =json.data[0].nm_donatur;
						document.getElementById("talamat").value =json.data[0].alamat;
						
					},
					error : function(error){
						
						
						
					}
					
				});
				
				
			}
			
			function tambah(){
				
				var data = new FormData();
				data.append('bulan',$("#tbulan").val());
				data.append('tahun',$("#ttahun").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('tagihansantri/add')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
										

					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						swal(json.pesan, '', json.status).catch(swal.noop);
						
						if(json.status=='success'){
							document.getElementById("formtambahtagihan").reset();
							$('#modaltambahtagihansantri').closeModal();
							$('#tabeltagihansantri').DataTable().ajax.reload();
						}
						
					},
					error : function(error){
						
						
						
					}
					
				});
				
			}
			
			
			
			
			
				
				
				
			
		</script>
		<!-- ================================================
						Script Generate Tagihan Santri
		================================================ -->


		
	