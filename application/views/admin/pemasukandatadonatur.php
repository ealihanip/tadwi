




	
		
<div class="kontenpembayaran">
		
	<div class="row">
		<center><h5>Transaksi Pemasukan Data Donatur</h5></center>
	</div>
	
	<div class="row">
		<a class="waves-effect waves-light black btn modal-trigger" onclick="openmodaltambah();">Tambah</a>
	</div>
	
	<div class="row"></div>
	
	
	<div class="row grey lighten-5 z-depth-1">
	
		<div class="col s12">
			<table id="tabeltransaksidonasi" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th>ID Transaksi</th>
						<th>ID Donatur</th>
						<th>Nama</th>
						<th>Tanggal</th>
						<th>Jumlah</th>
						<th>Transaksi</th>
						<th>Verifikasi</th>
						<th>Aksi</th>
					</tr>
				</thead>
			 
			</table>
			
		</div>
	
	
	</div>
		
</div>


		<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabeltransaksidonasi').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Cari Berdasarkan ID Atau Nama Donatur"
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [[ 0, "desc" ]], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url();?>transaksidonasi/getdata/',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "id_trans"},
							{"data": "id_donatur"},
							{"data": "nm_donatur"},
							{"data": "tgl_donasi"},
							{"data": "jml_donasi"},
							{"data": "transaksi"},
							{"data": "statverifikasi",},
							{"data": "aksi"}
						],
						columnDefs: [
							{ targets: [7], orderable: false, className: "dt-center"},
							
						],
						
								
						"createdRow": 
						
						function( row, data, dataIndex ) {
								if ( data['statverifikasi'] == "belum" ) {        
									$(row).addClass('purple accent-2');
						 
								}else{
									
									
								}
      

						}
						  
					});
	 
				});
			</script>
		
		<!-- ================================================
						End Script Datatable
		================================================ -->
		
		
		
		<!-- ================================================
						Modal Tambah Transaksi Donasi
		================================================ -->
		<div id="modaltransaksidonasi" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			
			<form id="formtransaksidonasi">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="judulmodaltransaksidonasi">Transaksi Donasi</h5></center>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="tid" class="black-text">ID Donatur</label>
				  </div>
				  
				  <div class="input-field col s5">
					<input id="tid" type="text" name="tid" type="text" data-error=".erortid" onchange="getdata();" >
					<div class="erortid" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  <div class="input-field col s4">
					<a class="waves-effect waves-light btn green" onclick="getdata();">Dapatkan Data</a>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label class="black-text">Nama</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="tnama" type="text" name="tnama" type="text" disabled class="black-text">
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label class="black-text">Alamat</label>
				  </div>
				  
				  <div class="input-field col s9">
					<textarea class="materialize-textarea black-text" id="talamat" type="text" name="talamat" type="text" disabled ></textarea>
				  </div>
				</div>
				
				
				
				<div class="row">
				   <div class="input-field col s3">
					<label for="ttanggaldonasi" class="black-text">Tanggal Donasi</label>
				  </div>
				  
				  <div class="input-field col s3">
					<input id="ttanggaldonasi" type="text" class="tanggallahir" name="ttanggaldonasi" type="text" data-error=".erorttanggaldonasi" placeholder="Masukan Tanggal Donasi" >
					<div class="erorttanggaldonasi" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
								
							
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ttotalbayar" class="black-text">Jumlah Donasi</label>
				  </div>
				  
				  <div class="input-field col s4">
					<input id="ttotaldonasi" type="text" name="ttotaldonasi" type="text" data-error=".erorttotaldonasi" placeholder="Masukan Jumlah Donasi">
					<div class="erorttotaldonasi" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				  
				</div>
				
				<div class="row">
				  
					<div class="input-field col s12">
						<button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
						</button>
					</div>
				</div>
				
			
				
			</form>
			
			
		  </div>
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
				
		<!-- ================================================
				Selesai Modal Transaksi DOnasi
		================================================ -->
		

		<!-- ================================================
		  Script Validator Transaksi Donasi
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
			
			
			$("#formtransaksidonasi").validate({
				rules: {
					tid: {
						required: true,
						minlength: 4,
						number:true
					},
					ttanggaldonasi: {
						required: true
					},
					
					ttotaldonasi: {
						required: true,
						minlength: 4,
						number:true
					},
					
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tid:{
						required: "Masukan ID Donatur",
						minlength: "ID Donatur lebih dari 4 karakter",
						number: "ID DOnatur Hanya Angka Yang Di perbolehkan"
					},
					
					ttanggaldonasi:{
						required: "Silahkan Pilih Tanggal Donasi"
					},
					
					ttotaldonasi:{
						required: "Silahkan Masukan Total Bayar",
						minlength: "Nominal kurang dari 4 dijit",
						number: "Silahkan masukan nomina yang valid"
					},
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
		  Selesai Script Validator Transaksi Donasi
		================================================ -->

		<!-- ================================================
						Script CRUD
		================================================ -->
		
		<script>
			
			
			
			function openmodaltambah(){
					
				$('#modaltransaksidonasi').openModal();
				$('#judulmodaltransaksidonasi').focus();
				
				
			}
									
			function getdata(){
				var data = new FormData();
				data.append('iddonatur',$("#tid").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('donatur/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
										

					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						document.getElementById("tnama").value =json.data[0].nm_donatur;
						document.getElementById("talamat").value =json.data[0].alamat;
						
					},
					error : function(error){
						
						
						
					}
					
				});
				
				
			}
			
			function tambah(){
				var data = new FormData();
				
				
				data.append('iddonatur', $("#tid").val());
				data.append('tanggaldonasi', $("#ttanggaldonasi").val());
				data.append('jumlahdonasi', $("#ttotaldonasi").val());
				data.append('transaksi', 'byadmin');
				data.append('statusverifikasi', 'sudah');
				
				$.ajax({
					url		: '<?php echo base_url('transaksidonasi/add')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						$('#modaltransaksidonasi').closeModal();
						document.getElementById("formtransaksidonasi").reset();
						$('#tabeltransaksidonasi').DataTable().ajax.reload();
						var json = $.parseJSON(data);
						swal(json.pesan, '', json.status).catch(swal.noop);
							 
					},
					error : function(error){
						
						
						 
					},
					
					
				});
				
			}
			
			function deletedata(idtransaksi){
				
				swal({
				  title: 'Apakah Kamu Yakin Untuk Menghapus Item Yang Di Pilih ?',
				  text: 'Data Yang Telah Di Hapus Tidak Akan Bisa Di Kembalikan',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Ya, Hapus'
				}).then(function () {
				  
					var data = new FormData();
					data.append('idtransaksi', idtransaksi);
				
				
					$.ajax({
						url		: '<?php echo base_url('transaksidonasi/delete')?>',
						type	: 'post',
						processData: false,
						contentType: false,
						dataType: 'html',
						data	: data,
						beforeSend : function(){
							
						},
						success : function(data){
							
							
							$('#tabeltransaksidonasi').DataTable().ajax.reload();
							
						}
					});
				  
				}).catch(swal.noop);
				
				
			}
			
			function verifikasi(idtransaksi){
				
				swal({
				  title: 'Anda Akan Melakukan Verifikasi Pada Transaksi Donasi Ini',
				  text: '',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Ya'
				}).then(function () {
				  
					var data = new FormData();
					data.append('idtransaksi', idtransaksi);
				
				
					$.ajax({
						url		: '<?php echo base_url('transaksidonasi/update')?>',
						type	: 'post',
						processData: false,
						contentType: false,
						dataType: 'html',
						data	: data,
						beforeSend : function(){
							
						},
						success : function(data){
							
							var json = $.parseJSON(data);
							swal(json.pesan, '', json.status).catch(swal.noop);
							$('#tabeltransaksidonasi').DataTable().ajax.reload();
							
						}
					});
				  
				}).catch(swal.noop);
				
				
				
				
				
				
			}
			
			
		</script>
		<!-- ================================================
						Script CRUD
		================================================ -->
	

