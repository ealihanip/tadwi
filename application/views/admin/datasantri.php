




	
		
<div class="kontensingle">
		
	<div class="row">
		<h4>Data Santri</h4>
	</div>
	
	<div class="row"></div>
	<div class="row"></div>
	
	<div class="row">
		<a class="waves-effect waves-light black btn" onclick="openmodaltambah()">Tambah Data Santri</a>
		
	</div>
	

	<div class="row"></div>
	
	
	
	<div class="row grey lighten-5 z-depth-1">
	
		<div class="col s12">
			<table id="tabelsantri" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th style="text-align:center;">NIS</th>
						<th style="text-align:center;">Nama Santri</th>
						<th style="text-align:center;">Jenis Kelamin</th>
						<th style="text-align:center;">Tempat Lahir</th>
						<th style="text-align:center;">Tanggal Lahir</th>
						<th style="text-align:center;">Angkatan</th>
						<th style="text-align:center;">Aksi</th>
					</tr>
				</thead>
				<tbody>
                </tbody>
			</table>
			
		</div>
	
	
	</div>
		
</div>
		<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabelsantri').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Cari Berdasarkan NIS Atau Nama"
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url('santri/getdata'); ?>',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "NIS"},
							{"data": "nm_santri"},
							{"data": "kelamin"},
							{"data": "tmp_lahir"},
							{"data": "tgl_lahir"},
							{"data": "angkatan"},
							{"data": "aksi"}
						],
						columnDefs: [
							{ targets: [6], orderable: false, className: "dt-center", width:"320px"},
							
						]	
						
					});
	 
				});
			</script>
		
		<!-- ================================================
						End Script Datatable
		================================================ -->
		
		<!-- ================================================
						Modal Tambah Data Santri
		================================================ -->
		<div id="modaltambahdatasantri" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			<form id="formtambahsantri">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="judultambahdatasantri">Tambah Data Santri</h5></center>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s6">
					<input id="tnis" type="text" name="tnis" type="text" data-error=".erortnis">
					<div class="erortnis" style="color:red;font-size:12px;"></div>
					<label for="tnis">NIS</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="tnamasantri" type="text" name="tnamasantri" type="text" data-error=".erortnamasantri">
					<div class="erortnamasantri" style="color:red;font-size:12px;"></div>
					<label for="tnamasantri">Nama Santri</label>
				  </div>
				</div>
				
				
				<div class="row">
				  <div class="input-field col s6">
					<input id="tjeniskelamin" type="text" name="tjeniskelamin" type="text" data-error=".erortjeniskelamin" placeholder="SIlahkan Pilih" >
					<label for="tjeniskelamin">Jenis Kelamin</label>
					<div class="erortjeniskelamin" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s6">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="tpilihjeniskelamin">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="tpilihjeniskelamin" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("tjeniskelamin").value = "Pria"; document.getElementById("tjeniskelamin").focus();'>Pria</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tjeniskelamin").value = "Wanita"; document.getElementById("tjeniskelamin").focus();'>Wanita</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s6">
					<input id="ttempatlahir" type="text" name="ttempatlahir" type="text" data-error=".erorttempatlahir" >
					<div class="erorttempatlahir" style="color:red;font-size:12px;"></div>
					<label for="ttempatlahir" >Tempat Lahir</label>
				  </div>
				  <div class="input-field col s6">
					<input id="ttanggallahir" type="text" class="tanggallahir" name="ttanggallahir" type="text" data-error=".erorttanggallahir" >
					<div class="erorttanggallahir" style="color:red;font-size:12px;"></div>
					<label for="ttanggallahir" >Tangal Lahir</label>
				 </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<textarea id="talamat" class="materialize-textarea" name="talamat" type="text" data-error=".erortalamat" ></textarea>
					<div class="erortalamat" style="color:red;font-size:12px;"></div>
					<label for="talamat">Alamat</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="tnotelepon" type="text" name="tnotelepon" type="text" data-error=".erortnotelepon" >
					<div class="erortnotelepon" style="color:red;font-size:12px;"></div>
					<label for="tnotelepon">No Telepon</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s6">
					<input id="tangkatan" type="text" name="tangkatan" type="text" data-error=".erortangkatan" placeholder="SIlahkan Pilih" onchange='document.getElementById("tangkatan").focus'>
					<label for="tangkatan">Angkatan</label>
					<div class="erortangkatan" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s6">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="tpilihangkatan">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="tpilihangkatan" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("tangkatan").value = "2014"; document.getElementById("tangkatan").focus();'>2014</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tangkatan").value = "2015"; document.getElementById("tangkatan").focus();'>2015</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tangkatan").value = "2016"; document.getElementById("tangkatan").focus();'>2016</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tangkatan").value = "2017"; document.getElementById("tangkatan").focus();'>2017</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tangkatan").value = "2018"; document.getElementById("tangkatan").focus();'>2018</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("tangkatan").value = "2019"; document.getElementById("tangkatan").focus();'>2019</a>
						</li>
											
					</ul> 	
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="tnamaorangtua" type="text" name="tnamaorangtua" type="text" data-error=".erortnamaorangtua" >
					<div class="erortnamaorangtua" style="color:red;font-size:12px;"></div>
					<label for="tnamaorangtua">Nama Orang Tua</label>
				  </div>
				</div>
				
				
				<div class="row">
					<div class="input-field col s8">	
						<div class="file-field input-field">
						  <input id="namafoto" class="file-path validate" type="text" name="namafoto" data-error=".erornamafoto">
							
							<div class="erornamafoto" style="color:red;font-size:12px;"></div>
						  <div class="btn black">
							<span>File</span>
							<input type="file" id="tfoto" />
						  </div>
						</div>
					</div>	
				</div>
				
				
				<div class="row">
				  <div class="input-field col s6">
					<input id="tusername" type="text" name="tusername"  data-error=".erortusername" onchange='cekusername($("#tusername").val());'>
					<div class="erortusername" style="color:red;font-size:12px;"></div>
					<label for="tusername">Username</label>
				  </div>
				  <div class="input-field col s6">
					<div id="cekusername" class="red-text"></div>
				  </div>
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s12" >
					<input id="tpassword" type="password" name="tpassword" type="text" data-error=".erortpassword" >
					<div class="erortpassword" style="color:red;font-size:12px;"></div>
					<label for="tpassword">Password</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="tkpassword" type="password" name="tkpassword" type="text" data-error=".erortkpassword" >
					<div class="erortkpassword" style="color:red;font-size:12px;"></div>
					<label for="tkpassword">Ketik Ulang Password</label>
				  </div>
				</div>
				
				
				
				<div class="row">
					<div class="input-field col s4">
					  <button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
					  </button>
					</div>
				</div>
			</form>
			</div>
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
		
		<!-- ================================================
						Selesai Modal Tambah Data Santri
		================================================ -->
		
		<!-- ================================================
						Modal edit Data Santri
		================================================ -->
		<div id="modaleditdatasantri" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			<form id="formeditsantri">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="juduleditdatasantri">Edit Data Santri</h5></center>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="input-field col s3">
					<label for="enis">NIS</label>
					
				  </div>
				  <div class="input-field col s6">
					<input id="enis" type="text" name="enis" type="text" data-error=".erorenis" placeholder="edit" disabled>
					<div class="erorenis" style="color:red;font-size:12px;"></div>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="enamasantri">Nama Santri</label>
				  </div>
				  
				  <div class="input-field col s19">
					<input id="enamasantri" type="text" name="enamasantri" type="text" data-error=".erorenamasantri" placeholder="edit">
					<div class="erorenamasantri" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="ejeniskelamin">Jenis Kelamin</label>
				  </div> 
				  <div class="input-field col s5">
					<input id="ejeniskelamin" type="text" name="ejeniskelamin" type="text" data-error=".erorejeniskelamin" placeholder="Silahkan Pilih" >
					<div class="erorejeniskelamin" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihjeniskelamin">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="pilihjeniskelamin" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("ejeniskelamin").value = "Pria"; document.getElementById("ejeniskelamin").focus();'>Pria</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("ejeniskelamin").value = "Wanita"; document.getElementById("ejeniskelamin").focus();'>Wanita</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="etempatlahir" >Tempat Lahir</label>
				  </div>
				  <div class="input-field col s3">
					<input id="etempatlahir" type="text" name="etempatlahir" type="text" data-error=".eroretempatlahir" placeholder="edit" >
					<div class="erorttempatlahir" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				  <div class="input-field col s3">
					
					<label for="etanggallahir" >Tangal Lahir</label>
				  </div>
				  
				  <div class="input-field col s2">
					<input id="etanggallahir" type="text" class="tanggallahir" name="etanggallahir" type="text" data-error=".eroretanggallahir" placeholder="edit" >
					<div class="eroretanggallahir" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ealamat">Alamat</label>
				  </div>
				  <div class="input-field col s9">
					<textarea id="ealamat" class="materialize-textarea" name="ealamat" type="text" data-error=".erortalamat" placeholder="edit"></textarea>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="enotelepon">No Telepon</label>
				  </div>
				  <div class="input-field col s9">
					<input id="enotelepon" type="text" name="enotelepon" type="text" data-error=".erorenotelepon" placeholder="edit">
					<div class="erorenotelepon" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="eangkatan">Angkatan </label>
				  </div> 
				  
				  
				   <div class="input-field col s5">
					<input id="eangkatan" type="text" name="eangkatan" type="text" data-error=".eroreangkatan" placeholder="Silahkan Pilih" onchange='document.getElementById("tangkatan").focus' placeholder="edit">
					
					<div class="eroreangkatan" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col 3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihangkatan">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="pilihangkatan" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("eangkatan").value = "2016"; document.getElementById("eangkatan").focus();'>2016</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("eangkatan").value = "2017"; document.getElementById("eangkatan").focus();'>2017</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="enamaorangtua">Nama Orang Tua</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="enamaorangtua" type="text" name="enamaorangtua" type="text" data-error=".erorenamaorangtua" placeholder="edit">
					<div class="erorenamaorangtua" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
				
				
				<div class="row">
				  <div class="input-field col s12">
				
						
						  <center><img id="efotoview" width="300px" height="auto"></center>
						
					
				  </div>
				</div>
				
				
				<div class="row">
					<div class="input-field col s2">	
						
					</div>
					
					<div class="input-field col s8">	
						<div class="file-field input-field">
						  <input id="enamafoto" class="file-path validate" type="text" name="enamafoto" data-error=".erorenamafoto" placeholder="edit">
							<input id="tempenamafoto" type="hidden">
							<div class="erorenamafoto" style="color:red;font-size:12px;"></div>
						  <div class="btn black">
							<span>File</span>
							<input type="file" id="efoto" />
						  </div>
						</div>
							
						
						
					</div>
	
				</div>
				
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="eusername">Username</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="eusername" type="text" name="eusername" type="text" data-error=".eroreusername" placeholder="edit" disabled>
					<div class="eroreusername" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3" >
					
					<label for="epassword">Password</label>
				  </div>
				  
				  
				  <div class="input-field col s9" >
					<input id="epassword" type="password" name="epassword" type="text" data-error=".erorepassword" placeholder="edit">
					<input id="prepassword" type="hidden">
					<div class="erorepassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ekpassword">Ketik Ulang Password</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="ekpassword" type="password" name="ekpassword" type="text" data-error=".erorekpassword" placeholder="edit">
					<div class="erorekpassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				
				
				<div class="row">
					<div class="input-field col s4">
					  <button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
					  </button>
					</div>
				</div>
			</form>
			</div>
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
		
		<!-- ================================================
						Selesai Modal Edit Data Santri 
		================================================ -->
		
		<!-- ================================================
						Modal Detail Data Santri
		================================================ -->
		<div id="modaldetailsantri" class="modal">
		  <div class="modal=header">
			<a class="btn grey modal-action modal-close">Keluar</a>
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			

				<div class="row">
				  <div class="col s12">
				   <center><h5 id="juduldetaildatasantri">Data Santri</h5></center>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>NIS</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnis"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Nama Santri</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnamasantri"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Jenis Kelamin</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="djeniskelamin"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Tempat Lahir</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dtempatlahir"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Tanggal Lahir</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dtanggallahir"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Alamat</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dalamat"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>No Handphone</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnotelepon"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Angkatan</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dangkatan"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Nama Orang Tua</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s9">
					<p id="dnamaorangtua"></p>
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="col s2">
					<p>Foto</p>
					
				  </div>
				  
				   <div class="col s1">
					<p>:</p>
					
				  </div>
				  
				  <div class="col s7">
					
					
					<a id="dlfotoview" data-fancybox data-caption="My caption">
						<img id="dfotoview" width="300px" height="auto" />
					</a>
				  </div>
				</div>
				
				
				
			</div>
			
		  </div>
		  <div class="modal-footer">
				
		  </div>
		</div>
		
		<!-- ================================================
						Selesai Modal Edit Data Santri 
		================================================ -->
		
		<!-- ================================================
				Script Validator Tambah Data Santri
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
			
			
			$("#formtambahsantri").validate({
				rules: {
					tnis: {
						required: true,
						minlength: 4,
						maxlength: 15,
						number:true
					},
					tnamasantri: {
						required: true,
						minlength: 4
					},
					tjeniskelamin: {
						required: true,
						minlength: 4
					},
					
					ttempatlahir: {
						required: true,
						minlength: 4
					},
					ttanggallahir: {
						required: true,
						minlength: 4
					},
					talamat: {
						required: true,
						minlength: 4
					},
					tnotelepon: {
						required: true,
						minlength: 4,
						number:true
					},
					tangkatan: {
						required: true,
						minlength: 4,
						number: true
					},
					tnamaorangtua: {
						required: true,
						minlength: 4
					},
					namafoto: {
						required: true,
						
					},
					tusername: {
						required: true,
						minlength: 4
					},
					
					tpassword: {
						required: true,
						minlength: 4
					},
					
					tkpassword: {
						required: true,
						minlength: 4,
						equalTo: "#tpassword"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					tnis:{
						required: "Masukan NIS Santri",
						minlength: "NIS Santri Harus Lebih dari 4 huruf",
						maxlength: "Karakter NIS Terlalu Banyak",
						number: "Hanya angka yang di perbolehkan Untuk NIS"
					},
					
					tnamasantri:{
						required: "Masukan Nama Santri",
						minlength: "Nama Santri lebih dari 4 huruf"
					},
					
					tjeniskelamin:{
						required: "Pilih Jenis Kelamin",
						minlength: "Silahkan Pilih Jenis Kelamin"
					},
					
					
					ttempatlahir:{
						required: "Masukan Tempat Lahir",
						minlength: "Tempat Lahir Harus lebih dari 4 huruf"
					},
					ttanggallahir:{
						required: "Masukan tanggal lahir santri",
						
					},
					talamat:{
						required: "Masukan alamat santri",
						minlength: "Alamat Harus lebih dari 4 huruf"
					},
					tnotelepon:{
						required: "Masukan no telepon santri",
						minlength: "No Telepon Harus lebih dari 4 huruf",
						number: "Hanya Angka Yang Di perbolehkan Untuk No telepon"
					},
					tangkatan:{
						required: "Pilih angkatan santri",
						minlength: "Silahkan Pilih Angkatan",
						number: "Hanya Angka Yang Di perbolehkan Untuk ANGKATAN"
					},
					tnamaorangtua:{
						required: "Masukan nama orang tua santri",
						minlength: "Nama Orang Tua Santri Harus lebih dari 4 huruf"
					},
					namafoto:{
						required: "pilih foto untuk di unggah",
						
					},
					tusername:{
						required: "Username tidak boleh kosong",
						minlength: "usernmae Harus lebih dari 4 huruf"
					},
					
					tpassword:{
						required: "Masukan password untuk santri",
						minlength: "Password Harus lebih dari 4 huruf"
					},
					tkpassword:{
						required: "Ulangi kata sandi",
						equalTo: "Password tidak tepat"
					},
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
		
		
		<!-- ================================================
				Script Validator Tambah Data Santri
		================================================ -->
		
		<!-- ================================================
				Script Validator Edit Data Santri
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					
					edit();
				}
			});
			
			
			$("#formeditsantri").validate({
				rules: {
					enis: {
						required: true,
						minlength: 4
					},
					enamasantri: {
						required: true,
						minlength: 4
					},
					ejeniskelamin: {
						required: true,
						minlength: 4
					},
					
					etempatlahir: {
						required: true,
						minlength: 4
					},
					etanggallahir: {
						required: true,
						minlength: 4
					},
					ealamat: {
						required: true,
						minlength: 4
					},
					enotelepon: {
						required: true,
						minlength: 4,
						number:true
					},
					eangkatan: {
						required: true,
						minlength: 4,
						number:true
					},
					enamaorangtua: {
						required: true,
						minlength: 4
					},
					enamafoto: {
						required: true,
						
					},
					eusername: {
						required: true,
						minlength: 4
					},
					
					epassword: {
						required: true,
						minlength: 4
					},
					
					ekpassword: {
						required: true,
						minlength: 4,
						equalTo: "#epassword"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					enis:{
						required: "Masukan NIS Santri",
						minlength: "NIS Santri Harus Lebih dari 4 huruf"
					},
					
					enamasantri:{
						required: "Masukan Nama Santri",
						minlength: "Nama Santri Harus Lebih Dari 4 Huruf"
					},
					
					ejeniskelamin:{
						required: "Pilih Jenis Kelamin",
						minlength: "Silahkan Pilih Jenis Kelamin"
					},
					
					
					etempatlahir:{
						required: "Masukan Tempat Lahir untuk pengguna",
						minlength: "Tempat Lahir Harus lebih dari 4 huruf"
					},
					etanggallahir:{
						required: "Masukan tanggal lahir santri",
						
					},
					ealamat:{
						required: "Masukan alamat santri",
						minlength: "Alamat Harus lebih dari 4 huruf"
					},
					enotelepon:{
						required: "Masukan no telepon santri",
						minlength: "No Telepon Santri Harus lebih dari 4 huruf",
						number: "Hanya Angka Yang di perbbolehkan"
					},
					eangkatan:{
						required: "Pilih angkatan santri",
						minlength: "angkatan Harus lebih dari 4 huruf",
						number:true
						
					},
					enamaorangtua:{
						required: "Masukan nama orang tua santri",
						minlength: "Nama Orang TUa Harus lebih dari 4 huruf"
					},
					enamafoto:{
						required: "pilih foto untuk di unggah",
						
					},
					eusername:{
						required: "Username tidak boleh kosong",
						minlength: "usernmae Harus lebih dari 4 huruf"
					},
					epassword:{
						required: "Masukan password untuk santri",
						minlength: "Password Harus lebih dari 4 huruf"
					},
					ekpassword:{
						required: "Ulangi kata sandi",
						equalTo: "Password tidak tepat"
					},
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
			});
			
		</script>
		
		
		<!-- ================================================
				Script Validator edit Data Santri
		================================================ -->
		
		<!-- ================================================
						Script CRUD
		================================================ -->
		
		<script>
			
			function openmodaltambah(){
				
				$('#modaltambahdatasantri').openModal();
				$('#judultambahdatasantri').focus();
				
			}
			function openmodaledit(nis){
				
				var data = new FormData();
				data.append('nis', nis);
				
				
				$.ajax({
					url		: '<?php echo base_url('santri/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						$('#modaleditdatasantri').openModal();
						
						document.getElementById("enis").value = json.NIS; 
						document.getElementById("enamasantri").value = json.nm_santri; 
						document.getElementById("ejeniskelamin").value = json.kelamin; 
						document.getElementById("etempatlahir").value = json.tmp_lahir; 
						document.getElementById("etanggallahir").value = json.tgl_lahir; 
						document.getElementById("ealamat").value = json.alamat; 
						document.getElementById("enotelepon").value = json.no_hp; 
						document.getElementById("eangkatan").value = json.angkatan; 
						document.getElementById("enamaorangtua").value = json.nm_ortu;
						document.getElementById("enamafoto").value = json.foto;
						$('#efotoview').attr('src', json.dirfoto);	
						document.getElementById("tempenamafoto").value = json.foto; 
						document.getElementById("eusername").value = json.username; 
						document.getElementById("epassword").value = json.password; 
						document.getElementById("prepassword").value = json.password; 
						document.getElementById("ekpassword").value = json.password; 
						$('#juduleditdatasantri').focus();
						
					}
				});
				
				
			}
			
			
			function deletedata(nis){
				
				swal({
				  title: 'Apakah Kamu Yakin Untuk Menghapus Item Yang Di Pilih ?',
				  text: 'Seluruh Data Identitas Santri Dan Data Transaksi Santri Yang Telah Di Hapus Tidak Akan Bisa Di Kembalikan',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Ya, Hapus'
				}).then(function () {
				  
					var data = new FormData();
					data.append('nis', nis);
					
					
					$.ajax({
						url		: '<?php echo base_url('santri/delete')?>',
						type	: 'post',
						processData: false,
						contentType: false,
						dataType: 'html',
						data	: data,
						beforeSend : function(){
							
						},
						success : function(data){
							
							
							$('#tabelsantri').DataTable().ajax.reload();
							var json = $.parseJSON(data);
						
							swal(json.pesan, '', json.status).catch(swal.noop);
						}
					});
				
				  
				}).catch(swal.noop);
				
				
				
				
				
			}
			
			function openmodaldetail(nis){
				
				var data = new FormData();
				data.append('nis', nis);
				
				
				$.ajax({
					url		: '<?php echo base_url('santri/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						$('#modaldetailsantri').openModal();
						
						$('#dnis').text(json.NIS);
						$('#dnamasantri').text(json.nm_santri); 
						$('#djeniskelamin').text(json.kelamin); 
						$('#dtempatlahir').text(json.tmp_lahir); 
						$('#dtanggallahir').text(json.tgl_lahir); 
						$('#dalamat').text(json.alamat); 
						$('#dnotelepon').text(json.no_hp); 
						$('#dangkatan').text(json.angkatan); 
						$('#dnamaorangtua').text(json.nm_ortu);
						$('#dnamafoto').text(json.foto);
						$('#dfotoview').attr('src', json.dirfoto);	
						$('#dlfotoview').attr('href', json.dirfoto);	
						
						document.getElementById("dempenamafoto").value = json.foto;
						$('#juduldetaildatasantri').focus();
						
						
						
							
					}
				});
				
				
			}
			
			function tambah(){
				 
				var data = new FormData();
				data.append('nis', $("#tnis").val());
				data.append('namasantri', $("#tnamasantri").val());
				data.append('jeniskelamin', $("#tjeniskelamin").val());
				data.append('tempatlahir', $("#ttempatlahir").val());
				data.append('tanggallahir', $("#ttanggallahir").val());
				data.append('alamat', $("#talamat").val());
				data.append('nohp', $("#tnotelepon").val());
				data.append('angkatan', $("#tangkatan").val());
				data.append('namaortu', $("#tnamaorangtua").val());
				data.append('username', $("#tusername").val());
				data.append('password', $("#tpassword").val());
				
				if($("#namafoto").val()!=''){
					data.append('foto', $("#tfoto")[0].files[0]);
					data.append('namafoto', $("#namafoto").val());
					


				}else{
					data.append('namafoto', '');
					
				}
				
				
				
				$.ajax({
					url		: '<?php echo base_url('santri/add')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						$('#modaltambahdatasantri').closeModal();
						document.getElementById("formtambahsantri").reset();
						
						
						$('#tabelsantri').DataTable().ajax.reload();	
						var json = $.parseJSON(data);
						
						swal(json.pesan, '', json.status).catch(swal.noop);
					}
				});
			 
			 
			};
			
			function edit(){
				

				var data = new FormData();
				data.append('nis', $("#enis").val());
				data.append('namasantri', $("#enamasantri").val());
				data.append('jeniskelamin', $("#ejeniskelamin").val());
				data.append('tempatlahir', $("#etempatlahir").val());
				data.append('tanggallahir', $("#etanggallahir").val());
				data.append('alamat', $("#ealamat").val());
				data.append('nohp', $("#enotelepon").val());
				data.append('angkatan', $("#eangkatan").val());
				data.append('namaortu', $("#enamaorangtua").val());
				data.append('username', $("#eusername").val());
				data.append('password', $("#epassword").val());
				data.append('prepassword', $("#prepassword").val());
				
				if ($("#tempenamafoto").val()==$("#enamafoto").val()){
					
					data.append('namafoto', $("#enamafoto").val());
				}else{
					
					data.append('foto', $("#efoto")[0].files[0]);
					data.append('namafoto','');
				}
				
					
				
				$.ajax({
					url		: '<?php echo base_url('santri/update')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						$('#modaleditdatasantri').closeModal();
						
						$('#tabelsantri').DataTable().ajax.reload();	
						var json = $.parseJSON(data);
						
						swal(json.pesan, '', json.status).catch(swal.noop);
					},
					error : function(error){
						
						
						 
					}
				});
			 
			 
			};
			
			
			
			
			
		</script>
		<!-- ================================================
						Script CRUD
		================================================ -->

		<!-- ================================================
						Script Cek Username
		================================================ -->
		<script>
		
			function cekusername(username){
				
				$('#cekusername').text('');
				var data = new FormData();
				data.append('username', username);
				$.ajax({
					url		: '<?php echo base_url('user/cekusername')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						var json = $.parseJSON(data);
						if(json.status=='success'){
							
							
							
						}else{
							
							$('#tusername').attr('placeholder', $('#tusername').val());
							document.getElementById("tusername").value = ''; 
							$('#cekusername').text('Username Telah Ada Silahkan Coba Yang Lain!');	
							
						}
						
						
						
					},
					error : function(error){
						
					}
				});
			 
						
						
			}
		
		
		</script>
		
		
		<!-- ================================================
						Script Cek Username
		================================================ -->
		
		
		
		


		

