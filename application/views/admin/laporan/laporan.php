<html>

<head>
	<title><?php echo $judul;?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
   
	<style>
		
		table{
			
			width:100%;
			border-collapse: collapse;
		}
		
		table,th,td{
	
			 border: 1px solid black;	
			
		}
		
		
		th,td{
			padding: 3px 3px 3px 3px;
			border: 1px solid black;	
			
		}


		.judul{
			padding-bottom: 40px;
			font-size: 35px;
			text-align : center;
		}
		
		.logo{
			
			width:100px;
			height:100px;
			font-size:5px;
			text-align : center;
		}
	
	
	</style>

</head>
	
	  
   

<body>
	<div class="logo">
	<img src="assets/gambar/logo.png" width="50PX" height="auto">
	<p>SISPENDANPEN</p>
	<p>PONDOK PESANTREN AL HASAN</p>
	
	</div>
	
	<br><br>
	<div id="laporansantri">
		<?php if($judul=="Laporan Data Santri"){?>
			<div class="judul">
				
				<?php echo $judul;?>
			
			</div>
			
			<table>
				<tr>
					<th>NIS</th>
					<th>Nama</th>
					<th>Angkatan</th>
					<th>Jenis Kelamin</th>
					<th>Tempat Lahir</th>
					<th>Tanggal Lahir</th>
					<th>No Telepon</th>
					<th>Nama Orang Tua</th>
				
				</tr>
				<?php 
					foreach ($data as $row)
				{
				
				
				?>
				
					<tr>
						<td>
							<?php echo $row->NIS; ?>
						</td>
						<td>
							<?php echo $row->nm_santri; ?>
						</td>
						<td>
							<?php echo $row->angkatan; ?>
						</td>
						<td>
							<?php echo $row->kelamin; ?>
						</td>
						<td>
							<?php echo $row->tmp_lahir; ?>
						</td>
						<td>
							<?php echo $row->tgl_lahir; ?>
						</td>		
						
						<td>
							<?php echo $row->no_hp; ?>
						</td>
						<td>
							<?php echo $row->nm_ortu; ?>
						</td>
						
						
					</tr>
				
					

				<?php } ?>	


			</table>
		<?php }?>


	</div>
	
	<div id="laporandonatur">
		<?php if($judul=="Laporan Data Donatur"){?>
			<div class="judul">

				<?php echo $judul;?>

			</div>
			<br>
			<table>
				<tr>
					<th>ID Donatur</th>
					<th>Nama</th>
					<th>Jenis Kelamin</th>
					<th>Tempat Lahir</th>
					<th>Tanggal Lahir</th>
					<th>No Telepon</th>
					<th>Email</th>
					<th>Status</th>
					<th>Tanggal Join</th>
				
				</tr>
				<?php 
					foreach ($data as $row)
				{
				
				
				?>
				
					<tr>
						<td>
							<?php echo $row->id_donatur; ?>
						</td>
						<td>
							<?php echo $row->nm_donatur; ?>
						</td>
					
						<td>
							<?php echo $row->kelamin; ?>
						</td>
						<td>
							<?php echo $row->tmp_lahir; ?>
						</td>
						<td>
							<?php echo $row->tgl_lahir; ?>
						</td>		
						
						<td>
							<?php echo $row->no_telp; ?>
						</td>
						<td>
							<?php echo $row->email; ?>
						</td>
						<td>
							<?php echo $row->status; ?>
						</td>
						<td>
							<?php echo $row->tgl_join; ?>
						</td>
						
						
					</tr>
				
					

				<?php } ?>	


			</table>
		<?php }?>


	</div>
	
	<div id="laporantagihan">
		<?php if($judul=="Laporan Data Tagihan"){?>
			<div class="judul">

				<?php echo $judul;?>

			</div>
			<br>
			<table>
				<tr>
					<th>ID Tagihan</th>
					<th>NIS</th>
					<th>Nama Santri</th>
					<th>Angkatan</th>
					<th>Bulan</th>
					<th>Tahun</th>
					<th>Makan</th>
					<th>Listrik</th>
					<th>Infaq</th>
					<th>Status</th>
				</tr>
				<?php 
					foreach ($data as $row)
				{
				
				
				?>
				
					<tr>
						<td>
							<?php echo $row->id_tagihan; ?>
						</td>
						<td>
							<?php echo $row->NIS; ?>
						</td>
					
						<td>
							<?php echo $row->nm_santri; ?>
						</td>
						<td>
							<?php echo $row->angkatan; ?>
						</td>
						<td>
							<?php echo $row->bulan; ?>
						</td>		
						
						<td>
							<?php echo $row->tahun; ?>
						</td>
						<td>
							<?php echo $row->makan; ?>
						</td>
						<td>
							<?php echo $row->listrik; ?>
						</td>
						<td>
							<?php echo $row->infaq; ?>
						</td>
						<td>
							<?php echo $row->status; ?>
						</td>
						
						
					</tr>
				
					

				<?php } ?>	


			</table>
		<?php }?>


	</div>
	
	<div id="laporanpembayaranpendaftaran">
		<?php if($judul=="Laporan Data Pembayaran Pendaftaran"){?>
			<div class="judul">

				<?php echo $judul;?>

			</div>
			<br>
			<table>
				<tr>
					<th>NIS</th>
					<th>Nama Santri</th>
					<th>Angkatan</th>
					<th>ID Pembayaran</th>
					<th>Tanggal Bayar</th>
					<th>Total Bayar</th>
				</tr>
				<?php 
					foreach ($data as $row)
				{
				
				
				?>
				
					<tr>
						<td>
							<?php echo $row->NIS; ?>
						</td>
						
						<td>
							<?php echo $row->nm_santri; ?>
						</td>
						<td>
							<?php echo $row->angkatan; ?>
						</td>
						<td>
							<?php echo $row->id_pmbyrn1; ?>
						</td>
					
						<td>
							<?php echo $row->tgl_bayar; ?>
						</td>		
						
						<td>
							<?php echo $row->total_bayar; ?>
						</td>
						
					</tr>
				
					

				<?php } ?>	


			</table>
		<?php }?>


	</div>
	
	<div id="laporanpembayaransyahriyah">
		<?php if($judul=="Laporan Data Pembayaran Syahriyah"){?>
			<div class="judul">

				<?php echo $judul;?>

			</div>
			<br>
			<table>
				<tr>
					<th>ID Pembayaran</th>
					<th>ID Tagihan</th>
					<th>NIS</th>
					<th>Nama</th>
					<th>Angkatan</th>
					<th>Tagihan Bulan</th>
					<th>Tagihan Tahun</th>
					<th>Tanggal Bayar</th>
					<th>Makan</th>
					<th>Listrik</th>
					<th>Infaq</th>
				</tr>
				<?php 
					foreach ($data as $row)
				{
				
				
				?>
				
					<tr>
						<td>
							<?php echo $row->id_pmbyrn2; ?>
						</td>
						
						<td>
							<?php echo $row->id_tagihan; ?>
						</td>
						<td>
							<?php echo $row->NIS; ?>
						</td>
						<td>
							<?php echo $row->nm_santri; ?>
						</td>
						<td>
							<?php echo $row->angkatan; ?>
						</td>
						
						<td>
							<?php echo $row->bulan; ?>
						</td>
						
						<td>
							<?php echo $row->tahun; ?>
						</td>
						
						<td>
							<?php echo $row->tgl_bayar; ?>
						</td>
						<td>
							<?php echo $row->pembayaran_makan; ?>
						</td>
					
						<td>
							<?php echo $row->pembayaran_listrik; ?>
						</td>		
						
						<td>
							<?php echo $row->pembayaran_infaq; ?>
						</td>
						
					</tr>
				
					

				<?php } ?>	


			</table>
		<?php }?>


	</div>
	
	<div id="laporandanadonatur">
		<?php if($judul=="Laporan Data Pemasukan Dana Donatur"){?>
			<div class="judul">

				<?php echo $judul;?>

			</div>
			<br>
			<table>
				<tr>
					<th>ID Transaksi</th>
					<th>ID Donatur</th>
					<th>Nama</th>
					<th>Status</th>
					<th>Tanggal Donasi</th>
					<th>Jumlah Donasi</th>
				</tr>
				<?php 
					foreach ($data as $row)
				{
				
				
				?>
				
					<tr>
						<td>
							<?php echo $row->id_trans; ?>
						</td>
						
						<td>
							<?php echo $row->id_donatur; ?>
						</td>
						<td>
							<?php echo $row->nm_donatur; ?>
						</td>
						<td>
							<?php echo $row->status; ?>
						</td>
						<td>
							<?php echo $row->tgl_donasi; ?>
						</td>
						<td>
							<?php echo $row->jml_donasi; ?>
						</td>
						
					</tr>
				
					

				<?php } ?>	


			</table>
		<?php }?>


	</div>
	
	
	
	
</body>


</html>