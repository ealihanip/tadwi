




	
		
<div class="kontenpembayaran">
		
	<div class="row">
		<center><h5>Transaksi Pembayaran Syahriyah Santri</h5></center>
	</div>
	
	<div class="row">
		<a class="waves-effect waves-light black btn" onclick="openmodaltambah();">Tambah</a>
	</div>
	
	<div class="row"></div>
	
	
	<div class="row grey lighten-5 z-depth-1">
	
		<div class="col s12">
			<table id="tabelpembayaransyahriyah" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th>Id Pembayaran</th>
						<th>Id Tagihan</th>
						<th>NIS</th>
						<th>Nama</th>
						<th>Angkatan</th>
						<th>Tanggal Bayar</th>
						<th>Makan</th>
						<th>Listrik</th>
						<th>Infaq</th>
					</tr>
				</thead>
			 
			</table>
			
		</div>
	
	
	</div>
		
</div>
		
		
		<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabelpembayaransyahriyah').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Pencarian"
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url('pembayaransyahriyah/getdata/'); ?>',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "id_pmbyrn2"},
							{"data": "id_tagihan"},
							{"data": "NIS"},
							{"data": "nm_santri"},
							{"data": "angkatan"},
							{"data": "tgl_bayar"},
							{"data": "pembayaran_makan"},
							{"data": "pembayaran_listrik"},
							{"data": "pembayaran_infaq"}
						],
						columnDefs: [
							
							
						]	
						
					});
	 
				});
			</script>
		
		<!-- ================================================
						End Script Datatable
		================================================ -->
		
		
		<!-- ================================================
				Modal Tambah Pembayaran Syahriyah Santri
		================================================ -->
		<div id="modalpembayaransyahriyahsantri" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			
			<form id="formtransaksipembayaransyahriyah">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="judultransaksipendaftaran">Transaksi Pembayaran Syahriyah</h5></center>
				  </div>
				</div>
				
				
				<div id=formgetdata>
					<div class="row">
					  <div class="input-field col s3">
						
						<label for="tnis" class="black-text">NIS</label>
					  </div>
					  
					  <div class="input-field col s5">
						<input id="tnis" type="text" name="tnis" type="text" data-error=".erortnis" placeholder="NIS">
						<div class="erortnis" style="color:red;font-size:12px;"></div>
						
					  </div>
					  
					</div>
					<div class="row">
					  <div class="input-field col s3">
						<label for="tbulan" class="black-text">Bulan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tbulan" type="text" name="tbulan" class="black-text disable" data-error=".erortbulan" placeholder="Silahkan Pilih" onchange="$('#tbulan').val('');">
						<div class="erortbulan" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s3">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihbulan">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihbulan" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Januari"; document.getElementById("tbulan").focus();'>Januari</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Februari"; document.getElementById("tbulan").focus();'>Februari</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Maret"; document.getElementById("tbulan").focus();'>Maret</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "April"; document.getElementById("tbulan").focus();'>April</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Mei"; document.getElementById("tbulan").focus();'>Mei</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Juni"; document.getElementById("tbulan").focus();'>Juni</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Juli"; document.getElementById("tbulan").focus();'>Juli</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Agustus"; document.getElementById("tbulan").focus();'>Agustus</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "September"; document.getElementById("tbulan").focus();'>September</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Oktober"; document.getElementById("tbulan").focus();'>Oktober</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "November"; document.getElementById("tbulan").focus();'>November</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulan").value = "Desember"; document.getElementById("tbulan").focus();'>Desember</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					<div class="row">
					  <div class="input-field col s3">
						<label for="ttahun" class="black-text">Tahun</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="ttahun" type="text" name="ttahun" class="black-text" data-error=".erorttahun" placeholder="Silahkan Pilih" onchange="$('#ttahun').val('');">
						<div class="erorttahun" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s3">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihtahun">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihtahun" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("ttahun").value = "2017"; document.getElementById("ttahun").focus();'>2017</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahun").value = "2018"; document.getElementById("ttahun").focus();'>2018</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahun").value = "2019"; document.getElementById("ttahun").focus();'>2019</a>
							</li>
										
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
					  
					  <div class="input-field col s4">
						<a class="waves-effect waves-light btn green" onclick="getdata();">Dapatkan Data</a>
						
					  </div>
					  
					</div>
				</div>	
				<div id="detailsantri" class="row green lighten-5 z-depth-1">
					<br>
					<center>Data Santri</center>
					<table>
						<tr>
							<td width="1px">
							Nama
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dnama"></div>
							</td>
							
						</tr>
						<tr>
							<td width="1px">
							Alamat
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dalamat"></div>
							</td>
							
						</tr>
						<tr>
							<td width="1px">
							Angkatan
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dangkatan"></div>
							</td>
							
						</tr>
						
						
						
					</table>
					
				</div>
				<div id="detailtagihan" class="row green lighten-5 z-depth-1">
					<br>
					<center>Data Tagihan</center>
					<table>
						<tr>
							<td width="150px">
							ID Tagihan
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="didtagihan"></div>
							</td>
							
						</tr>
						<tr>
							<td width="1px">
							Bulan
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dbulantagihan"></div>
							</td>
							
						</tr>
						<tr>
							<td width="1px">
							Tahun
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dtahuntagihan"></div>
							</td>
							
						</tr>
						<tr>
							<td width="1px">
							Sisa Tagihan Makan
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dmakan"></div>
							</td>
							
						</tr>
						
						<tr>
							<td width="1px">
							Sisa Tagihan Listrik
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dlistrik"></div>
							</td>
							
						</tr>
						
						<tr>
							<td width="1px">
							Sisa Tagihan Infaq
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dinfaq"></div>
							</td>
							
						</tr>
						<tr>
							<td width="1px">
							Status
							</td>
							<td width="1px">
							:
							</td>
							<td>
								<div id="dstatus"></div>
							</td>
							
						</tr>
						
						
						
					</table>
					
					
					
				</div>
				
				<div id="forminputdata">
					<div class="row">
					   <div class="input-field col s3">
						<label for="ttanggalbayar" class="black-text">Tanggal Bayar</label>
					  </div>
					  
					  <div class="input-field col s3">
						<input id="ttanggalbayar" type="text" class="tanggallahir" name="ttanggalbayar" type="text" data-error=".erorttanggalbayar" >
						<div class="erorttanggalbayar" style="color:red;font-size:12px;"></div>
						
					  </div>
					</div>
					
					<div class="row">
					  <div class="input-field col s3">
						
						<label for="tmakan" class="black-text">Makan</label>
					  </div>
					  
					  <div class="input-field col s4">
						<input id="tmakan" type="text" name="tmakan" type="text" data-error=".erortmakan" placeholder="Masukan Jumlah Pembayaran" value="0">
						<div class="erortmakan" style="color:red;font-size:12px;"></div>
						
					  </div>
					  
					  
					  
					</div>
					
					<div class="row">
					  <div class="input-field col s3">
						
						<label for="tlistrik" class="black-text">Listrik</label>
					  </div>
					  
					  <div class="input-field col s4">
						<input id="tlistrik" type="text" name="tlistrik" type="text" data-error=".erortlistrik" placeholder="Masukan Jumlah Pembayaran" value="0">
						<div class="erortlistrik" style="color:red;font-size:12px;"></div>
						
					  </div>
					  
					  
					  
					</div>
					
					<div class="row">
					  <div class="input-field col s3">
						
						<label for="tinfaq" class="black-text">infaq</label>
					  </div>
					  
					  <div class="input-field col s4">
						<input id="tinfaq" type="text" name="tinfaq" type="text" data-error=".erortinfaq" placeholder="Masukan Jumlah Pembayaran" value="0">
						<div class="erortinfaq" style="color:red;font-size:12px;"></div>
						
					  </div>
					  
					  
					  
					</div>
					
					<div class="row">
					  
						<div class="input-field col s12">
							<button class="btn blue" type="submit" style="width:100%; height:60px;">Bayar
							
							</button>
						</div>
					</div>
					
				</div>
				
			</form>
			
			
		  </div>
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
				
		<!-- ================================================
			Selesai Modal Tambah Pembayaran Syahriyah Santri
		================================================ -->

		<!-- ================================================
		  Script Validator Tambah Data Pembayaran Pendaftaran
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
						
			$("#formtransaksipembayaransyahriyah").validate({
				rules: {
					tnis: {
						required: true,
						minlength: 4,
						number:true
					},
					ttanggalbayar: {
						required: true
					},
					tmakan: {
						required: true,
						number:true
					},
					tlistrik: {
						required: true,
						number:true
					},
					tinfaq: {
						required: true,
						number:true
					},
					tbulan:{
						required: "Silahkan Pilih Bulan"
					},
					
					
					ttahun:{
						required: "Silahkan Pilih Tahun"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tnis:{
						required: "Masukan NIS Santri",
						minlength: "NIS Santri lebih dari 4 karakter",
						number: "NIS Hanya Angka Yang Di perbolehkan"
					},
					
					ttanggalbayar:{
						required: "Silahkan Pilih Tanggal Bayar"
					},
					
					tmakan:{
						required: "Silahkan Masukan Total Bayar",
						min: "Nominal kurang dari 4 dijit",
						number: "Silahkan masukan nominal yang valid"
					},
					tlistrik:{
						required: "Silahkan Masukan Total Bayar",
						min: "Nominal kurang dari 4 dijit",
						number: "Silahkan masukan nominal yang valid"
					},
					tinfaq:{
						required: "Silahkan Masukan Total Bayar",
						min: "Nominal kurang dari 4 dijit",
						number: "Silahkan masukan nominal yang valid"
					},
					
					tbulan:{
						required: "Silahkan Pilih Bulan"
					},
					
					
					ttahun:{
						required: "Silahkan Pilih Tahun"
					},
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
		  Selesai Script Validator Tambah Data Pendaftaran
		================================================ -->
		
		
		
		<!-- ================================================
						Script CRUD
		================================================ -->
		
		<script>
			
			
			
			function openmodaltambah(){
					
				$('#modalpembayaransyahriyahsantri').openModal();
				document.getElementById("formtransaksipembayaransyahriyah").reset();
				$('#formgetdata').show();
				$('#forminputdata').hide();
				$('#detailsantri').hide();
				$('#detailtagihan').hide();
				
				
			}
						
			function getdata(){
				var data = new FormData();
				data.append('nis',$("#tnis").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('santri/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
										

					},
					success : function(data){
						$('#detailsantri').show();
						var json = $.parseJSON(data);
						
						$('#dnama').text(json.nm_santri);
						$('#dangkatan').text(json.angkatan);
						$('#dalamat').text(json.alamat);
						
					},
					error : function(error){
						
						
						
					}
					
				});
				
				var data = new FormData();
				data.append('nis',$("#tnis").val());
				data.append('bulan',$("#tbulan").val());
				data.append('tahun',$("#ttahun").val());
				
				$.ajax({
					url		: '<?php echo base_url('pembayaransyahriyah/getsisatagihan')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
										

					},
					success : function(data){
						$('#detailtagihan').show();
						var jsontagihan = $.parseJSON(data);
						
						$('#didtagihan').text(jsontagihan.id_tagihan);
						$('#dbulantagihan').text(jsontagihan.bulan);
						$('#dtahuntagihan').text(jsontagihan.tahun);
						$('#dmakan').text(jsontagihan.makan);
						$('#dlistrik').text(jsontagihan.listrik);
						$('#dinfaq').text(jsontagihan.infaq);
						$('#dstatus').text(jsontagihan.status);
						
						
						if(jsontagihan.respon=='success'){
	
							$('#detailtagihan').show();
							$('#forminputdata').show();
							$('#formgetdata').hide();
						}else{
							
							$('#forminputdata').hide();
							swal('Data Tidak Di Temukan Silahkan Coba Menggunakan Bulan Dan Tahun Yang Lain', '', jsontagihan.respon).catch(swal.noop);
							
						}
					},
					error : function(error){
						
						
						
					}
					
				});
				
				
			}
			
			function tambah(){
				
				var data = new FormData();
				
				
				data.append('nis', $("#tnis").val());
				data.append('bulan', $("#tbulan").val());
				data.append('tahun', $("#ttahun").val());
				data.append('makan', $("#tmakan").val());
				data.append('listrik', $("#tlistrik").val());
				data.append('infaq', $("#tinfaq").val());
				data.append('tanggalbayar', $("#ttanggalbayar").val());
				
				$.ajax({
					url		: '<?php echo base_url('pembayaransyahriyah/add')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						
						
						var json = $.parseJSON(data);
						swal(json.pesan, '', json.status).catch(swal.noop);
						
						
						if(json.status=="error"){
							
							
						}else{
							
							$('#modalpembayaransyahriyahsantri').closeModal();
							$('#tabelpembayaransyahriyah').DataTable().ajax.reload();
							
						}
						
						
						
							 
					},
					error : function(error){
						
						
						 
					},
					
					
				});
				
			}
			
			
			
			
		</script>
		<!-- ================================================
						Script CRUD
		================================================ -->
	

