




	
		
<div class="kontenpembayaran">
		
	<div class="row">
		<center><h5>Transaksi Pembayaran Pendaftaran Santri</h5></center>
	</div>
	
	<div class="row">
		<a class="waves-effect waves-light black btn" onclick="openmodaltambah();">Tambah</a>
	</div>
	
	<div class="row"></div>
	
	
	<div class="row grey lighten-5 z-depth-1">
	
		<div class="col s12">
			<table id="tabelpembayaranpendaftaran" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th>Id Pembayaran</th>
						<th>NIS</th>
						<th>Nama</th>
						<th>Angkatan</th>
						<th>Tanggal Bayar</th>
						<th>Total Bayar</th>
						<th>Aksi</th>
					</tr>
				</thead>
			 
			</table>
			
		</div>
	
	
	</div>
		
</div>
		
		<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabelpembayaranpendaftaran').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Cari Berdasarkan ID Atau Nama Donatur"
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url('pembayaranpendaftaran/getdata/'); ?>',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "id_pmbyrn1"},
							{"data": "NIS"},
							{"data": "nm_santri"},
							{"data": "angkatan"},
							{"data": "tgl_bayar"},
							{"data": "total_bayar"},
							{"data": "aksi"}
						],
						columnDefs: [
							{ targets: [6], orderable: false, className: "dt-center"},
							
						]	
						
					});
	 
				});
			</script>
		
		<!-- ================================================
						End Script Datatable
		================================================ -->
		
		
		
		
		
		<!-- ================================================
						Modal Tambah Pembayaran Santri
		================================================ -->
		<div id="modaltransaksipendaftaransantri" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			
			<form id="formtransaksipendaftaransantri">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="judultransaksipendaftaran">Transaksi Pembayaran Pendaftaran</h5></center>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="tnis" class="black-text">NIS</label>
				  </div>
				  
				  <div class="input-field col s5">
					<input id="tnis" type="text" name="tnis" type="text" data-error=".erortnis" placeholder="NIS" onchange="getdata();" >
					<div class="erortnis" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  <div class="input-field col s4">
					<a class="waves-effect waves-light btn green" onclick="getdata();">Dapatkan Data</a>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label class="black-text">Nama</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="tnama" type="text" name="tnama" type="text" disabled class="black-text">
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label class="black-text">Jenis Kelamin</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="tjeniskelamin" type="text" name="tjeniskelamin" type="text" disabled class="black-text">
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label class="black-text">Angkatan</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="tangkatan" type="text" name="tangkatan" type="text" disabled class="black-text">
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label class="black-text">Alamat</label>
				  </div>
				  
				  <div class="input-field col s9">
					<textarea class="materialize-textarea black-text" id="talamat" type="text" name="talamat" type="text" disabled ></textarea>
				  </div>
				</div>
				
				
				
				<div class="row">
				   <div class="input-field col s3">
					<label for="ttanggalbayar" class="black-text">Tanggal Bayar</label>
				  </div>
				  
				  <div class="input-field col s3">
					<input id="ttanggalbayar" type="text" class="tanggallahir" name="ttanggalbayar" type="text" data-error=".erorttanggalbayar" >
					<div class="erorttanggalbayar" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ttotalbayar" class="black-text">Total Bayar</label>
				  </div>
				  
				  <div class="input-field col s4">
					<input id="ttotalbayar" type="text" name="ttotalbayar" type="text" data-error=".erorttotalbayar" placeholder="Masukan Jumlah Pembayaran">
					<div class="erorttotalbayar" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				  
				</div>
				
				<div class="row">
				  
					<div class="input-field col s12">
						<button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
						</button>
					</div>
				</div>
				
			
				
			</form>
			
			
		  </div>
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
				
		<!-- ================================================
				Selesai Modal Tambah Pembayaran Santri
		================================================ -->
				
		<!-- ================================================
		  Script Validator Tambah Data Pembayaran Pendaftaran
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
			
			
			$("#formtransaksipendaftaransantri").validate({
				rules: {
					tnis: {
						required: true,
						minlength: 4,
						number:true
					},
					ttanggalbayar: {
						required: true
					},
					ttotalbayar: {
						required: true,
						minlength: 4,
						number:true
					},
					
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tnis:{
						required: "Masukan NIS Santri",
						minlength: "NIS Santri lebih dari 4 karakter",
						number: "NIS Hanya Angka Yang Di perbolehkan"
					},
					
					ttanggalbayar:{
						required: "Silahkan Pilih Tanggal Bayar"
					},
					
					ttotalbayar:{
						required: "Silahkan Masukan Total Bayar",
						minlength: "Nominal kurang dari 4 dijit",
						number: "Silahkan masukan nominal yang valid"
					},
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
		  Selesai Script Validator Tambah Data Pendaftaran
		================================================ -->
				
		<!-- ================================================
						Script CRUD
		================================================ -->
		
		<script>
			
			
			
			function openmodaltambah(){
					
				$('#modaltransaksipendaftaransantri').openModal();
				$('#judultransaksipendaftaran').focus();
				
				
			}
			
			function getdata(){
				var data = new FormData();
				data.append('nis',$("#tnis").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('santri/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
										

					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						document.getElementById("tnama").value =json.nm_santri;
						document.getElementById("tjeniskelamin").value =json.kelamin;
						document.getElementById("tangkatan").value =json.angkatan;
						document.getElementById("talamat").value =json.alamat;
						
					},
					error : function(error){
						
						
						
					}
					
				});
				
				
			}
			
			function tambah(){
				var data = new FormData();
				
				
				data.append('nis', $("#tnis").val());
				data.append('tanggalbayar', $("#ttanggalbayar").val());
				data.append('totalbayar', $("#ttotalbayar").val());
				
				$.ajax({
					url		: '<?php echo base_url('pembayaranpendaftaran/add')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						$('#modaltransaksipendaftaransantri').closeModal();
						document.getElementById("formtransaksipendaftaransantri").reset();
						$('#tabelpembayaranpendaftaran').DataTable().ajax.reload();
						
						var json = $.parseJSON(data);
						swal(json.pesan, '', json.status).catch(swal.noop);
							 
					},
					error : function(error){
						
						
						 
					},
					
					
				});
				
			}
			
			function deletedata(idpembayaran){
				
				swal({
				  title: 'Apakah Kamu Yakin Untuk Menghapus Item Yang Di Pilih ?',
				  text: 'Data Yang Telah Di Hapus Tidak Akan Bisa Di Kembalikan',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Ya, Hapus'
				}).then(function () {
				  
					var data = new FormData();
					data.append('idpembayaran', idpembayaran);
				
				
					$.ajax({
						url		: '<?php echo base_url('pembayaranpendaftaran/delete')?>',
						type	: 'post',
						processData: false,
						contentType: false,
						dataType: 'html',
						data	: data,
						beforeSend : function(){
							
						},
						success : function(data){
							
							
							$('#tabelpembayaranpendaftaran').DataTable().ajax.reload();
							
						}
					});
				  
				}).catch(swal.noop);
				
				
				
				
				
				
			}
			
			
		</script>
		<!-- ================================================
						Script CRUD
		================================================ -->

		

