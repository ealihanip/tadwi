




	
		
<div class="kontensingle">
		
	<div class="row">
		<h4>Data User</h4>
	</div>
	
	<div class="row"></div>
	<div class="row"></div>
	
	<div class="row">
		<a class="waves-effect waves-light black btn" onclick="openmodaltambah();">Tambah</a>
	</div>
	

	<div class="row"></div>
	
	
	
	<div class="row grey lighten-5 z-depth-1">
	
		<div class="col s12">
			<table id="tabeluser" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th style="text-align:center;">ID User</th>
						<th style="text-align:center;">Username</th>
						<th style="text-align:center;">Jenis User</th>
						<th style="text-align:center;">Aksi</th>
					</tr>
				</thead>
				<tbody>
                </tbody>
			</table>
			
		</div>
	
	
	</div>
		
</div>
		
		
		
		
		<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabeluser').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Cari Berdasarkan ID Atau Usernmae"
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url('user/getdata/'); ?>',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "id_user"},
							{"data": "username"},
							{"data": "jenisuser"},
							{"data": "aksi"}
						],
						columnDefs: [
							{ targets: [3], orderable: false, className: "dt-center", width:"320px"},
							
						],	
						"createdRow": 
						
						function( row, data, dataIndex ) {
								if ( data['jenisuser'] == "santri" ) {        
									$(row).addClass('green accent-2');
						 
								}else if( data['jenisuser'] == "donatur" ){
									
									$(row).addClass('blue lighten-4');
								}else{
									
									
									
								}
      

						}
						
					});
	 
				});
			</script>
		
		<!-- ================================================
						End Script Datatable
		================================================ -->
		
		<!-- ================================================
						Modal Tambah Data Administrator
		================================================ -->
		<div id="modaltambahdataadministrator" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
			<div class="form grey lighten-5 z-depth-1">
			<form id="formtambahuser">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="judultambahdatauser">Tambah User</h5></center>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s6">
					<input id="tusername" type="text" name="tusername"  data-error=".erortusername" onchange='cekusername($("#tusername").val());'>
					<div class="erortusername" style="color:red;font-size:12px;"></div>
					<label for="tusername">Username</label>
				  </div>
				  <div class="input-field col s6">
					<div id="cekusername" class="red-text"></div>
				  </div>
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s12" >
					<input id="tpassword" type="password" name="tpassword" type="text" data-error=".erortpassword" >
					<div class="erortpassword" style="color:red;font-size:12px;"></div>
					<label for="tpassword">Password</label>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s12">
					<input id="tkpassword" type="password" name="tkpassword" type="text" data-error=".erortkpassword" >
					<div class="erortkpassword" style="color:red;font-size:12px;"></div>
					<label for="tkpassword">Ketik Ulang Password</label>
				  </div>
				</div>
				
				
				
				<div class="row">
					<div class="input-field col s4">
					  <button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
					  </button>
					</div>
				</div>
			</form>
			</div>
			
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
		
		<!-- ================================================
						Selesai Modal Tambah Administrator
		================================================ -->
		
		<!-- ================================================
						Modal edit Data Santri
		================================================ -->
		<div id="modaledituser" class="modal">
		  <div class="modal=header">
		  
		  </div>
		  
		  <div class="modal-content">
		  
		  <div class="form grey lighten-5 z-depth-1">
			<form id="formedituser">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="juduleditdataadmin">Edit Data Admin</h5></center>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="eiduser">ID User</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="eiduser" type="text" name="eiduser" type="text" data-error=".eroreiduser" placeholder="edit" class="black-text" Disabled>
					<div class="eroreiduser" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="eusername">Username</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="eusername" type="text" name="eusername" type="text" data-error=".eroreusername" placeholder="edit">
					<div class="eroreusername" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3" >
					
					<label for="epassword">Password</label>
				  </div>
				  
				  
				  <div class="input-field col s9" >
					<input id="epassword" type="password" name="epassword" type="text" data-error=".erorepassword" placeholder="edit">
					<input id="prepassword" type="hidden"">
					<div class="erorepassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ekpassword">Ketik Ulang Password</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="ekpassword" type="password" name="ekpassword" type="text" data-error=".erorekpassword" placeholder="edit">
					<div class="erorekpassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				
				
				<div class="row">
					<div class="input-field col s4">
					  <button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
					  </button>
					</div>
				</div>
			</form>
			</div>
			
		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>
		
		<!-- ================================================
						Selesai Modal Edit Data Santri 
		================================================ -->
		<!-- ================================================
				Script Validator Tambah Data Admin
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
			
			
			$("#formtambahuser").validate({
				rules: {
					tusername: {
						required: true,
						minlength: 4
					},
					
					tpassword: {
						required: true,
						minlength: 4
					},
					
					tkpassword: {
						required: true,
						minlength: 4,
						equalTo: "#tpassword"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					tusername:{
						required: "Username tidak boleh kosong",
						minlength: "usernmae Harus lebih dari 4 huruf"
					},
					
					tpassword:{
						required: "Masukan password untuk santri",
						minlength: "Password Harus lebih dari 4 huruf"
					},
					tkpassword:{
						required: "Ulangi kata sandi",
						equalTo: "Password tidak tepat"
					},
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
		
		<!-- ================================================
				Script Validator Tambah Data Admin
		================================================ -->
		
		<!-- ================================================
				Script Validator Edit Data Admin
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					
					edit();
				}
			});
			
			
			$("#formedituser").validate({
				rules: {
					
					eusername: {
						required: true,
						minlength: 4
					},
					
					epassword: {
						required: true,
						minlength: 4
					},
					
					ekpassword: {
						required: true,
						minlength: 4,
						equalTo: "#epassword"
					},
					
				},
				
				
				
				//For custom messages
				messages: {
					
					eusername:{
						required: "Username tidak boleh kosong",
						minlength: "usernmae Harus lebih dari 4 huruf"
					},
					epassword:{
						required: "Masukan password untuk santri",
						minlength: "Password Harus lebih dari 4 huruf"
					},
					ekpassword:{
						required: "Ulangi kata sandi",
						equalTo: "Password tidak tepat"
					},
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
			});
			
		</script>
		
		
		<!-- ================================================
				Script Validator edit Data Admin
		================================================ -->
		
		<!-- ================================================
						Script CRUD
		================================================ -->
		
		<script>
			
			function openmodaltambah(){
				
				$('#modaltambahdataadministrator').openModal();
				$('#judultambahdatauser').focus();
				
			}
			function openmodaledit(iduser){
				
				var data = new FormData();
				data.append('iduser', iduser);
				
				
				$.ajax({
					url		: '<?php echo base_url('user/get')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						var json = $.parseJSON(data);
						
						$('#modaledituser').openModal();
						
						document.getElementById("eiduser").value = json.id_user; 
						document.getElementById("eusername").value = json.username; 
						document.getElementById("epassword").value = json.password; 
						document.getElementById("prepassword").value = json.password; 
						document.getElementById("ekpassword").value = json.password; 
						$('#juduleditdataadmin').focus();
						
					}
				});
				
				
			}
			
			
			function deletedata(iduser){
				
				swal({
				  title: 'Apakah Kamu Yakin Untuk Menghapus Item Yang Di Pilih ?',
				  text: 'Data Yang Telah Di Hapus Tidak Akan Bisa Di Kembalikan',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Ya, Hapus'
				}).then(function () {
				  
					var data = new FormData();
					data.append('iduser', iduser);
				
				
					$.ajax({
						url		: '<?php echo base_url('user/delete')?>',
						type	: 'post',
						processData: false,
						contentType: false,
						dataType: 'html',
						data	: data,
						beforeSend : function(){
							
						},
						success : function(data){
							
							var json = $.parseJSON(data);
						
							swal(json.pesan, '', json.status).catch(swal.noop);
							
							$('#tabeluser').DataTable().ajax.reload();
							
						}
					});
				  
				}).catch(swal.noop);
				
				
				
				
				
				
			}
			
			
			function tambah(){
				 
				var data = new FormData();
				data.append('username', $("#tusername").val());
				data.append('password', $("#tpassword").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('user/add')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						$('#modaltambahdataadministrator').closeModal();
						document.getElementById("formtambahuser").reset();
						
						var json = $.parseJSON(data);
						
						swal(json.pesan, '', json.status).catch(swal.noop);
						
						
						$('#tabeluser').DataTable().ajax.reload();	
						 
					}
				});
			 
			 
			};
			
			function edit(){
				

				var data = new FormData();
				data.append('iduser', $("#eiduser").val());
				data.append('username', $("#eusername").val());
				data.append('password', $("#epassword").val());
				data.append('prepassword', $("#prepassword").val());
				
				
				$.ajax({
					url		: '<?php echo base_url('user/update')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						$('#modaledituser').closeModal();
						
						$('#tabeluser').DataTable().ajax.reload();	
						
						var json = $.parseJSON(data);
						
						swal(json.pesan, '', json.status).catch(swal.noop);
						
					},
					error : function(error){
						
						
						 
					}
				});
			 
			 
			};
			
			
			
			
			
		</script>
		<!-- ================================================
						Script CRUD
		================================================ -->


