	
	
	
	<div class="kontenpembayaran">
		
		<center><h3>Kelola Halaman HOME</h3></center>
		
		
		<a class="waves-effect waves-light black btn" onclick="update()">Simpan</a>
		
		
		
		
		<?php $this->ckeditor->editor('halamanhome'); ?>
		
	</div>
	
	
	
	<script>
	
	function getdata(){
				
		var data = new FormData();
		data.append('namakonten', 'home');
		
		
		$.ajax({
			url		: '<?php echo base_url('admin/kelolaweb/get')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				var json = $.parseJSON(data);
				
				
				document.getElementsByName("halamanhome")[0].value=json.konten[0].isikonten;
				
			}
		});
		
		
	}
	
	
	function update(){
				
		var data = new FormData();
		data.append('isikonten', CKEDITOR.instances.halamanhome.getData());
		data.append('namakonten', 'home');
		
		$.ajax({
			url		: '<?php echo base_url('admin/kelolaweb/update')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				var json = $.parseJSON(data);
						
				swal(json.pesan, '', json.status).catch(swal.noop);
				
			}
		});
		
		
	}
	
	</script>
	
	<script>
		getdata();
	</script>