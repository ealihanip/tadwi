




	
		
		<div class="kontensingle">
		
			<center><h1>Laporan</h1></center>
		
			<div class="row">
				<p>Ini Adalah Halaman Pembuat Laporan.</p>
				<p>Silahkan Pilih Laporan Mana Yang Dibuat Serta Filter Apa Yang Berlaku.</p>
				<p>Laporan Akan Di Buat Berupa File Berekstensi PDF</p>
			</div>
			
			
			<div id="input1" class="formlaporan grey lighten-5 z-depth-1">
				
				
				<form id="inputlaporan">
					<div class="row">
					  <div class="input-field col s2">
						<label for="tlaporan" class="black-text">Laporan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tlaporan" type="text" name="tlaporan" class="black-text disable" data-error=".erortlaporan" placeholder="Silahkan Pilih" onchange="$('#tlaporan').val('');">
						<div class="erortlaporan" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihbulan">Pilih Laporan<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihbulan" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tlaporan").value = "Data Santri"; document.getElementById("tlaporan").focus();'>Data Santri</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tlaporan").value = "Data Donatur"; document.getElementById("tlaporan").focus();'>Data Donatur</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tlaporan").value = "Data Tagihan"; document.getElementById("tlaporan").focus();'>Data Tagihan</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tlaporan").value = "Data Pembayaran Pendaftaran"; document.getElementById("tlaporan").focus();'>Data Pembayaran Pendaftaran</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tlaporan").value = "Data Pembayaran Syahriyah"; document.getElementById("tlaporan").focus();'>Data Pembayaran Syahriyah</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tlaporan").value = "Data Pemasukan Dana Donatur"; document.getElementById("tlaporan").focus();'>Data Pemasukan Dana Donatur</a>
							</li>
							
												
						</ul>
					  </div>
					  
					</div>
				
					<div class="row">
				  
						<div class="input-field col s12">
							<button class="btn blue" type="submit" style="width:100%; height:60px;">Lanjut
							
							</button>
						</div>
					</div>
					
					<div class="row">
				  
						<div class="input-field col s12">
							
						</div>
					</div>
				
				</form>
			
			</div>
			
			
			<div id="input2" class="formlaporan grey lighten-5 z-depth-1">
				
				
				<form id="santri">
				
					<div class="row">
				  
						<div class="input-field col s12">
							<center><h5> FIlter Laporan Untuk Santri</h5></center>
						</div>
					</div>
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="tangkatansantri" class="black-text">Angkatan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tangkatansantri" type="text" name="tangkatansantri" class="black-text disable" data-error=".erortangkatansantri" placeholder="Silahkan Pilih" onchange="$('#tangkatansantri').val('');">
						<div class="erortangkatansantri" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihangkatansantri">Pilih Angkatan<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihangkatansantri" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tangkatansantri").value = "semua-angkatan"; document.getElementById("tangkatansantri").focus();'>semua-angkatan</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatansantri").value = "2014"; document.getElementById("tangkatansantri").focus();'>2014</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatansantri").value = "2015"; document.getElementById("tangkatansantri").focus();'>2015</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatansantri").value = "2016"; document.getElementById("tangkatansantri").focus();'>2016</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatansantri").value = "2017"; document.getElementById("tangkatansantri").focus();'>2017</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatansantri").value = "2018"; document.getElementById("tangkatansantri").focus();'>2018</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatansantri").value = "2019"; document.getElementById("tangkatansantri").focus();'>2019</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="tjeniskelaminsantri" class="black-text">Jenis Kelamin</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tjeniskelaminsantri" type="text" name="tjeniskelaminsantri" class="black-text disable" data-error=".erortjeniskelaminsantri" placeholder="Silahkan Pilih" onchange="$('#tjeniskelaminsantri').val('');">
						<div class="erortjeniskelaminsantri" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihjeniskelaminsantri">Pilih Jenis Kelamin<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihjeniskelaminsantri" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tjeniskelaminsantri").value = "semua-jenis-kelamin"; document.getElementById("tjeniskelaminsantri").focus();'>semua-jenis-kelamin</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tjeniskelaminsantri").value = "Pria"; document.getElementById("tjeniskelaminsantri").focus();'>Pria</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tjeniskelaminsantri").value = "Wanita"; document.getElementById("tjeniskelaminsantri").focus();'>Wanita</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					<div class="row">
				  
						<div class="input-field col s12">
							<button class="btn blue" type="submit" style="width:100%; height:60px;">Lanjut
							
							</button>
						</div>
					</div>
					
					
				</form>
								
				<form id="tagihan">
				
					<div class="row">
				  
						<div class="input-field col s12">
							<center><h5> Filter Laporan Untuk Laporan Tagihan</h5></center>
						</div>
					</div>
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="tstatustagihan" class="black-text">Status Tagihan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tstatustagihan" type="text" name="tstatustagihan" class="black-text disable" data-error=".erortstatustagihan" placeholder="Silahkan Pilih" onchange="$('#tstatustagihan').val('');">
						<div class="erortstatustagihan" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihstatustagihan">Pilih Status<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihstatustagihan" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tstatustagihan").value = "semua-status"; document.getElementById("tstatustagihan").focus();'>Semua Status</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tstatustagihan").value = "sudah-lunas"; document.getElementById("tstatustagihan").focus();'>Sudah Lunas</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tstatustagihan").value = "belum-lunas"; document.getElementById("tstatustagihan").focus();'>Belum Lunas</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="ttahuntagihan" class="black-text">Tahun Tagihan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="ttahuntagihan" type="text" name="ttahuntagihan" class="black-text disable" data-error=".erorttahuntagihan" placeholder="Silahkan Pilih" onchange="$('#ttahuntagihan').val('');">
						<div class="erorttahuntagihan" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihtahuntagihan">Pilih Tahun<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihtahuntagihan" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("ttahuntagihan").value = "semua-tahun"; document.getElementById("ttahuntagihan").focus();'>Semua Tahun</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahuntagihan").value = "2017"; document.getElementById("ttahuntagihan").focus();'>2017</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahuntagihan").value = "2018"; document.getElementById("ttahuntagihan").focus();'>2018</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahuntagihan").value = "2019"; document.getElementById("ttahuntagihan").focus();'>2019</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					<div class="row">
					  <div class="input-field col s2">
						<label for="tbulantagihan" class="black-text">Bulan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tbulantagihan" type="text" name="tbulantagihan" class="black-text disable" data-error=".erortbulantagihan" placeholder="Silahkan Pilih" onchange="$('#tbulantagihan').val('');">
						<div class="erortbulantagihan" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihbulantagihan">Pilih Bulan<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihbulantagihan" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "semua-bulan"; document.getElementById("tbulantagihan").focus();'>Semua Bulan</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Januari"; document.getElementById("tbulantagihan").focus();'>Januari</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Februari"; document.getElementById("tbulantagihan").focus();'>Februari</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Maret"; document.getElementById("tbulantagihan").focus();'>Maret</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "April"; document.getElementById("tbulantagihan").focus();'>April</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Mei"; document.getElementById("tbulantagihan").focus();'>Mei</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Juni"; document.getElementById("tbulantagihan").focus();'>Juni</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Juli"; document.getElementById("tbulantagihan").focus();'>Juli</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Agustus"; document.getElementById("tbulantagihan").focus();'>Agustus</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "September"; document.getElementById("tbulantagihan").focus();'>September</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Oktober"; document.getElementById("tbulantagihan").focus();'>Oktober</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "November"; document.getElementById("tbulantagihan").focus();'>November</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulantagihan").value = "Desember"; document.getElementById("tbulantagihan").focus();'>Desember</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					<div class="row">
				  
						<div class="input-field col s12">
							<button class="btn blue" type="submit" style="width:100%; height:60px;">Buat Laporan
							
							</button>
						</div>
					</div>
					
					
				</form>
				
				<form id="pembayaranpendaftaran">
					
					<div class="row">
				  
						<div class="input-field col s12">
							<center><h5> Filter Laporan Untuk Data Pembayaran Pendaftaran</h5></center>
						</div>
					</div>
					
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="tangkatanpembayaranpendaftaran" class="black-text">Angkatan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tangkatanpembayaranpendaftaran" type="text" name="tangkatanpembayaranpendaftaran" class="black-text disable" data-error=".erortangkatanpembayaranpendaftaran" placeholder="Silahkan Pilih" onchange="$('#tangkatanpembayaranpendaftaran').val('');">
						<div class="erortangkatanpembayaranpendaftaran" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihangkatanpembayaranpendaftaran">Pilih Angkatan<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihangkatanpembayaranpendaftaran" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaranpendaftaran").value = "semua-angkatan"; document.getElementById("tangkatanpembayaranpendaftaran").focus();'>Semua Tahun</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaranpendaftaran").value = "2014"; document.getElementById("tangkatanpembayaranpendaftaran").focus();'>2014</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaranpendaftaran").value = "2015"; document.getElementById("tangkatanpembayaranpendaftaran").focus();'>2015</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaranpendaftaran").value = "2016"; document.getElementById("tangkatanpembayaranpendaftaran").focus();'>2016</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaranpendaftaran").value = "2017"; document.getElementById("tangkatanpembayaranpendaftaran").focus();'>2017</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaranpendaftaran").value = "2018"; document.getElementById("tangkatanpembayaranpendaftaran").focus();'>2018</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaranpendaftaran").value = "2019"; document.getElementById("tangkatanpembayaranpendaftaran").focus();'>2019</a>
							</li>
							
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="tstatuspembayaranpendaftaran" class="black-text">Status</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tstatuspembayaranpendaftaran" type="text" name="tstatuspembayaranpendaftaran" class="black-text disable" data-error=".erortstatuspembayaranpendaftaran" placeholder="Silahkan Pilih" onchange="$('#tstatuspembayaranpendaftaran').val('');">
						<div class="erortstatuspembayaranpendaftaran" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihanstatuspembayaran">Pilih Status<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihanstatuspembayaran" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tstatuspembayaranpendaftaran").value = "semua-status-pembayaran"; document.getElementById("tstatuspembayaranpendaftaran").focus();'>Semua Status Pembayaran</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tstatuspembayaranpendaftaran").value = "sudah-bayar"; document.getElementById("tstatuspembayaranpendaftaran").focus();'>Sudah Bayar</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tstatuspembayaranpendaftaran").value = "belum-bayar"; document.getElementById("tstatuspembayaranpendaftaran").focus();'>Belum Bayar</a>
							</li>
							
							
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
				  
						<div class="input-field col s12">
							<button class="btn blue" type="submit" style="width:100%; height:60px;">Buat Laporan
							
							</button>
						</div>
					</div>
					
					
				
				</form>
				
				<form id="pemasukandanadonatur">
					
					<div class="row">
				  
						<div class="input-field col s12">
							<center><h5> Filter Laporan Untuk Data Pemasukan Donatur</h5></center>
						</div>
					</div>
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="ttahunpemasukandanadonatur" class="black-text">Tahun</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="ttahunpemasukandanadonatur" type="text" name="ttahunpemasukandanadonatur" class="black-text disable" data-error=".erorttahunpemasukandanadonatur" placeholder="Silahkan Pilih" onchange="$('#ttahunpemasukandanadonatur').val('');">
						<div class="erorttahunpemasukandanadonatur" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihtahunpemasukandanadonatur">Pilih Tahun<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihtahunpemasukandanadonatur" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("ttahunpemasukandanadonatur").value = "semua-tahun"; document.getElementById("ttahunpemasukandanadonatur").focus();'>Semua Tahun</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahunpemasukandanadonatur").value = "2016"; document.getElementById("ttahunpemasukandanadonatur").focus();'>2016</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahunpemasukandanadonatur").value = "2017"; document.getElementById("ttahunpemasukandanadonatur").focus();'>2017</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahunpemasukandanadonatur").value = "2017"; document.getElementById("ttahunpemasukandanadonatur").focus();'>2018</a>
							</li>
							
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="tbulanpemasukandanadonatur" class="black-text">Bulan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tbulanpemasukandanadonatur" type="text" name="tbulanpemasukandanadonatur" class="black-text disable" data-error=".erortbulanpemasukandanadonatur" placeholder="Silahkan Pilih" onchange="$('#tbulanpemasukandanadonatur').val('');">
						<div class="erortbulanpemasukandanadonatur" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihbulanpemasukandanadonatur">Pilih Bulan<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihbulanpemasukandanadonatur" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "semua-bulan"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Semua Bulan</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "1"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Januari</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "2"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Februari</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "3"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Maret</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "4"; document.getElementById("tbulanpemasukandanadonatur").focus();'>April</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "5"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Mei</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "6"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Juni</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "7"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Juli</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "8"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Agustus</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "9"; document.getElementById("tbulanpemasukandanadonatur").focus();'>September</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "10"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Oktober</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "11"; document.getElementById("tbulanpemasukandanadonatur").focus();'>November</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpemasukandanadonatur").value = "12"; document.getElementById("tbulanpemasukandanadonatur").focus();'>Desember</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					
					
					<div class="row">
				  
						<div class="input-field col s12">
							<button class="btn blue" type="submit" style="width:100%; height:60px;">Buat Laporan
							
							</button>
						</div>
					</div>
					
				
				</form>
								
				<form id="pembayaransyahriyah">
					
					<div class="row">
				  
						<div class="input-field col s12">
							<center><h5> Filter Laporan Untuk Data Pembayaran Pendaftaran</h5></center>
						</div>
					</div>
					<div class="row">
					  <div class="input-field col s2">
						<label for="tangkatanpembayaransyahriyah" class="black-text">Angkatan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tangkatanpembayaransyahriyah" type="text" name="tangkatanpembayaransyahriyah" class="black-text disable" data-error=".erortangkatanpembayaransyahriyah" placeholder="Silahkan Pilih" onchange="$('#tangkatanpembayaransyahriyah').val('');">
						<div class="erortangkatanpembayaransyahriyah" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihangkatanpembayaransyahriyah">Pilih Angakatan<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihangkatanpembayaransyahriyah" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaransyahriyah").value = "semua-angkatan"; document.getElementById("tangkatanpembayaransyahriyah").focus();'>Semua Tahun</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaransyahriyah").value = "2014"; document.getElementById("tangkatanpembayaransyahriyah").focus();'>2014</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaransyahriyah").value = "2015"; document.getElementById("tangkatanpembayaransyahriyah").focus();'>2015</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaransyahriyah").value = "2016"; document.getElementById("tangkatanpembayaransyahriyah").focus();'>2016</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaransyahriyah").value = "2017"; document.getElementById("tangkatanpembayaransyahriyah").focus();'>2017</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaransyahriyah").value = "2018"; document.getElementById("tangkatanpembayaransyahriyah").focus();'>2018</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tangkatanpembayaransyahriyah").value = "2019"; document.getElementById("tangkatanpembayaransyahriyah").focus();'>2019</a>
							</li>
									
							
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
					  <div class="input-field col s2">
						<label for="ttahunpembayaransyahriyah" class="black-text">Tahun Pembayaran</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="ttahunpembayaransyahriyah" type="text" name="ttahunpembayaransyahriyah" class="black-text disable" data-error=".erorttahunpembayaransyahriyah" placeholder="Silahkan Pilih" onchange="$('#ttahunpembayaransyahriyah').val('');">
						<div class="erorttahunpembayaransyahriyah" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihtahunpembayaransyahriyah">Pilih Tahun Pembayaran<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihtahunpembayaransyahriyah" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("ttahunpembayaransyahriyah").value = "semua-tahun"; document.getElementById("ttahunpembayaransyahriyah").focus();'>Semua Tahun</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahunpembayaransyahriyah").value = "2017"; document.getElementById("ttahunpembayaransyahriyah").focus();'>2017</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahunpembayaransyahriyah").value = "2018"; document.getElementById("ttahunpembayaransyahriyah").focus();'>2018</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("ttahunpembayaransyahriyah").value = "2019"; document.getElementById("ttahunpembayaransyahriyah").focus();'>2019</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					<div class="row">
					  <div class="input-field col s2">
						<label for="tbulanpembayaransyahriyah" class="black-text">Bulan</label>
					  </div> 
					  <div class="input-field col s5">
						<input id="tbulanpembayaransyahriyah" type="text" name="tbulanpembayaransyahriyah" class="black-text disable" data-error=".erortbulanpembayaransyahriyah" placeholder="Silahkan Pilih" onchange="$('#tbulanpembayaransyahriyah').val('');">
						<div class="erortbulanpembayaransyahriyah" style="color:red;font-size:12px;"></div>
					  </div> 
					  
					  <div class="input-field col s5">
						<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihbulanpembayaransyahriyah">Pilih Bulan Pembayaran<i class="mdi-navigation-arrow-drop-down right"></i></a>
						<ul id="pilihbulanpembayaransyahriyah" class="dropdown-content">
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "semua-bulan"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Semua Bulan</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "1"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Januari</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "2"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Februari</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "3"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Maret</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "4"; document.getElementById("tbulanpembayaransyahriyah").focus();'>April</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "5"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Mei</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "6"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Juni</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "7"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Juli</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "8"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Agustus</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "9"; document.getElementById("tbulanpembayaransyahriyah").focus();'>September</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "10"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Oktober</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "11"; document.getElementById("tbulanpembayaransyahriyah").focus();'>November</a>
							</li>
							<li><a class="black-text" onclick='document.getElementById("tbulanpembayaransyahriyah").value = "12"; document.getElementById("tbulanpembayaransyahriyah").focus();'>Desember</a>
							</li>
												
						</ul>
					  </div>
					  
					</div>
					
					<div class="row">
				  
						<div class="input-field col s12">
							<button class="btn blue" type="submit" style="width:100%; height:60px;">Buat Laporan
							
							</button>
						</div>
					</div>
					
					
					
			</div>
			
		
		</div>
		
		
		
		<!-- ================================================
				Script Validator input laporan
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					next();
					
				}
			});
			
			

			$("#inputlaporan").validate({
				rules: {
					tlaporan: {
						required: true,
						minlength: 2
					}
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tlaporan:{
						required: "Silahkan Pilih Laporan"
					}
					
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
			Selesai Script Validator input laporan 
		================================================ -->
		
		<!-- ================================================
				Script Validator input filter santri
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					getlaporansatri();
					
				}
			});
			
			

			$("#santri").validate({
				rules: {
					tangkatansantri: {
						required: true,
						minlength: 2
					},
					tjeniskelaminsantri: {
						required: true,
						minlength: 2
					}
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tangkatansantri:{
						required: "Silahkan Pilih Angkatan"
					},
					tjeniskelaminsantri:{
						required: "Silahkan Pilih Jenis Kelamin"
					}
					
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
			Selesai Script Validator filter santri 
		================================================ -->
		
		<!-- ================================================
				Script Validator input filter tagihan
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					getlaporantagihan();
					
				}
			});
			
			

			$("#tagihan").validate({
				rules: {
					tstatustagihan: {
						required: true,
						minlength: 2
					},
					ttahuntagihan: {
						required: true,
						minlength: 2
					},
					tbulantagihan: {
						required: true,
						minlength: 2
					}
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tstatustagihan:{
						required: "Silahkan Pilih Status"
					},
					ttahuntagihan:{
						required: "Silahkan Pilih Tahun"
					},
					tbulantagihan:{
						required: "Silahkan Pilih Bulan"
					}
					
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
			Selesai Script Validator filter tagihan 
		================================================ -->
		
		<!-- ================================================
				Script Validator input filter Pembayaran Pendaftarab
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					getlaporanpembayaranpendaftaran();
					
				}
			});
			
			

			$("#pembayaranpendaftaran").validate({
				rules: {
					tangkatanpembayaranpendaftaran: {
						required: true,
						minlength: 2
					},
					tstatuspembayaranpendaftaran: {
						required: true,
						minlength: 2
					}
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					tangkatanpembayaranpendaftaran:{
						required: "Silahkan Pilih Angjata"
					},
					tstatuspembayaranpendaftaran:{
						required: "Silahkan Pilih Status"
					}
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
			Selesai Script Validator filter Pembayaran Pendaftarab 
		================================================ -->
		<!-- ================================================
				Script Validator input filter Pembayaran Syahriyah
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					getlaporanpembayarabsyahriyah();
					
				}
			});
			
			

			$("#pembayaransyahriyah").validate({
				rules: {
					ttahunpembayaransyahriyah: {
						required: true,
						minlength: 2
					},
					tangkatanpembayaransyahriyah: {
						required: true,
						minlength: 2
					},
					tbulanpembayaransyahriyah: {
						required: true,
						minlength: 1
					}
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					ttahunpembayaransyahriyah:{
						required: "Silahkan Pilih Tahun"
					},
					tangkatanpembayaransyahriyah:{
						required: "Silahkan Pilih angkatan"
					},
					tbulanpembayaransyahriyah:{
						required: "Silahkan Pilih Bulan"
					}
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
			Selesai Script Validator filter Pembayaran Syahriyah 
		================================================ -->
		
		================================================ -->
		<!-- ================================================
				Script Validator input filter Pembayaran Syahriyah
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					getlaporanpemasukandanadonatur();
					
				}
			});
			
			

			$("#pemasukandanadonatur").validate({
				rules: {
					ttahunpemasukandanadonatur: {
						required: true,
						minlength: 2
					},
					
					tbulanpemasukandanadonatur: {
						required: true,
						minlength: 1
					}
					
				},
				
				
				
				//For custom messages
				messages: {
					
					
					ttahunpemasukandanadonatur:{
						required: "Silahkan Pilih Tahun"
					},
					
					tbulanpemasukandanadonatur:{
						required: "Silahkan Pilih Bulan"
					}
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
			Selesai Script Validator filter Pembayaran Syahriyah 
		================================================ -->
		
		
		
		
		
		
		
		<script>
			function mulai(){
				
				$('#input2').hide();
				$('#santri').hide();
				$('#donatur').hide();
				$('#tagihan').hide();
				$('#pembayaranpendaftaran').hide();
				$('#pembayaransyahriyah').hide();
				$('#pemasukandanadonatur').hide();
				
				
			}
			
			function next(){
				
				$('#input1').hide();
				
				if($('#tlaporan').val()=="Data Santri"){
					
					$('#input2').show();
					$('#santri').show();
					
				}
				
				if($('#tlaporan').val()=="Data Donatur"){
					
					getlaporandonatur();
					
				}
				if($('#tlaporan').val()=="Data Tagihan"){
					
					$('#input2').show();
					$('#tagihan').show();
					
				}
				if($('#tlaporan').val()=="Data Pembayaran Pendaftaran"){
					
					$('#input2').show();
					$('#pembayaranpendaftaran').show();
					
				}
				if($('#tlaporan').val()=="Data Pembayaran Syahriyah"){
					
					$('#input2').show();
					$('#pembayaransyahriyah').show();
					
				}
				if($('#tlaporan').val()=="Data Pemasukan Dana Donatur"){
					
					$('#input2').show();
					$('#pemasukandanadonatur').show();
					
				}
				
				
				
				
			}
			
			function getlaporansatri(){
				
				angkatan=$("#tangkatansantri").val();
				jeniskelamin=$("#tjeniskelaminsantri").val();
				$('#input1').show();
				$('#input2').hide();
				$('#santri').hide();
				window.open("<?php echo base_url()?>admin/getlaporan/laporan-data-santri/"+angkatan+"/"+jeniskelamin, "_blank");
			}
			
			function getlaporandonatur(){
				
				$('#input1').show();
				$('#input2').hide();
				$('#santri').hide();
				window.open("<?php echo base_url()?>admin/getlaporan/laporan-data-donatur/", "_blank");
			}
			
			function getlaporantagihan(){
				
				status=$("#tstatustagihan").val();
				tahun=$("#ttahuntagihan").val();
				bulan=$("#tbulantagihan").val();
				
				$('#input1').show();
				$('#input2').hide();
				$('#tagihan').hide();
				window.open("<?php echo base_url()?>admin/getlaporan/laporan-data-tagihan/"+status+"/"+tahun+"/"+bulan,"_blank");
			}
			
			
			function getlaporanpembayaranpendaftaran(){
				
				status=$("#tstatuspembayaranpendaftaran").val();
				angkatan=$("#tangkatanpembayaranpendaftaran").val();
				
				$('#input1').show();
				$('#input2').hide();
				$('#pembayaranpendaftaran').hide();
				window.open("<?php echo base_url()?>admin/getlaporan/laporan-data-pembayaran-pendaftaran/"+angkatan+"/"+status,"_blank");
			}
			
			function getlaporanpembayarabsyahriyah(){
				
				angkatan=$("#tangkatanpembayaransyahriyah").val();
				tahun=$("#ttahunpembayaransyahriyah").val();
				bulan=$("#tbulanpembayaransyahriyah").val();
				
				
				$('#input1').show();
				$('#input2').hide();
				$('#pembayaransyahriyah').hide();
				window.open("<?php echo base_url()?>admin/getlaporan/laporan-data-pembayaran-syahriyah/"+angkatan+"/"+tahun+"/"+bulan,"_blank");
			}
			
			
			function getlaporanpemasukandanadonatur(){
				
				tahun=$("#ttahunpemasukandanadonatur").val();
				bulan=$("#tbulanpemasukandanadonatur").val();
				
				
				$('#input1').show();
				$('#input2').hide();
				$('#pembayaransyahriyah').hide();
				window.open("<?php echo base_url()?>admin/getlaporan/laporan-data-pemasukan-dana-donatur/"+tahun+"/"+bulan,"_blank");
			}
			
		</script>
		
		
		<script>
			mulai();
		</script>



