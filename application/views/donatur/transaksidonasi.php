	
	
	
	<div class="kontenpembayaran">
		
		
		<form id="formtransaksidonasi">

				<div class="row">
				  <div class="input-field col s12">
				   <center><h5 id="judultransaksidonasi">Transaksi Donasi</h5></center>
				  </div>
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ttotalbayar" class="black-text">Jumlah Donasi</label>
				  </div>
				  
				  <div class="input-field col s4">
					<input id="ttotaldonasi" type="text" name="ttotaldonasi" type="text" data-error=".erorttotaldonasi" placeholder="Masukan Jumlah Donasi">
					<div class="erorttotaldonasi" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				  
				</div>
				
				<div class="row">
				  
					<div class="input-field col s3">
						<button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
						</button>
					</div>
				</div>
				
			
				
			</form>
			
		
	</div>
	
	
		<!-- ================================================
		  Script Validator Transaksi Donasi
		================================================ -->
		<script>
			
			$.validator.setDefaults({
				submitHandler: function() {
					tambah();
					
				}
			});
			
			
			$("#formtransaksidonasi").validate({
				rules: {
					
					
					ttotaldonasi: {
						required: true,
						minlength: 4,
						number:true
					},
					
					
				},
				
				
				
				//For custom messages
				messages: {
								
					
					ttotaldonasi:{
						required: "Silahkan Masukan Total Bayar",
						minlength: "Nominal kurang dari 4 dijit",
						number: "Silahkan masukan nomina yang valid"
					},
					
					
				},
				errorElement : 'div',
				errorPlacement: function(error, element) {
				  var placement = $(element).data('error');
				  if (placement) {
					$(placement).append(error)
				  } else {
					error.insertAfter(element);
				  }
				}
				
				
				
			});
			
		</script>
				
		<!-- ================================================
		  Selesai Script Validator Transaksi Donasi
		================================================ -->
		<!-- ================================================
						Script CRUD
		================================================ -->
		
		<script>
			
			
			function tambah(){
				var data = new FormData();
				
				
				data.append('iddonatur', '<?php echo $id;?>');
				data.append('tanggaldonasi', '<?php echo date("Y-m-d");?>');
				data.append('jumlahdonasi', $("#ttotaldonasi").val());
				data.append('transaksi', 'online');
				data.append('statusverifikasi', 'belum');
				
				$.ajax({
					url		: '<?php echo base_url('transaksidonasi/add')?>',
					type	: 'post',
					processData: false,
					contentType: false,
					dataType: 'html',
					data	: data,
					beforeSend : function(){
						
					},
					success : function(data){
						
						window.location.replace('<?php echo base_url('donatur/sukses-transaksi-donasi')?>');
							 
					},
					error : function(error){
						
						
						 
					},
					
					
				});
				
			}
			
			
			
		</script>
		<!-- ================================================
						Script CRUD
		================================================ -->
		
		
		
		