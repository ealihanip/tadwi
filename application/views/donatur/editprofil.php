	
	
	
	<div class="kontenpembayaran">
		
		<center><h4>EDIT PROFIL DONATUR</h4></center>
		
		
		<div class="form grey lighten-5 z-depth-1">
			<form id="formeditdonatur">

				<div class="row"  id="juduleditdatadonatur">
				  <div class="input-field col s12">
				   <center><h5></h5></center>
				  </div>
				</div>
				
				<input type="hidden" id="eiddonatur">
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="enamadonatur">Nama Donatur</label>
				  </div>
				  
				  <div class="input-field col s19">
					<input id="enamadonatur" type="text" name="enamadonatur" type="text" data-error=".erorenamadonatur" placeholder="edit">
					<div class="erorenamadonatur" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="ejeniskelamin">Jenis Kelamin</label>
				  </div> 
				  <div class="input-field col s5">
					<input id="ejeniskelamin" type="text" name="ejeniskelamin" type="text" data-error=".erorejeniskelamin" placeholder="Silahkan Pilih" >
					<div class="erorejeniskelamin" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="pilihjeniskelamin">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="pilihjeniskelamin" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("ejeniskelamin").value = "Pria"; document.getElementById("ejeniskelamin").focus();'>Pria</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("ejeniskelamin").value = "Wanita"; document.getElementById("ejeniskelamin").focus();'>Wanita</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					<label for="etempatlahir" >Tempat Lahir</label>
				  </div>
				  <div class="input-field col s3">
					<input id="etempatlahir" type="text" name="etempatlahir" type="text" data-error=".eroretempatlahir" placeholder="edit" >
					<div class="eroretempatlahir" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				  
				  <div class="input-field col s3">
					
					<label for="etanggallahir" >Tangal Lahir</label>
				  </div>
				  
				  <div class="input-field col s2">
					<input id="etanggallahir" type="text" class="tanggallahir" name="etanggallahir" type="text" data-error=".eroretanggallahir" placeholder="edit" >
					<div class="eroretanggallahir" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ealamat">Alamat</label>
				  </div>
				  <div class="input-field col s9">
					<textarea id="ealamat" class="materialize-textarea" name="ealamat" type="text" data-error=".erorealamat" placeholder="edit"></textarea>
					
				  </div>
				  
				</div>
								
				<div class="row">
				  <div class="input-field col s3">
					<label for="temail">Email</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="eemail" type="text" name="eemail" type="text" data-error=".eroreemail">
					<div class="eroreemail" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
								
				<div class="row">
				  <div class="input-field col s3">
					<label for="enotelepon">No Telepon</label>
				  </div>
				  <div class="input-field col s9">
					<input id="enotelepon" type="text" name="enotelepon" type="text" data-error=".erorenotelepon" placeholder="edit">
					<div class="erorenotelepon" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				
				  <div class="input-field col s3">
					<label for="tnorekening">No Rekening</label>
				  </div>
				  <div class="input-field col s9">
					<input id="enorekening" type="text" name="enorekening" type="text" data-error=".erorenorekening">
					<div class="erorenorekening" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
				<div class="row">
				   <div class="input-field col s3">
					<label for="etanggaljoin" >Tanggal Join</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="etanggaljoin" type="text" class="tanggallahir" name="etanggaljoin" type="text" data-error=".eroretanggaljoin" >
					<div class="erorettanggaljoin" style="color:red;font-size:12px;"></div>
					
				  </div>
				</div>
				
				<div class="row">
				  
				  <div class="input-field col s3">
					<label for="ejeniskelamin">Status Donatur</label>
				  </div> 
				  
				  <div class="input-field col s5">
					<input id="estatusdonatur" type="text" name="estatusdonatur" type="text" data-error=".erorestatusdonatur" placeholder="SIlahkan Pilih" >
					
					<div class="erorestatusdonatur" style="color:red;font-size:12px;"></div>
				  </div> 
				  
				  <div class="input-field col s3">
					<a class="btn-flat dropdown-button black darken-4 accent-2 white-text"  data-activates="estatusdonaturih">Pilih<i class="mdi-navigation-arrow-drop-down right"></i></a>
					<ul id="estatusdonaturih" class="dropdown-content">
						<li><a class="black-text" onclick='document.getElementById("estatusdonatur").value = "Tetap"; document.getElementById("estatusdonatur").focus();'>Tetap</a>
						</li>
						<li><a class="black-text" onclick='document.getElementById("estatusdonatur").value = "Bukan Tetap"; document.getElementById("estatusdonatur").focus();'>Bukan Tetap</a>
						</li>
											
					</ul>
				  </div>
				  
				</div>
				
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="eusername">Username</label>
				  </div>
				  
				  <div class="input-field col s5">
					<input id="eusername" type="text" name="eusername" type="text" data-error=".eroreusername" disabled>
					<div class="eroreusername" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3" >
					
					<label for="epassword">Password</label>
				  </div>
				  
				  
				  <div class="input-field col s9" >
					<input id="epassword" type="password" name="epassword" type="text" data-error=".erorepassword">
					<div class="erorepassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				<div class="row">
				  <div class="input-field col s3">
					
					<label for="ekpassword">Ketik Ulang Password</label>
				  </div>
				  
				  <div class="input-field col s9">
					<input id="ekpassword" type="password" name="ekpassword" type="text" data-error=".erorekpassword">
					<div class="erorekpassword" style="color:red;font-size:12px;"></div>
					
				  </div>
				  
				</div>
				
				
				
				<div class="row">
					<div class="input-field col s4">
					  <button class="btn blue" type="submit" style="width:100%; height:60px;">Simpan
						
					  </button>
					</div>
				</div>
			</form>
		</div>
			
		
		
		
	</div>
	
	
	
	
	<!-- ================================================
			Script Validator Edit Data donatur
	================================================ -->
	<script>
		
		$.validator.setDefaults({
			submitHandler: function() {
				edit();
				
			}
		});
		
		
		$("#formeditdonatur").validate({
			rules: {
				enamadonatur: {
					required: true,
					minlength: 4
				},
				ejeniskelamin: {
					required: true,
					minlength: 4
				},
				etempatlahir: {
					required: true,
					minlength: 4
				},
				etanggallahir: {
					required: true,
					minlength: 4
				},
				eemail: {
					required: true,
					minlength: 4
				},
				enotelepon: {
					required: true,
					minlength: 4,
					number:true
				},
				ealamat: {
					required: true,
					minlength: 4
				},
				enorekening: {
					required: true,
					minlength: 4,
					number:true
				},
				estatusdonatur: {
					required: true,
					minlength: 4
				},
				etanggaljoin: {
					required: true,
					
				},
				eusername: {
					required: true,
					minlength: 4
				},
				epassword: {
					required: true,
					minlength: 4
				},
				ekpassword: {
					required: true,
					minlength: 4,
					equalTo: "#epassword"
				},
				
			},
			
			
			
			//For custom messages
			messages: {
				enamadonatur:{
					required: "Masukan Nama Donatur",
					minlength: "Nama Donatur lebih dari 4 huruf"
				},
				ejeniskelamin:{
					required: "Pilih Jenis Kelamin",
					minlength: "Silahkan Pilih Jenis Kelamin"
				},
				eemail:{
					required: "silahkan masukan email",
					minlength: "Email harus diisi"
				},
				etempatlahir:{
					required: "Masukan Tempat Lahir",
					minlength: "Tempat Lahir Harus lebih dari 4 huruf"
				},
				etanggallahir:{
					required: "Masukan tanggal lahir Donatur",
					
				},
				ealamat:{
					required: "Masukan alamat Donatur",
					minlength: "Alamat Harus lebih dari 4 huruf"
				},
				enotelepon:{
					required: "Masukan no telepon Donatur",
					minlength: "No Telepon Harus lebih dari 4 huruf",
					number: "Hanya Angka Yang Di Perbolehkan"
				},
				enorekening:{
					required: "Silahkan Masukan No rekening",
					minlength: "No Rekening Harus lebih dari 4 Angka",
					number: "Hanya Angka Yang Di Perbolehkan"
				},
				estatusdonatur:{
					required: "Silahkan Pilih Status Donatur",
					minlength: "Silahkan Pilih Status Donatur"
				},
				etanggaljoin:{
					required: "Silahkan Masukan Tanggal Join",
					
				},
				eusername:{
					required: "Username tidak boleh kosong",
					minlength: "usernmae Harus lebih dari 4 huruf"
				},
				epassword:{
					required: "Masukan password untuk Donatur",
					minlength: "Password Harus lebih dari 4 huruf"
				},
				ekpassword:{
					required: "Ulangi kata sandi",
					equalTo: "Password tidak tepat"
				},
				
			},
			errorElement : 'div',
			errorPlacement: function(error, element) {
			  var placement = $(element).data('error');
			  if (placement) {
				$(placement).append(error)
			  } else {
				error.insertAfter(element);
			  }
			}
			
			
			
		});
		
	</script>
	
	<!-- ================================================
			Script Validator Edit Data Donatur
	================================================ -->

	
	
	<!-- ================================================
			Script Edit Data Donatur
	================================================ -->
	
	<script>
	
	function getdataedit(iddonatur){
		
		var data = new FormData();
		data.append('iddonatur', iddonatur);
		
		
		$.ajax({
			url		: '<?php echo base_url('donatur/get')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				var json = $.parseJSON(data);
								
				document.getElementById("eiddonatur").value =json.data[0].id_donatur;
				document.getElementById("enamadonatur").value =json.data[0].nm_donatur;
				document.getElementById("ejeniskelamin").value =json.data[0].kelamin; 
				document.getElementById("etempatlahir").value =json.data[0].tmp_lahir; 
				document.getElementById("etanggallahir").value =json.data[0].tgl_lahir; 
				document.getElementById("ealamat").value =json.data[0].alamat; 
				document.getElementById("eemail").value =json.data[0].email; 
				document.getElementById("enotelepon").value =json.data[0].no_telp; 
				document.getElementById("enorekening").value =json.data[0].no_rek; 
				document.getElementById("estatusdonatur").value =json.data[0].status; 
				document.getElementById("etanggaljoin").value =json.data[0].tgl_join; 
				document.getElementById("eusername").value =json.datauser[0].username; 
				document.getElementById("epassword").value =json.datauser[0].password; 
				document.getElementById("ekpassword").value =json.datauser[0].password; 
				$('#juduleditdatadonatur').focus();
				
				
			}
		});
		
		
	}
			
	function edit(){
		 
		var data = new FormData();
		
		data.append('iddonatur', $("#eiddonatur").val());
		data.append('namadonatur', $("#enamadonatur").val());
		data.append('jeniskelamin', $("#ejeniskelamin").val());
		data.append('tempatlahir', $("#etempatlahir").val());
		data.append('tanggallahir', $("#etanggallahir").val());
		data.append('alamat', $("#ealamat").val());
		data.append('email', $("#eemail").val());
		data.append('nohp', $("#enotelepon").val());
		data.append('norekening', $("#enorekening").val());
		data.append('statusdonatur', $("#estatusdonatur").val());
		data.append('tanggaljoin', $("#etanggaljoin").val());
		data.append('username', $("#eusername").val());
		data.append('password', $("#epassword").val());
		
		$.ajax({
			url		: '<?php echo base_url('donatur/update')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				window.location.replace('<?php echo base_url('donatur/profil')?>');
				 
			},
			error : function(error){
				
				
				 
			},
			
			
		});
	 
	 
	};
	
	
	</script>
	<!-- ================================================
			Selesai Script Edit Data Donatur
	================================================ -->
	<script>
	
		getdataedit(<?php echo $id;?>);
		
	</script>