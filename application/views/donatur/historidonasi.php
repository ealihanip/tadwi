	
	
	
	<div class="kontenpembayaran">
		
		<center><h1>Histori Donasi</h1></center>
		
		
		<div class="row grey lighten-5 z-depth-1">
		
			<div class="col s12">
				<table id="tabeltransaksidonasi" class="responsive-table display" cellspacing="0">
					<thead>
						<tr>
							<th>ID Transaksi</th>
							<th>ID Donatur</th>
							<th>Nama</th>
							<th>Tanggal</th>
							<th>Jumlah</th>
							<th>Status Verifikasi</th>
						</tr>
					</thead>
				 
				</table>
				
			</div>
		
		
		</div>
	</div>
	
	
	<!-- ================================================
						Script Datatable
		================================================ -->
			<script type="text/javascript">
	 
				var save_method; //for save method string
				var table;
	 
				$(document).ready(function() {
					//datatables
					table = $('#tabeltransaksidonasi').DataTable({
						
						bLengthChange: false,
						info: false,
						pageLength: 10,
						oLanguage: {
							sSearch: "Cari Berdasarkan ID Atau Nama Donatur"
						},						
						processing: true, //Feature control the processing indicator.
						serverSide: true, //Feature control DataTables' server-side processing mode.
						order: [[ 0, "desc" ]], //Initial no order.
						// Load data for the table's content from an Ajax source
						ajax: {
							url: '<?php echo base_url('transaksidonasi/getdata/');?><?php echo $id;?>',
							type: "post"
						},
						//Set column definition initialisation properties.
						columns: [
							{"data": "id_trans"},
							{"data": "id_donatur"},
							{"data": "nm_donatur"},
							{"data": "tgl_donasi"},
							{"data": "jml_donasi"},
							{"data": "statverifikasi",}
						],
						columnDefs: [
							
							
						],
						
								
						"createdRow": 
						
						function( row, data, dataIndex ) {
								if ( data['statverifikasi'] == "belum" ) {        
									$(row).addClass('purple accent-2');
						 
								}else{
									
									
								}
      

						}
						  
					});
	 
				});
			</script>