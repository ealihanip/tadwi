	
	
	<div class="kontenpembayaran">
		
		<center><h4>PROFIL DONATUR</h4></center>
		<div class="form grey lighten-5 z-depth-1">
			

			<div class="row" id="juduldetaildatasantri">
			  <div class="col s12">
			   
			  </div>
			</div>
			
			
			<div class="row">
			  
			  <div class="col s2">
				<p>ID Donatur</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="didonatur"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>Nama Donatur</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="dnamadonatur"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>Jenis Kelamin</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="djeniskelamin"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>Tempat Lahir</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="dtempatlahir"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>Tanggal Lahir</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="dtanggallahir"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>Alamat</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="dalamat"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>Email</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="demail"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>No Handphone</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="dnotelepon"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>No Rekening</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="dnorekening"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>Status</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="dstatus"></p>
			  </div>
			</div>
			
			<div class="row">
			  
			  <div class="col s2">
				<p>Tanggal Join</p>
				
			  </div>
			  
			   <div class="col s1">
				<p>:</p>
				
			  </div>
			  
			  <div class="col s9">
				<p id="dtanggaljoin"></p>
			  </div>
			</div>
				
				
			<a href="<?php echo base_url();?>donatur/edit-profil"class="btn blue">Edit Profil</a>	
		</div>
		
	</div>
	
	
	
	<script>
	
	function getdata(iddonatur){
				
		var data = new FormData();
		data.append('iddonatur', iddonatur);
		
		
		$.ajax({
			url		: '<?php echo base_url('donatur/get')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				var json = $.parseJSON(data);
				
						
				$('#didonatur').text(json.data[0].id_donatur);
				$('#dnamadonatur').text(json.data[0].nm_donatur);
				$('#djeniskelamin').text(json.data[0].kelamin); 
				$('#dtempatlahir').text(json.data[0].tmp_lahir); 
				$('#dtanggallahir').text(json.data[0].tgl_lahir); 
				$('#dalamat').text(json.data[0].alamat); 
				$('#demail').text(json.data[0].email); 
				$('#dnotelepon').text(json.data[0].no_telp); 
				$('#dnorekening').text(json.data[0].no_rek); 
				$('#dstatus').text(json.data[0].status); 
				$('#dtanggaljoin').text(json.data[0].tgl_join); 
				
				
			}
		});
		
		
	}
				
	
	</script>
	
	<script>

		getdata(<?php echo $id;?>);

	</script>