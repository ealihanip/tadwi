<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


/*Home*/
$route['home'] = 'front/menu/getviewhome';
$route['pendaftaran-donatur'] = 'front/menu/getviewpendaftarandonatur';
$route['cara-pembayaran-donasi'] = 'front/menu/getviewcarapembayarandonasi';
$route['sukses-pendaftaran-donatur'] = 'front/menu/getviewsuksespendaftaran';


/*ADMIN*/
$route['admin'] = 'admin/menu/getviewhome';
$route['admin/data-santri'] = 'admin/menu/getviewsantri';
$route['admin/data-donatur'] = 'admin/menu/getviewdonatur';
$route['admin/data-user'] = 'admin/menu/getviewuser';
$route['admin/pembayaran'] = 'admin/menu/getviewhomepembayaran';
$route['admin/pembayaran-pendaftaran-santri'] = 'admin/menu/getviewpembayaranpendaftaransantri';
$route['admin/pembayaran-syahriyah-santri'] = 'admin/menu/getviewpembayaransyahriyahsantri';
$route['admin/pembayaran-tagihan-santri'] = 'admin/menu/getviewtagihansantri';
$route['admin/pemasukan-donatur'] = 'admin/menu/getviewpemasukandatadonatur';
$route['admin/kelola-web-home'] = 'admin/menu/getviewkelolawebhome';
$route['admin/kelola-web-cara-pembayaran-donasi'] = 'admin/menu/getviewkelolawebcarapembayarandonasi';
$route['admin/laporan'] = 'admin/menu/getviewlaporan';
$route['admin/logout'] = 'admin/menu/logout';
$route['admin/getlaporan/(:any)'] = 'admin/laporan/getlaporan/';
$route['admin/getlaporan/(:any)/(:any)'] = 'admin/laporan/getlaporan/';
$route['admin/getlaporan/(:any)/(:any)/(:any)'] = 'admin/laporan/getlaporan/';
$route['admin/getlaporan/(:any)/(:any)/(:any)/(:any)'] = 'admin/laporan/getlaporan/';

/*donatur*/
$route['donatur'] = 'donaturindex/menu/getviewhome';
$route['donatur/profil'] = 'donaturindex/menu/getviewprofil';
$route['donatur/edit-profil'] = 'donaturindex/menu/getvieweditprofil';
$route['donatur/histori-donasi'] = 'donaturindex/menu/getviewhistoridonasi';
$route['donatur/transaksi-donasi'] = 'donaturindex/menu/getviewtransaksidonasi';
$route['donatur/sukses-transaksi-donasi'] = 'donaturindex/menu/getviewsuksestransaksidonasi';
$route['donatur/logout'] = 'donaturindex/menu/logout';

/*walisantri*/
$route['walisantri'] = 'walisantriindex/menu/getviewhome';
$route['walisantri/biodata'] = 'walisantriindex/menu/getviewbiodata';
$route['walisantri/edit-biodata'] = 'walisantriindex/menu/getvieweditbiodata';
$route['walisantri/tagihan'] = 'walisantriindex/menu/getviewtagihan';
$route['walisantri/logout'] = 'walisantriindex/menu/logout';




