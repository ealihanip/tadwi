-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 25 Des 2017 pada 08.20
-- Versi Server: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ponpes`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id_user` varchar(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(100) NOT NULL,
  `jenisuser` varchar(20) NOT NULL,
  `ref_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `jenisuser`, `ref_id`) VALUES
('1000002', 'kacol', '128641e7cd7051c3586306d92835ebde', 'santri', '123123123123'),
('1000003', 'kanep1', '80d82cf0f568d0bbaa777fa064f4cb6b', 'santri', '123123123127'),
('1000005', 'kanepzz', 'c7001c9f5ad66543f60e8749ac5a7366', 'donatur', '20020170001'),
('1000006', 'kacol1', '128641e7cd7051c3586306d92835ebde', 'donatur', '20020170003'),
('1000007', 'kanep', '80d82cf0f568d0bbaa777fa064f4cb6b', 'santri', '12312223312'),
('1000008', 'kanepz', '80d82cf0f568d0bbaa777fa064f4cb6b', 'santri', '23234234'),
('1000009', 'adminkanepz', '80d82cf0f568d0bbaa777fa064f4cb6b', 'admin', 'admin'),
('1000010', 'lina', 'f6f4deb7dad1c2e5e0b4d6569dc3c1c5', 'donatur', '20020170004'),
('1000011', 'April', '37d153a06c79e99e4de5889dbe2e7c57', 'admin', 'admin'),
('1000012', 'nepnep', '60b1d3acfb0ba79acfd888d86ae30f0e', 'santri', '123123123129'),
('1000013', 'dudu', 'e70d3289cbd29cf4ff07bfc001459b30', 'donatur', '20020170006'),
('1000014', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
