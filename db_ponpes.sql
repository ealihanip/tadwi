-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2017 at 10:51 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ponpes`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pbr_syh`
--

CREATE TABLE `tbl_pbr_syh` (
  `id_pmbyrn2` varchar(15) NOT NULL,
  `id_tagihan` varchar(15) NOT NULL,
  `tgl_bayar` varchar(15) NOT NULL,
  `pembayaran_makan` varchar(15) NOT NULL,
  `pembayaran_listrik` varchar(15) NOT NULL,
  `pembayaran_infaq` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pbr_syh`
--

INSERT INTO `tbl_pbr_syh` (`id_pmbyrn2`, `id_tagihan`, `tgl_bayar`, `pembayaran_makan`, `pembayaran_listrik`, `pembayaran_infaq`) VALUES
('50020170001', '30020170002', '2017-11-28', '100000', '0', '0'),
('50020170003', '30020170006', '2017-12-13', '330000', '50000', '20000'),
('50020170005', '30020170003', '2017-12-03', '330000', '50000', '20000');

-- --------------------------------------------------------

--
-- Table structure for table `tb_donatur`
--

CREATE TABLE `tb_donatur` (
  `id_donatur` varchar(11) NOT NULL,
  `nm_donatur` varchar(30) DEFAULT NULL,
  `kelamin` varchar(255) DEFAULT NULL,
  `tmp_lahir` varchar(20) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `no_rek` varchar(15) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL,
  `tgl_join` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_donatur`
--

INSERT INTO `tb_donatur` (`id_donatur`, `nm_donatur`, `kelamin`, `tmp_lahir`, `tgl_lahir`, `email`, `no_telp`, `alamat`, `no_rek`, `status`, `tgl_join`) VALUES
('20020170001', 'kanep', 'Wanita', 'bandung', '2017-11-21', 'bandung@gmail.com', '23423234234', 'dsfsfsdf', '234234', 'Tetap', '2017-11-19'),
('20020170003', 'kacol', 'Pria', 'bandung', '2017-11-12', 'ealihanip@gmail.com', '234234234', 'bandung', '123123123123123', 'Bukan Tetap', '2017-11-21'),
('20020170004', 'Lina', 'Wanita', 'Subang', '1995-08-30', 'lina@gmail.com', '089812345678', 'SUBANG', '123456908', 'Bukan Tetap', '2017-11-30');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konten_web`
--

CREATE TABLE `tb_konten_web` (
  `idkonten` int(15) NOT NULL,
  `namakonten` varchar(255) NOT NULL,
  `isikonten` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_konten_web`
--

INSERT INTO `tb_konten_web` (`idkonten`, `namakonten`, `isikonten`) VALUES
(1, 'home', '<p style=\"margin-left:0cm; text-align:center\"><span style=\"font-size:22px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\">Pondok Pesantren Al Hasan beralamat di&nbsp; Jl.Jendral A. Yani Dawuan Barat Cikampek Karawang. Pesantren&nbsp; Al Hasan merupakan lembaga pendidikan non formal dengan bercirikan salafiyah yang mengkaji kitab-kitab kuning karangan para ulama. Mencakup kajian ilmu tauhid, ilmu fiqih, ilmu &lsquo;alat,tasawuf,&nbsp; tahsinulqur-an, tafsir dll, bernaung dibawah Yayasan Pondok Pesantren Al Hasan Nahdlotul Ulama.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0cm; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0cm\"><span style=\"font-size:24px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\"><strong>Visi dan Misi</strong></span></span></p>\r\n\r\n<p style=\"margin-left:0cm\"><span style=\"font-size:24px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\"><strong>Visi</strong></span></span></p>\r\n\r\n<p><span style=\"font-size:24px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\">Mencetak Insan Kamil yang &lsquo;Amil.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm\"><span style=\"font-size:24px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\"><strong>Misi</strong></span></span></p>\r\n\r\n<p style=\"margin-left:0cm\"><span style=\"font-size:24px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\">1. Melaksanakan kajian kitab-kitab kuning.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm\"><span style=\"font-size:24px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\">2. Membekali santri dengan aqidah Ahi Sunnah wal Jama&rsquo;ah.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm\"><span style=\"font-size:24px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\">3. Membiasakan sholat berjama&rsquo;ah.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm\"><span style=\"font-size:24px\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\">4. Membiasakan akhlak mulia.</span></span></p>\r\n'),
(2, 'cara pembayaran donasi', '<p>asdasd</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pbr_pdftrn`
--

CREATE TABLE `tb_pbr_pdftrn` (
  `id_pmbyrn1` varchar(11) NOT NULL,
  `NIS` varchar(15) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `total_bayar` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pbr_pdftrn`
--

INSERT INTO `tb_pbr_pdftrn` (`id_pmbyrn1`, `NIS`, `tgl_bayar`, `total_bayar`) VALUES
('60020170001', '123123123123', '2017-11-28', '200000'),
('60020170002', '123123123127', '2017-11-21', '200000');

-- --------------------------------------------------------

--
-- Table structure for table `tb_santri`
--

CREATE TABLE `tb_santri` (
  `NIS` varchar(15) NOT NULL,
  `nm_santri` varchar(30) NOT NULL,
  `kelamin` varchar(10) NOT NULL,
  `tmp_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `angkatan` varchar(4) NOT NULL,
  `nm_ortu` varchar(30) NOT NULL,
  `foto` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_santri`
--

INSERT INTO `tb_santri` (`NIS`, `nm_santri`, `kelamin`, `tmp_lahir`, `tgl_lahir`, `alamat`, `no_hp`, `angkatan`, `nm_ortu`, `foto`) VALUES
('123123123123', 'kacol januar', 'Wanita', 'bandung', '2017-11-22', 'bandung', '123123123123', '2016', 'kanep', '1231231231239.jpg'),
('123123123127', 'kanep', 'Pria', 'bandung', '2017-11-20', 'bandung', '123123123123', '2017', 'bandung', '123123123127.jpg'),
('123123123129', 'nepnep', 'Pria', 'bandung', '2017-12-31', 'cikampek', '57567567567', '2017', 'bandung', '123123123129.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tagihan`
--

CREATE TABLE `tb_tagihan` (
  `id_tagihan` varchar(15) NOT NULL,
  `NIS` varchar(15) NOT NULL,
  `bulan` varchar(10) NOT NULL,
  `tahun` varchar(10) NOT NULL,
  `makan` varchar(15) NOT NULL,
  `listrik` varchar(15) NOT NULL,
  `infaq` varchar(15) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tagihan`
--

INSERT INTO `tb_tagihan` (`id_tagihan`, `NIS`, `bulan`, `tahun`, `makan`, `listrik`, `infaq`, `status`) VALUES
('30020170002', '123123123123', 'Januari', '2017', '330000', '50000', '20000', 'Belum Lunas'),
('30020170003', '123123123127', 'Januari', '2017', '330000', '50000', '20000', 'Sudah Lunas'),
('30020170006', '123123123123', 'Februari', '2017', '330000', '50000', '20000', 'Sudah Lunas'),
('30020170007', '123123123127', 'Februari', '2017', '330000', '50000', '20000', 'Belum Lunas'),
('30020170010', '123123123123', 'Maret', '2017', '330000', '50000', '20000', 'Belum Lunas'),
('30020170011', '123123123127', 'Maret', '2017', '330000', '50000', '20000', 'Belum Lunas'),
('30020170013', '123123123123', 'Mei', '2017', '330000', '50000', '20000', 'Belum Lunas'),
('30020170014', '123123123127', 'Mei', '2017', '330000', '50000', '20000', 'Belum Lunas');

-- --------------------------------------------------------

--
-- Table structure for table `tb_trx_dntr`
--

CREATE TABLE `tb_trx_dntr` (
  `id_trans` varchar(15) NOT NULL,
  `id_donatur` varchar(15) DEFAULT NULL,
  `tgl_donasi` date NOT NULL,
  `jml_donasi` varchar(12) NOT NULL,
  `transaksi` varchar(50) NOT NULL,
  `statverifikasi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trx_dntr`
--

INSERT INTO `tb_trx_dntr` (`id_trans`, `id_donatur`, `tgl_donasi`, `jml_donasi`, `transaksi`, `statverifikasi`) VALUES
('70020170001', '20020170001', '2017-11-28', '200000', 'byadmin', 'sudah'),
('70020170002', '20020170001', '2017-11-29', '200000', 'byadmin', 'sudah'),
('70020170003', '20020170001', '2017-11-21', '200000', 'online', 'sudah'),
('70020170004', '20020170001', '2017-11-21', '500000', 'online', 'sudah'),
('70020170005', '20020170001', '2017-11-15', '150000', 'byadmin', 'sudah'),
('70020170006', '20020170001', '2017-12-24', '50000', 'byadmin', 'sudah'),
('70020170007', '20020170003', '2017-12-12', '150000', 'byadmin', 'sudah'),
('70020170008', '20020170004', '2017-11-30', '200000', 'online', 'sudah'),
('70020170009', '20020170004', '2017-12-02', '1000000', 'online', 'sudah'),
('70020170010', '20020170001', '2017-12-03', '10000000', 'byadmin', 'sudah'),
('70020170011', '20020170004', '2017-12-12', '20000', 'online', 'belum');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` varchar(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `jenisuser` varchar(20) NOT NULL,
  `ref_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `jenisuser`, `ref_id`) VALUES
('1000001', 'admin', 'admin', 'admin', 'admin'),
('1000002', 'kacol', 'kacol', 'santri', '123123123123'),
('1000003', 'kanep1', 'kanep', 'santri', '123123123127'),
('1000005', 'kanepzz', 'kanep123', 'donatur', '20020170001'),
('1000006', 'kacol1', 'kacol', 'donatur', '20020170003'),
('1000007', 'kanep', 'kanep', 'santri', '12312223312'),
('1000008', 'kanepz', 'kanep', 'santri', '23234234'),
('1000009', 'adminkanepz', 'kanep', 'admin', 'admin'),
('1000010', 'lina', 'lina', 'donatur', '20020170004'),
('1000011', 'April', 'april', 'admin', 'admin'),
('1000012', 'nepnep', 'hanep', 'santri', '123123123129');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pbr_syh`
--
ALTER TABLE `tbl_pbr_syh`
  ADD PRIMARY KEY (`id_pmbyrn2`);

--
-- Indexes for table `tb_donatur`
--
ALTER TABLE `tb_donatur`
  ADD PRIMARY KEY (`id_donatur`);

--
-- Indexes for table `tb_konten_web`
--
ALTER TABLE `tb_konten_web`
  ADD PRIMARY KEY (`idkonten`);

--
-- Indexes for table `tb_pbr_pdftrn`
--
ALTER TABLE `tb_pbr_pdftrn`
  ADD PRIMARY KEY (`id_pmbyrn1`),
  ADD KEY `NIS` (`NIS`);

--
-- Indexes for table `tb_santri`
--
ALTER TABLE `tb_santri`
  ADD PRIMARY KEY (`NIS`);

--
-- Indexes for table `tb_tagihan`
--
ALTER TABLE `tb_tagihan`
  ADD PRIMARY KEY (`id_tagihan`),
  ADD KEY `FKSANTRI` (`NIS`);

--
-- Indexes for table `tb_trx_dntr`
--
ALTER TABLE `tb_trx_dntr`
  ADD PRIMARY KEY (`id_trans`),
  ADD KEY `id_donatur` (`id_donatur`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_konten_web`
--
ALTER TABLE `tb_konten_web`
  MODIFY `idkonten` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_pbr_pdftrn`
--
ALTER TABLE `tb_pbr_pdftrn`
  ADD CONSTRAINT `tb_pbr_pdftrn_ibfk_1` FOREIGN KEY (`NIS`) REFERENCES `tb_santri` (`NIS`);

--
-- Constraints for table `tb_tagihan`
--
ALTER TABLE `tb_tagihan`
  ADD CONSTRAINT `FKSANTRI` FOREIGN KEY (`NIS`) REFERENCES `tb_santri` (`NIS`);

--
-- Constraints for table `tb_trx_dntr`
--
ALTER TABLE `tb_trx_dntr`
  ADD CONSTRAINT `tb_trx_dntr_ibfk_1` FOREIGN KEY (`id_donatur`) REFERENCES `tb_donatur` (`id_donatur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
